-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Packages][Packages:1]]
-- * Packages

use work.common_package.all;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Entity][Entity:1]]
-- * Entity

entity catiroc_usb_if is
   generic (nb_asics             : natural range 1 to 8   := 1;
            input_bus_width_bits : natural range 1 to 128 := 128;
            extend               : integer range 0 to 1   := 0);
   port (main_rst        : in  std_logic;  -- global reset
         EnabledChannels : in  std_logic_vector(7 downto 0);
         -- input interface
         -- data in (8 asic managers side)
         asic_clk        : in  std_logic;
         asic_empty      : in  std_logic_vector(nb_asics-1 downto 0);
         asic_rd         : out std_logic_vector(nb_asics-1 downto 0);
         asic_data       : in  std_logic_vector(input_bus_width_bits*nb_asics-1 downto 0);
         -- if to usb manager
         fifo_clk        : in  std_logic;
         fifo_dataout    : out std_logic_vector((8*extend+input_bus_width_bits)-1 downto 0);
         fifo_rd         : in  std_logic;
         fifo_empty      : out std_logic);
end entity catiroc_usb_if;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of catiroc_usb_if is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Signals][Signals:1]]
-- ** Signals

   signal nb_asics_unsigned : unsigned(3 downto 0) := X"0";
   signal priority_flag     : unsigned(3 downto 0) := X"0";
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Signals][Signals:2]]
-- ** Signals

   signal fifo_datain                     : std_logic_vector((8*extend+input_bus_width_bits)-1 downto 0) := (others => '0');
   signal fifo_wr_en, fifo_full           : std_logic                                                    := '0';
   signal mystate                         : mystate_type;
   signal asic_empty_inner, asic_rd_inner : std_logic_vector(7 downto 0)                                 := (others => '0');
   signal asic_data_inner                 : std_logic_vector(8*input_bus_width_bits-1 downto 0)          := (others => '0');
-- signal extend_bit_vector                 : std_logic_vector(0 downto 0);
-- @@@ Signals:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Generic][Generic:1]]
-- ** Generic
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Randomize][Randomize:1]]
-- *** Randomize

   nb_asics_unsigned <= to_unsigned(nb_asics-1, 4);
-- @@@ Randomize:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Randomize][Randomize:2]]
-- *** Randomize

   process(asic_clk)
   begin
      if (asic_clk'event and asic_clk = '1') then
         if priority_flag = nb_asics_unsigned then
            priority_flag <= (others => '0');
         else
            priority_flag <= priority_flag + 1;
         end if;
      end if;
   end process;
-- @@@ Randomize:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Convert%20to%208%20bits][Convert to 8 bits:1]]
-- *** Convert to 8 bits

   asic_empty_inner <= std_logic_vector(resize(unsigned(asic_empty), 8));
   asic_rd          <= asic_rd_inner(nb_asics-1 downto 0);
   asic_data_inner  <= std_logic_vector(resize(unsigned(asic_data), input_bus_width_bits*8));
-- @@@ Convert to 8 bits:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Write%20fifo][Write fifo:1]]
-- *** Write fifo

--extend_bit_vector <= std_logic_vector(to_unsigned(extend, 1));
--extend_bit <= extend_bit_vector(0);

   process (asic_clk)
   begin

      if (asic_clk = '1' and asic_clk'event) then

         if (main_rst = '1') then

            fifo_wr_en    <= '0';
            asic_rd_inner <= (others => '0');
            mystate       <= mystate_state_1;

         else

            case mystate is

               when mystate_state_1 =>

                  asic_rd_inner <= (others => '0');
                  fifo_wr_en    <= '0';

                  if (priority_flag = X"0" and asic_empty_inner(0) = '0' and fifo_full = '0' and EnabledChannels(0) = '1') then

                     asic_rd_inner(0) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(1*input_bus_width_bits-1 downto 1*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "000";
                     else
                        fifo_datain <= asic_data_inner(1*input_bus_width_bits-1 downto 1*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"1" and asic_empty_inner(1) = '0' and fifo_full = '0' and EnabledChannels(1) = '1') then

                     asic_rd_inner(1) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(2*input_bus_width_bits-1 downto 2*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "001";
                     else
                        fifo_datain <= asic_data_inner(2*input_bus_width_bits-1 downto 2*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"2" and asic_empty_inner(2) = '0' and fifo_full = '0' and EnabledChannels(2) = '1') then

                     asic_rd_inner(2) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(3*input_bus_width_bits-1 downto 3*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "010";
                     else
                        fifo_datain <= asic_data_inner(3*input_bus_width_bits-1 downto 3*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"3" and asic_empty_inner(3) = '0' and fifo_full = '0' and EnabledChannels(3) = '1') then

                     asic_rd_inner(3) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(4*input_bus_width_bits-1 downto 4*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "011";
                     else
                        fifo_datain <= asic_data_inner(4*input_bus_width_bits-1 downto 4*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"4" and asic_empty_inner(4) = '0' and fifo_full = '0' and EnabledChannels(4) = '1') then

                     asic_rd_inner(4) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(5*input_bus_width_bits-1 downto 5*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "100";
                     else
                        fifo_datain <= asic_data_inner(5*input_bus_width_bits-1 downto 5*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"5" and asic_empty_inner(5) = '0' and fifo_full = '0' and EnabledChannels(5) = '1') then

                     asic_rd_inner(5) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(6*input_bus_width_bits-1 downto 6*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "101";
                     else
                        fifo_datain <= asic_data_inner(6*input_bus_width_bits-1 downto 6*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"6" and asic_empty_inner(6) = '0' and fifo_full = '0' and EnabledChannels(6) = '1') then

                     asic_rd_inner(6) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(7*input_bus_width_bits-1 downto 7*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "110";
                     else
                        fifo_datain <= asic_data_inner(7*input_bus_width_bits-1 downto 7*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  elsif (priority_flag = X"7" and asic_empty_inner(7) = '0' and fifo_full = '0' and EnabledChannels(7) = '1') then

                     asic_rd_inner(7) <= '1';
                     if extend = 1 then
                        fifo_datain <= asic_data_inner(8*input_bus_width_bits-1 downto 8*input_bus_width_bits-input_bus_width_bits) & '0' & X"0" & "111";
                     else
                        fifo_datain <= asic_data_inner(8*input_bus_width_bits-1 downto 8*input_bus_width_bits-input_bus_width_bits);
                     end if;
                     mystate <= mystate_state_2;

                  else

                     asic_rd_inner <= (others => '0');
                     -- asic_to_read <= X"00";
                     fifo_datain   <= (others => '1');
                     mystate       <= mystate_state_1;

                  end if;

               when mystate_state_2 =>

                  fifo_wr_en    <= '1';
                  asic_rd_inner <= (others => '0');
                  mystate       <= mystate_state_1;

            end case;

         end if;

      end if;

   end process;
-- @@@ Write fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Fifo][Fifo:1]]
-- ** Fifo
-- @@@ Fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Data%20readout][Data readout:1]]
-- *** Data readout

   U_fifo_sc : if input_bus_width_bits = 64 generate
      U_fifo : fifo_64
         port map (rst    => main_rst,
                   -- incoming data from nb_asics ASICS
                   wr_clk => asic_clk,
                   din    => fifo_datain,
                   wr_en  => fifo_wr_en,
                   full   => fifo_full,
                   -- if to usb managager
                   rd_clk => fifo_clk,
                   dout   => fifo_dataout,
                   rd_en  => fifo_rd,
                   empty  => fifo_empty);
   end generate;
-- @@@ Data readout:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*S%20Curve][S Curve:1]]
-- *** S Curve

   U_fifo_events : if input_bus_width_bits = 128 generate
      U_fifo : fifo_128
         port map (rst    => main_rst,
                   -- incoming data from nb_asics ASICS
                   wr_clk => asic_clk,
                   din    => fifo_datain,
                   wr_en  => fifo_wr_en,
                   full   => fifo_full,
                   -- if to usb managager
                   rd_clk => fifo_clk,
                   dout   => fifo_dataout,
                   rd_en  => fifo_rd,
                   empty  => fifo_empty);
   end generate;
-- @@@ S Curve:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*Slow%20control][Slow control:1]]
-- *** Slow control

   U_fifo_sc2 : if input_bus_width_bits = 8 generate
      U_fifo : fifo_8
         port map (rst    => main_rst,
                   -- incoming data from nb_asics ASICS
                   wr_clk => asic_clk,
                   din    => fifo_datain,
                   wr_en  => fifo_wr_en,
                   full   => fifo_full,
                   -- if to usb managager
                   rd_clk => fifo_clk,
                   dout   => fifo_dataout,
                   rd_en  => fifo_rd,
                   empty  => fifo_empty);
   end generate;
-- @@@ Slow control:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_usb_if.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
