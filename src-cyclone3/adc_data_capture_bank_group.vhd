-----------------------------------------------------------------------
----- File Handle_channel.vhd
----- Top file for handling ADC 1 voies/14bit output
----- Author N.Alexis
----- Rev V1.0  Instanciation of component
-----
-----------------------------------------------------------------------

-- * Libraries
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Package
use work.cati_package.all;

-- * Entity
entity adc_data_capture_bank_group is
   generic(channel_id : std_logic_vector(15 downto 0) := "0000000000000000");
   port (rx_inclock            : in  std_logic;
         rx_readclock          : in  std_logic;  -- oscillator send by adc 50Mhz trought pll
         rx_syncclock          : in  std_logic;
         clock_adc             : in  std_logic;
         rst                   : in  std_logic;
         trigger               : in  std_logic;
         pll_locked            : in  std_logic;
         --ADC
         adc_data              : in  std_logic_vector (3 downto 0);
         -- usb
         enable                : in  std_logic_vector (3 downto 0);  -- one hot
         channel_data_out_fifo : out std_logic_vector(15 downto 0);  -- usb Output bus (Send data to usb)
         read_channelfifos     : in  std_logic;  --FIFO1 read request
         fifo_channel_empty    : out std_logic);

end entity adc_data_capture_bank_group;

-- * Architecture
architecture Behavioral of adc_data_capture_bank_group is

   signal channel_output1, channel_output1_tmp : std_logic_vector (6 downto 0);
   signal channel_output2, channel_output2_tmp : std_logic_vector (6 downto 0);
   signal channel_output3, channel_output3_tmp : std_logic_vector (6 downto 0);
   signal channel_output4, channel_output4_tmp : std_logic_vector (6 downto 0);
   signal first_frame, second_frame            : std_logic_vector (6 downto 0);
   signal channel_fulldata                     : std_logic_vector (15 downto 0);
   signal fifo_clear                           : std_logic;
   signal fifo_wrreq                           : std_logic;
   signal disactive_channel                    : std_logic;
   signal s_fifo_empty                         : std_logic;
   signal flag_fifo_wrData                     : std_logic;
   signal enable_mux                           : std_logic := '0';
   signal fill_fifo_counter                    : unsigned (9 downto 0);
   signal receive_ADCdata_state                : receive_ADCdata_type;

begin

   fifo_channel_empty <= s_fifo_empty;

   -- ** Channel 0
   Altlvdsrx_inst1 : Altlvdsrx
      port map (rx_data_reset => not pll_locked,
                rx_in         => adc_data(0 downto 0),
                rx_inclock    => rx_inclock,    --70
                rx_readclock  => rx_readclock,  --20
                rx_syncclock  => rx_syncclock,  --10
                rx_out        => channel_output1_tmp);

   process (rx_readclock)
   begin
      if (rx_readclock'event and rx_readclock = '1') then
         channel_output1 <= channel_output1_tmp;
      end if;
   end process;


   -- ** Channel 1
   Altlvdsrx_inst2 : Altlvdsrx
      port map (rx_data_reset => not pll_locked,
                rx_in         => adc_data(1 downto 1),
                rx_inclock    => rx_inclock,
                rx_readclock  => rx_readclock,
                rx_syncclock  => rx_syncclock,
                rx_out        => channel_output2_tmp);

   process (rx_readclock)
   begin
      if (rx_readclock'event and rx_readclock = '1') then
         channel_output2 <= channel_output2_tmp;
      end if;
   end process;


   -- ** Channel 2
   Altlvdsrx_inst3 : Altlvdsrx
      port map (rx_data_reset => not pll_locked,
                rx_in         => adc_data(2 downto 2),
                rx_inclock    => rx_inclock,
                rx_readclock  => rx_readclock,
                rx_syncclock  => rx_syncclock,
                rx_out        => channel_output3_tmp);

   process (rx_readclock)
   begin
      if (rx_readclock'event and rx_readclock = '1') then
         channel_output3 <= channel_output3_tmp;
      end if;
   end process;


   -- ** Channel 3
   Altlvdsrx_inst4 : Altlvdsrx
      port map (rx_data_reset => not pll_locked,
                rx_in         => adc_data(3 downto 3),
                rx_inclock    => rx_inclock,
                rx_readclock  => rx_readclock,
                rx_syncclock  => rx_syncclock,
                rx_out        => channel_output4_tmp);

   process (rx_readclock)
   begin
      if (rx_readclock'event and rx_readclock = '1') then
         channel_output4 <= channel_output4_tmp;
      end if;
   end process;


   -- ** Register data and demultiplex
   --! Take only one of the four channels
   process (rx_readclock)
   begin

      if (rx_readclock'event and rx_readclock = '1') then

         -- second register stage
         second_frame <= first_frame;

         if (enable(0) = '1') then
            first_frame <= channel_output1;
         elsif (enable(1) = '1') then
            first_frame <= channel_output2;
         elsif (enable(2) = '1') then
            first_frame <= channel_output3;
         elsif (enable(3) = '1') then
            first_frame <= channel_output4;
         end if;

      end if;

   end process;


   -- ** Fill fifo
   --! Write 128 16 bits words to fifo, when it is empty
   --! One 16 bits header, followed by 127 samples

   enable_mux        <= enable(0) or enable(1) or enable(2) or enable(3);
   disactive_channel <= '1' when rst = '1' or enable_mux = '0' or pll_locked = '0' else '0';

   process (rx_syncclock, disactive_channel)
   begin

      if (disactive_channel = '1') then

         -- flag_fifo_wrData      <= '0';
         fifo_wrreq            <= '0';
         fifo_clear            <= '1';
         fifo_wrreq            <= '0';
         fill_fifo_counter     <= (others => '0');
         receive_ADCdata_state <= standby;

      elsif (rx_syncclock'event and rx_syncclock = '1') then

         fifo_clear <= '0';

         case receive_ADCdata_state is

            when standby =>

               fill_fifo_counter <= (others => '0');
               channel_fulldata  <= (others => '0');
               -- flag_fifo_wrData  <= '0';
               fifo_wrreq        <= '0';
               if (s_fifo_empty = '1') then
                  receive_ADCdata_state <= Initfifo;
               else
                  receive_ADCdata_state <= standby;
               end if;

            -- Write data header, one 16 bits word
            when Initfifo =>

               fill_fifo_counter     <= (others => '0');
               -- channel_fulldata      <= X"FFF" & "11" & channel_id(1 downto 0);--channel_id;
               channel_fulldata      <= channel_id;
               fifo_wrreq            <= '1';
               receive_ADCdata_state <= fillFifo_data;

            -- Write data, 127 16 bits words
            when fillFifo_data =>

               if (fill_fifo_counter < 127) then
                  -- channel_fulldata      <= X"0" & channel_id(1 downto 0) & std_logic_vector(fill_fifo_counter);--"00" & first_frame & second_frame;
                  channel_fulldata      <= "00" & first_frame & second_frame;
                  fifo_wrreq            <= '1';
                  fill_fifo_counter     <= fill_fifo_counter + 1;
                  receive_ADCdata_state <= fillFifo_data;
               else
                  fifo_wrreq            <= '0';
                  -- flag_fifo_wrData      <= '0';
                  channel_fulldata      <= (others => '0');
                  fill_fifo_counter     <= (others => '0');
                  receive_ADCdata_state <= standby;
               end if;

         end case;

      end if;

   end process;


   -- *** FIFO
   adc_channel_fifo_inst : adc_channel_fifo
      port map (aclr    => fifo_clear,
                data    => channel_fulldata,
                rdclk   => clock_adc,
                rdreq   => read_channelfifos,
                wrclk   => rx_syncclock,
                wrreq   => fifo_wrreq,
                q       => channel_data_out_fifo,
                rdempty => s_fifo_empty,
                wrfull  => open);

end architecture Behavioral;


--sample 7 last bits and fifo store
--process (rx_readclock,disactive_channel)
--begin
--  if (disactive_channel ='1') then
--    fifo_clear <= '1';
--       fifo_wrreq <= '0';
--    channel_fulldata <= (others => '0');
--
--  elsif (rx_readclock'event and rx_readclock ='1') then --rx_readclock
--    fifo_clear <= '0';
--
--       if fifo_full = '0' then
--         if(flag_fifo_wrData = '1') then
--                       if (enable(0) = '1') then
--                        channel_fulldata <=  "00" & channel_output1 & first_frame;
--           fifo_wrreq       <= '1';
--                       elsif (enable(1) = '1') then
--                        channel_fulldata <= "00" & channel_output2 & first_frame;
--           fifo_wrreq       <= '1';
--                       elsif (enable(2) = '1') then
--                   channel_fulldata <= "00" & channel_output3 & first_frame;
--           fifo_wrreq       <= '1';
--                       elsif (enable(3) = '1') then
--                   channel_fulldata <= "00" & channel_output4 & first_frame;
--           fifo_wrreq       <= '1';
--                       end if;
--              elsif(flag_fifo_init = '1') then
--         channel_fulldata <= channel_id;
--         fifo_wrreq       <= '1';
--      else
--              fifo_wrreq       <= '0';
--                 channel_fulldata <= (others => '0');
--              end if;
--    else
--          fifo_wrreq       <= '0';
--               channel_fulldata <= (others => '0');
--       end if;
--  end if;
--end process;
