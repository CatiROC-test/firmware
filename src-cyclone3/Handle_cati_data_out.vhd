-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Omega%20test%20board%20code][Omega test board code:1]]
-- ** Omega test board code

use work.cati_package.all;
-- @@@ Omega test board code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Entity][Entity:1]]
-- * Entity

entity Handle_cati_data_out is
   generic (output_nb     : std_logic             := '0';
            lenght_strobe : natural range 1 to 63 := 4);  -- 1 for channels from 8 to 15, otherwise 0
   port (rst               : in  std_logic;
         NewEvent          : out std_logic;
         test              : out std_logic_vector(10 downto 0);
         --cati
         clk_80MHz         : in  std_logic;  -- clk out @ 80 MHz
         cati_dataout      : in  std_logic;
         NewEventByChannel : out std_logic_vector(15 downto 0);
         cati_strobe       : in  std_logic;
         nbBitselect       : in  std_logic;  -- A 1 on prends 21bit de G Q et T sinon 11 juste G + Q
         decode_gray       : in  std_logic;  -- Set to 1 to decode to binary
         ---periferal
         fifo_dataout      : out std_logic_vector(63 downto 0);
         fifo_rd           : in  std_logic;
         fifo_empty        : out std_logic);
end entity Handle_cati_data_out;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of Handle_cati_data_out is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Signals][Signals:1]]
-- ** Signals

   signal Frame_select                          : std_logic                                  := '0';
   signal data_in                               : std_logic_vector (63 downto 0)             := (others => '0');
   signal q1, q2                                : std_logic_vector (47 downto 0)             := (others => '0');
   signal Fifo_data_in1, Fifo_data_in1_f1       : std_logic_vector (31 downto 0)             := (others => '0');
   signal Fifo_data_in1_f1_del                  : std_logic_vector (47 downto 0)             := (others => '0');
   signal Fifo_data_in2                         : std_logic_vector (31 downto 0)             := (others => '0');
   signal Fifo_data_in2_f2                      : std_logic_vector (47 downto 0)             := (others => '0');
   signal Fifo_data_out_frames                  : std_logic_vector (63 downto 0)             := (others => '0');
   signal Fifo_rden_frames                      : std_logic                                  := '0';
   signal Fifo_wreq                             : std_logic                                  := '0';
   signal Fifo_wreq_frame1, Fifo_wreq_frame1_f1 : std_logic                                  := '0';
   signal Fifo_wreq_frame1_f1_del               : std_logic                                  := '0';
   signal Fifo_wreq_frame2, Fifo_wreq_frame2_f2 : std_logic                                  := '0';
   signal Fifo_full                             : std_logic                                  := '0';
   signal Fifo_empty_frame2                     : std_logic                                  := '0';
   signal Frame_bitContor                       : unsigned (4 downto 0)                      := (others => '0');
   signal strobe                                : std_logic_vector(lenght_strobe-1 downto 0) := (others => '1');
   signal dataout                               : std_logic_vector(lenght_strobe-1 downto 0) := (others => '1');
   signal dataout_uniFramefifo_state            : dataout_fifo_state_type2;
   signal change_frame                          : std_logic                                  := '0';
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Event%20counter%20signals][Event counter signals:1]]
-- *** Event counter signals

   type EventCounterType is array(0 to 15) of unsigned(10 downto 0);
   signal EventCounters : EventCounterType     := (others => "000" & X"00");
   signal ChannelNb     : unsigned(3 downto 0) := (others => '0');
-- @@@ Event counter signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Generic][Generic:1]]
-- ** Generic

   test(10 downto 0) <= (others => '0');
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Falling%20edge%20detection][Falling edge detection:1]]
-- ** Falling edge detection

   process(clk_80MHz)
   begin
      if (clk_80MHz'event and clk_80MHz = '1') then
         if rst = '1' then
            strobe       <= (others => '1');
            dataout      <= (others => '1');
            Frame_select <= '0';
         else
            -- Shift register
            for i in lenght_strobe-1 downto 1 loop
               strobe(i)  <= strobe(i-1);
               dataout(i) <= dataout(i-1);
            end loop;
            strobe(0)  <= cati_strobe;
            dataout(0) <= cati_dataout;
            -- Falling edge detector toggles "frame select"
            -- Anti glitch detection with a long shift register
            if (strobe = X"FFFFF"&X"00000") then
               Frame_select <= not Frame_select;
            else
               Frame_select <= Frame_select;
            end if;
         end if;
      end if;
   end process;
-- @@@ Falling edge detection:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*New%20event%20detection][New event detection:1]]
-- ** New event detection

   process(clk_80MHz)
   begin
      if (clk_80MHz'event and clk_80MHz = '1') then
         if rst = '1' then
            NewEvent <= '0';
         else
            if (strobe = X"FFFFF"&X"00000") and (Frame_select = '0') then
               NewEvent <= '1';
            else
               NewEvent <= '0';
            end if;
         end if;
      end if;
   end process;
-- @@@ New event detection:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Data%20to%20fifo%20process][Data to fifo process:1]]
-- ** Data to fifo process

   process (clk_80MHz)
   begin

      if (clk_80MHz'event and clk_80MHz = '1') then

         if rst = '1' then

            Fifo_wreq_frame1 <= '0';
            Fifo_wreq_frame2 <= '0';
            Fifo_data_in1    <= X"ABCDABCD";
            Fifo_data_in2    <= X"ABCDABCE";
            Frame_bitContor  <= (others => '0');

         else

            -- frame 1
            if (strobe(lenght_strobe-1) = '0' and Frame_select = '1') then

               Fifo_wreq_frame2 <= '0';
               Fifo_data_in1    <= "00" & output_nb & Fifo_data_in1(27 downto 0) & dataout(lenght_strobe-1);
               if (Frame_bitContor = 28) then  --28
                  Frame_bitContor  <= (others => '0');
                  Fifo_wreq_frame1 <= '1';
               else
                  Frame_bitContor  <= Frame_bitContor + 1;
                  Fifo_wreq_frame1 <= '0';
               end if;

            -- frame 2
            elsif (strobe(lenght_strobe-1) = '0' and Frame_select = '0') then

               Fifo_wreq_frame1 <= '0';
               if (nbBitselect = '1') then

                  -- 21 bits frame
                  Fifo_data_in2 <= X"00" & "000" & Fifo_data_in2(19 downto 0) & dataout(lenght_strobe-1);
                  if (Frame_bitContor = 20) then  --20
                     Frame_bitContor  <= (others => '0');
                     Fifo_wreq_frame2 <= '1';
                  else
                     Frame_bitContor  <= Frame_bitContor + 1;
                     Fifo_wreq_frame2 <= '0';
                  end if;

               else

                  -- 11 bits frame
                  Fifo_data_in2 <= X"00000" & '0' & Fifo_data_in2(9 downto 0) & dataout(lenght_strobe-1);
                  if (Frame_bitContor = 10) then  --20
                     Frame_bitContor  <= (others => '0');
                     Fifo_wreq_frame2 <= '1';
                  else
                     Frame_bitContor  <= Frame_bitContor + 1;
                     Fifo_wreq_frame2 <= '0';
                  end if;
               end if;

            else

               Fifo_wreq_frame1 <= '0';
               Fifo_wreq_frame2 <= '0';
               Fifo_data_in1    <= (others => '1');
               Fifo_data_in2    <= (others => '1');
               Frame_bitContor  <= (others => '0');

            end if;

            -- if (strobe(lenght_strobe-1) = '1' and Frame_select = '0') then
            --    -- if Fifo_full_frame2 = '0' then
            --    --    enable <= '1';
            --    -- else
            --    --    enable <= '0';
            --    -- end if;
            --    enable <= not(Fifo_full_frame2);
            -- end if;

         end if;

      end if;

   end process;
-- @@@ Data to fifo process:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Gray%20decode%20frame%201][Gray decode frame 1:1]]
-- ** Gray decode frame 1

   process (clk_80MHz)
      variable Fifo_wreq_frame1_f1_v : std_logic                     := '0';
      variable Fifo_data_in1_f1_v    : std_logic_vector(31 downto 0) := (others => '0');
   begin
      if (clk_80MHz'event and clk_80MHz = '1') then
         if rst = '1' then
            Fifo_wreq_frame1_f1 <= '0';
            Fifo_data_in1_f1    <= (others => '1');
         else
            Fifo_wreq_frame1_f1_v            := Fifo_wreq_frame1;
            Fifo_data_in1_f1_v(31 downto 29) := Fifo_data_in1(31 downto 29);
            -- Nb. Channel
            Fifo_data_in1_f1_v(28)           := Fifo_data_in1(28);
            for i in 27 downto 26 loop
               Fifo_data_in1_f1_v(i) := Fifo_data_in1(i) xor Fifo_data_in1_f1_v(i+1);
            end loop;
            -- Coarse Time
            Fifo_data_in1_f1_v(25) := Fifo_data_in1(25);
            for i in 24 downto 0 loop
               Fifo_data_in1_f1_v(i) := Fifo_data_in1(i) xor Fifo_data_in1_f1_v(i+1);
            end loop;
            -- Take decoded value or not
            if decode_gray = '1' then
               Fifo_wreq_frame1_f1 <= Fifo_wreq_frame1_f1_v;
               Fifo_data_in1_f1    <= Fifo_data_in1_f1_v;
            else
               Fifo_wreq_frame1_f1 <= Fifo_wreq_frame1;
               Fifo_data_in1_f1    <= Fifo_data_in1;
            end if;
         end if;
      end if;
   end process;
-- @@@ Gray decode frame 1:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Event%20counter%20management][Event counter management:1]]
-- *** Event counter management

   ChannelNb <= unsigned(Fifo_data_in1_f1(29 downto 26));

   process (clk_80MHz)
   begin
      if (clk_80MHz'event and clk_80MHz = '1') then
         if rst = '1' then
            Fifo_data_in1_f1_del    <= X"ABCDABCDABCD";
            Fifo_wreq_frame1_f1_del <= '0';
         else
            Fifo_data_in1_f1_del <=
               X"0" & '0' &
               std_logic_vector(EventCounters(to_integer(ChannelNb))) &
               Fifo_data_in1_f1;
            Fifo_wreq_frame1_f1_del <= Fifo_wreq_frame1_f1;
         end if;
         if rst = '1' then
            EventCounters     <= (others => "000" & X"00");
            NewEventByChannel <= (others => '0');
         elsif Fifo_wreq_frame1_f1 = '1' then
            EventCounters(to_integer(ChannelNb)) <=
               EventCounters(to_integer(ChannelNb)) + 1;
            NewEventByChannel(to_integer(ChannelNb)) <= '1';
         else
            EventCounters(to_integer(ChannelNb)) <=
               EventCounters(to_integer(ChannelNb));
            NewEventByChannel <= (others => '0');
         end if;
      end if;
   end process;
-- @@@ Event counter management:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Gray%20decode%20frame%202][Gray decode frame 2:1]]
-- ** Gray decode frame 2

   process (clk_80MHz)
      variable Fifo_wreq_frame2_f2_v : std_logic                     := '0';
      variable Fifo_data_in2_f2_v    : std_logic_vector(31 downto 0) := (others => '0');
   begin
      if (clk_80MHz'event and clk_80MHz = '1') then
         if rst = '1' then
            Fifo_wreq_frame2_f2 <= '0';
            Fifo_data_in2_f2    <= (others => '1');
         else
            Fifo_wreq_frame2_f2_v            := Fifo_wreq_frame2;
            Fifo_data_in2_f2_v(31 downto 20) := Fifo_data_in2(31 downto 20);
            -- Time
            Fifo_data_in2_f2_v(19)           := Fifo_data_in2(19);
            for i in 18 downto 10 loop
               Fifo_data_in2_f2_v(i) := Fifo_data_in2(i) xor Fifo_data_in2_f2_v(i+1);
            end loop;
            -- ADC
            Fifo_data_in2_f2_v(9) := Fifo_data_in2(9);
            for i in 8 downto 0 loop
               Fifo_data_in2_f2_v(i) := Fifo_data_in2(i) xor Fifo_data_in2_f2_v(i+1);
            end loop;
            -- Take decoded value or not
            if decode_gray = '1' then
               Fifo_wreq_frame2_f2 <= Fifo_wreq_frame2_f2_v;
               Fifo_data_in2_f2    <= X"0000" & Fifo_data_in2_f2_v;
            else
               Fifo_wreq_frame2_f2 <= Fifo_wreq_frame2;
               Fifo_data_in2_f2    <= X"0000" & Fifo_data_in2;
            end if;
         end if;
      end if;
   end process;
-- @@@ Gray decode frame 2:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Fifo:%20frame%201][Fifo: frame 1:1]]
-- ** Fifo: frame 1
-- @@@ Fifo: frame 1:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Omega%20test%20board%20code][Omega test board code:1]]
-- *** Omega test board code

   Cati_frame1_fifo : Cati_frame_fifo
      port map (sclr        => rst,
                data        => Fifo_data_in1_f1_del,
                almost_full => open,
                clock       => clk_80MHz,
                rdreq       => Fifo_rden_frames,
                wrreq       => Fifo_wreq_frame1_f1_del,
                q           => q1,
                empty       => open,
                full        => open);
-- @@@ Omega test board code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Fifo:%20frame%202][Fifo: frame 2:1]]
-- ** Fifo: frame 2
-- @@@ Fifo: frame 2:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Omega%20test%20board%20code][Omega test board code:1]]
-- *** Omega test board code

   Cati_frame2_fifo : Cati_frame_fifo
      port map (sclr        => rst,
                data        => Fifo_data_in2_f2,
                almost_full => open,
                clock       => clk_80MHz,
                rdreq       => Fifo_rden_frames,
                wrreq       => Fifo_wreq_frame2_f2,
                q           => q2,
                empty       => Fifo_empty_frame2,
                full        => open);
-- @@@ Omega test board code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Merge%20data%20process][Merge data process:1]]
-- ** Merge data process

   process(clk_80MHz)
   begin

      if (clk_80MHz'event and clk_80MHz = '1') then

         if rst = '1' then

            -- do nothing
            Fifo_wreq                  <= '0';
            data_in                    <= (others => '0');
            Fifo_rden_frames           <= '0';
            dataout_uniFramefifo_state <= standby;

         else

            case dataout_uniFramefifo_state is
-- @@@ Merge data process:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Standby%20state][Standby state:1]]
-- *** Standby state

               when standby =>

                  Fifo_wreq <= '0';
                  data_in   <= (others => '0');
                  if (Fifo_full = '0' and Fifo_empty_frame2 = '0') then
                     Fifo_rden_frames           <= '1';
                     dataout_uniFramefifo_state <= write_tempo;
                  else
                     Fifo_rden_frames           <= '0';
                     dataout_uniFramefifo_state <= standby;
                  end if;
-- @@@ Standby state:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Write%20state][Write state:1]]
-- *** Write state

               when write_tempo =>

                  Fifo_wreq                  <= '1';
                  data_in                    <= q1(31 downto 0) & q2(20) & q1(42 downto 32) & q2(19 downto 0);
                  Fifo_rden_frames           <= '0';
                  dataout_uniFramefifo_state <= standby;

            end case;
         end if;
      end if;
   end process;
-- @@@ Write state:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Big%20FIFO][Big FIFO:1]]
-- ** Big FIFO
-- @@@ Big FIFO:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*Omega%20test%20board%20code][Omega test board code:1]]
-- *** Omega test board code

   Cati_unifFifo_inst1 : Cati_unifFifo
      port map (sclr  => rst,
                data  => data_in,
                clock => clk_80MHz,
                rdreq => fifo_rd,
                wrreq => Fifo_wreq,
                q     => fifo_dataout,
                empty => fifo_empty,
                full  => Fifo_full);
-- @@@ Omega test board code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/Handle_cati_data_out.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
