-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Entity][Entity:1]]
-- * Entity

entity external_trigger is
   port (clk                   : in  std_logic;
         main_rst              : in  std_logic;
         trig_ext              : out std_logic;
         params                : in  std_logic_vector(3 downto 0);
         EnableExternalTrigger : in  std_logic;
         enable                : in  std_logic;
         io_fpga1              : in  std_logic);
end entity external_trigger;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of external_trigger is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Signals][Signals:1]]
-- ** Signals

   signal counter_trig_ext : unsigned(15 downto 0) := (others => '0');
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Signals][Signals:2]]
-- ** Signals

   signal trig_ext_inner : std_logic := '0';  -- temp trig ext to be oored with external signal
-- @@@ Signals:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*Process][Process:1]]
-- ** Process

   U_trig_ext : process (clk) is
   begin
      if clk'event and clk = '1' then
         if main_rst = '1' then
            counter_trig_ext <= (others => '0');
            trig_ext_inner   <= '0';
         else
            if counter_trig_ext >= unsigned(params & X"800") then
               trig_ext_inner   <= enable;
               counter_trig_ext <= (others => '0');
            else
               trig_ext_inner   <= '0';
               counter_trig_ext <= counter_trig_ext + 1;
            end if;
         end if;
      end if;
   end process U_trig_ext;

   trig_ext <= trig_ext_inner or (io_fpga1 and EnableExternalTrigger);
-- @@@ Process:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/external_trigger.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
