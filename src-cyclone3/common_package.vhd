-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Package][Package:1]]
-- * Package

package common_package is
-- @@@ Package:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Types][Types:1]]
-- ** Types

   type TriggerCounters is array(0 to 31) of unsigned(15 downto 0);

   type dataout_fifo_state_type2 is (standby, write_tempo);

-- state signal in for wriiting in fifo
   type write_fifo_state is (write_standby, write_end, write_dataSignals, write_toFifo, write_countor);

-- state signal in SLOW control fsm
   type slow_control_write_state is (write_sl_standby, write_sl_resetb, write_sl_reset, write_sl, write_sl_tempo_1, write_sl_tempo_2, write_sl_probe, write_sl_run);
--type slow_control_read_state is (write_sl_standby,write_sl_reset,write_sl);

-- state signal in for wriiting in fifo
   type dataout_fifo_state_type is (standby, write_tempo);

   type mystate_type is (mystate_state_1, mystate_state_2);

   type inspec is array (7 downto 0) of std_logic_vector (23 downto 0);
-- @@@ Types:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Components][Components:1]]
-- ** Components
-- @@@ Components:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*USB%20Interface][USB Interface:1]]
-- *** USB Interface

   component usb_if is
      generic (RegBankLength   : natural;   -- RegBankLength bytes
               nb_asics        : natural;   -- bytes
               --
               p1_data_width   : natural;   -- bytes
               p2_data_width   : natural;   -- bytes
               p3_data_width   : natural;   -- bytes
               p4_data_width   : natural;
               p5_data_width   : natural;
               p6_data_width   : natural;   -- bytes
               --
               p1_params_width : natural;   -- bytes
               p2_params_width : natural;   -- bytes
               p3_params_width : natural;
               p4_params_width : natural;
               p5_params_width : natural;
               p6_params_width : natural);  -- bytes
      port (main_rst      : in    std_logic;
            interrupt_out : out   std_logic;
            BoardID       : in    std_logic_vector(7 downto 0);
            -- USB if
            usb_rxf       : in    std_logic;
            usb_txe       : in    std_logic;
            clk_2232      : in    std_logic;
            usb_oe        : out   std_logic;
            usb_rd        : out   std_logic;
            usb_wr        : out   std_logic;
            usb_siwua     : out   std_logic;
            usb_data      : inout std_logic_vector(7 downto 0);
            --
            disable_40MHz : out   std_logic;
            --
            -- P1
            p1_datain     : in    std_logic_vector(p1_data_width*8-1 downto 0);
            p1_params     : out   std_logic_vector(p1_params_width*8-1 downto 0);
            p1_rd         : out   std_logic;
            p1_empty      : in    std_logic;
            p1_enable     : out   std_logic;
            -- P2
            p2_datain     : in    std_logic_vector((p2_data_width+1)*8-1 downto 0);
            p2_params     : out   std_logic_vector(p2_params_width*8*nb_asics-1 downto 0);
            p2_rd         : out   std_logic;
            p2_empty      : in    std_logic;
            p2_enable     : out   std_logic;
            -- P3
            p3_datain     : in    std_logic_vector((p3_data_width+1)*8-1 downto 0);
            p3_params     : out   std_logic_vector(p3_params_width*8*nb_asics-1 downto 0);
            p3_rd         : out   std_logic;
            p3_empty      : in    std_logic;
            p3_enable     : out   std_logic;
            -- P4
            p4_datain     : in    std_logic_vector(p4_data_width*8-1 downto 0);
            p4_params     : out   std_logic_vector(p4_params_width*8-1 downto 0);
            p4_rd         : out   std_logic;
            p4_empty      : in    std_logic;
            p4_enable     : out   std_logic;
            -- P5
            p5_datain     : in    std_logic_vector(p5_data_width*8-1 downto 0);
            p5_params     : out   std_logic_vector(p5_params_width*8-1 downto 0);
            p5_rd         : out   std_logic;
            p5_empty      : in    std_logic;
            p5_enable     : out   std_logic;
            -- Peripheral 6
            p6_datain     : in    std_logic_vector(p6_data_width*8-1 downto 0);
            p6_params     : out   std_logic_vector(p6_params_width*8-1 downto 0);
            p6_rd         : out   std_logic;
            p6_empty      : in    std_logic;
            p6_enable     : out   std_logic;
            --Inspection
            px1_sel       : out   std_logic_vector(15 downto 0);
            px2_sel       : out   std_logic_vector(15 downto 0));
   end component usb_if;
-- @@@ USB Interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Catiroc][Catiroc:1]]
-- *** Catiroc

   component catiroc is
      generic (num_of_discri_channels : natural range 1 to 16;
               p2_data_width          : natural range 1 to 16);
      port (clk_10MHz         : in  std_logic;  -- generic oscilator present on board
            clk_80MHz         : in  std_logic;  -- generic oscilator present on board
            clk_40MHz         : in  std_logic;
            clk_480MHz        : in  std_logic;
            clk_120MHz        : in  std_logic;
            pll_locked        : in  std_logic;
            discri_falling    : out std_logic_vector(15 downto 0);
            ovf               : in  std_logic;
            main_rst          : in  std_logic;
            triggers          : in  std_logic_vector (15 downto 0);
            NewEvent          : out std_logic;
            NewEventByChannel : out std_logic_vector(15 downto 0);
            --cati
            resetb            : out std_logic;
            StartSys          : out std_logic;
            sr_clk            : out std_logic;
            sr_in             : out std_logic;  -- serialized data to configure cati
            sr_rst            : out std_logic;  -- reset cati before sending configuration
            sr_select         : out std_logic;  -- to be able to write in cati
            sr_out            : in  std_logic;  -- serialized data to read back conf sen
            -- usb
            clk_2232          : in  std_logic;  -- usb clock, 60 MHz
            p2_datain         : out std_logic_vector(p2_data_width*8-1 downto 0);
            p2_params         : in  std_logic_vector (7 downto 0);
            p3_datain         : out std_logic_vector(7 downto 0);
            p3_params         : in  std_logic_vector(527 downto 0);
            p3_read           : in  std_logic;
            p2_read           : in  std_logic;
            p3_empty          : out std_logic;
            p2_empty          : out std_logic;
            p3_enable         : in  std_logic;
            p2_enable         : in  std_logic;
            cati_dataout      : in  std_logic_vector (1 downto 0);
            cati_strobe       : in  std_logic_vector (1 downto 0));
   end component catiroc;
-- @@@ Catiroc:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Catiroc%20SlowControl][Catiroc SlowControl:1]]
-- *** Catiroc SlowControl

   component catiroc_slowcontrol is
      port (clk          : in  std_logic;  -- generic oscilator present on board
            main_rst     : in  std_logic;
            -- cati
            resetb       : out std_logic;
            start_system : out std_logic;
            sr_clk       : out std_logic;
            sr_in        : out std_logic;  -- serialized data to configure cati
            sr_rst       : out std_logic;  -- reset cati before sending configuration
            sr_select    : out std_logic;  -- to be able to write in cati
            sr_out       : in  std_logic;  -- serialized data to read back conf sen
            -- usb
            clk_2232     : in  std_logic;  -- usb clock, 60 MHz
            p3_params    : in  std_logic_vector(527 downto 0);
            usb_data_out : out std_logic_vector(7 downto 0);  -- usb Output bus (Send data to usb)
            usb_fifo1_rd : in  std_logic;  -- USB demande lecture de la fifo
            enable       : in  std_logic;  --PC demande l'ordre decriture et de lecture dans la fifo
            fifo_empty   : out std_logic);
   end component catiroc_slowcontrol;
-- @@@ Catiroc SlowControl:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Catiroc%20Data%20Capture][Catiroc Data Capture:1]]
-- *** Catiroc Data Capture

   component catiroc_data_capture is
      generic (num_of_discri_channels : natural range 1 to 16;
               p2_data_width          : natural range 1 to 16);
      port(Clk_usb           : in  std_logic;
           ovf               : in  std_logic;
           triggers          : in  std_logic_vector (15 downto 0);
           test              : out std_logic_vector(10 downto 0);
           NewEvent          : out std_logic;
           NewEventByChannel : out std_logic_vector(15 downto 0);
           clk_80Mhz         : in  std_logic;
           clk_40Mhz         : in  std_logic;
           clk_480MHz        : in  std_logic;
           clk_120MHz        : in  std_logic;
           pll_locked        : in  std_logic;
           discri_falling    : out std_logic_vector(15 downto 0);
           main_rst          : in  std_logic;
           resetb            : out std_logic;
           StartSys          : out std_logic;
           usb_params2       : in  std_logic_vector (7 downto 0);
           fifo_dataout      : out std_logic_vector(p2_data_width*8-1 downto 0);
           fifo_rd           : in  std_logic;
           fifo_empty        : out std_logic;
           p2_enable         : in  std_logic;
           cati_dataout      : in  std_logic_vector (1 downto 0);
           cati_strobe       : in  std_logic_vector (1 downto 0));
   end component catiroc_data_capture;
-- @@@ Catiroc Data Capture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Cati%20Data%20Out][Cati Data Out:1]]
-- *** Cati Data Out

   component cati_data_out is
      generic (nb_channels     : natural range 1 to 8         := 4;
               active_channels : std_logic_vector(7 downto 0) := "00000011");  -- no comment
      port (clk_in       : in  std_logic;   -- clk in @ X MHz
            rst          : in  std_logic;   -- init reset
            emulated     : in  std_logic;   -- select real or emulated data
            -- asic data
            --
            cati_dataout : in  std_logic;   -- data from asic
            cati_strobe  : in  std_logic;   -- strobe from asic
            --
            -- emulated data
            --
            clk_out      : out std_logic;   -- clk out @ 80 MHz
            dataout      : out std_logic;   -- serial data out
            strobe       : out std_logic);  -- data valid, active low
   end component cati_data_out;
-- @@@ Cati Data Out:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Handle_cati_data_out][Handle_cati_data_out:1]]
-- *** Handle_cati_data_out

   component Handle_cati_data_out is
      generic (output_nb     : std_logic             := '0';
               lenght_strobe : natural range 1 to 63 := 4);
      port (rst               : in  std_logic;
            test              : out std_logic_vector(10 downto 0);
            NewEvent          : out std_logic;
            --cati
            clk_80MHz         : in  std_logic;  -- clk out @ 80 MHz
            cati_dataout      : in  std_logic;
            NewEventByChannel : out std_logic_vector(15 downto 0);
            cati_strobe       : in  std_logic;
            nbBitselect       : in  std_logic;
            decode_gray       : in  std_logic;
            ---periferal
            fifo_dataout      : out std_logic_vector(63 downto 0);
            fifo_rd           : in  std_logic;
            fifo_empty        : out std_logic);
   end component Handle_cati_data_out;
-- @@@ Handle_cati_data_out:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Discriminator%20data][Discriminator data:1]]
-- *** Discriminator data

   component discriminator_data is
      generic (num_of_discri_channels : natural range 1 to 16);
      port (rst                     : in  std_logic;
            ovf                     : in  std_logic;
            resetb                  : in  std_logic;
            clk_80MHz               : in  std_logic;
            clk_40MHz               : in  std_logic;
            discri_rising           : in  std_logic_vector(15 downto 0);
            discri_falling          : in  std_logic_vector(15 downto 0);
            discri_data_f           : in  std_logic_vector(num_of_discri_channels*6-1 downto 0);
            discri_data_r           : in  std_logic_vector(num_of_discri_channels*6-1 downto 0);
            tstamp                  : in  std_logic_vector(31 downto 0);
            ReduceDiscriminatorData : in  std_logic;
            -- fifo if
            fifo_dataout            : out std_logic_vector(63 downto 0);
            fifo_rd                 : in  std_logic;
            fifo_empty              : out std_logic);
   end component discriminator_data;
-- @@@ Discriminator data:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*S%20curve][S curve:1]]
-- *** S curve

   component s_curve is
      generic (CounterLength : natural range 1 to 30;
               nb_channels   : natural range 1 to 16;
               asic_nb       : natural range 0 to 7);
      port (clk               : in  std_logic;
            NewEventByChannel : in  std_logic_vector(nb_channels-1 downto 0);
            main_rst          : in  std_logic;
            triggers          : in  std_logic_vector(15 downto 0);
            clk_2232          : in  std_logic;
            p_params          : in  std_logic_vector(31 downto 0);
            p_datain          : out std_logic_vector(127 downto 0);
            p_rd              : in  std_logic;
            p_enable          : in  std_logic;
            p_empty           : out std_logic);
   end component s_curve;
-- @@@ S curve:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*P1%20Counter][P1 Counter:1]]
-- *** P1 Counter

   component p1_counter is
      port (clk_fpga     : in  std_logic;
            main_rst     : in  std_logic;
            clk_2232     : in  std_logic;
            usb_data_in  : in  std_logic_vector(31 downto 0);
            usb_data_out : out std_logic_vector(127 downto 0);
            usb_fifo1_rd : in  std_logic;
            usb_wrreq    : in  std_logic;
            fifo_empty   : out std_logic);
   end component p1_counter;
-- @@@ P1 Counter:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*External%20trigger][External trigger:1]]
-- *** External trigger

   component external_trigger is
      port (clk                   : in  std_logic;
            main_rst              : in  std_logic;
            params                : in  std_logic_vector(3 downto 0);
            trig_ext              : out std_logic;
            EnableExternalTrigger : in  std_logic;
            enable                : in  std_logic;
            io_fpga1              : in  std_logic);
   end component external_trigger;
-- @@@ External trigger:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Catiroc%20usb%20if][Catiroc usb if:1]]
-- *** Catiroc usb if

   component catiroc_usb_if is
      generic (nb_asics             : natural range 1 to 8;
               input_bus_width_bits : natural range 1 to 128;
               extend               : integer range 0 to 1);
      port (main_rst        : in  std_logic;
            EnabledChannels : in  std_logic_vector(7 downto 0);
            asic_clk        : in  std_logic;
            asic_empty      : in  std_logic_vector(nb_asics-1 downto 0);
            asic_rd         : out std_logic_vector(nb_asics-1 downto 0);
            asic_data       : in  std_logic_vector(input_bus_width_bits*nb_asics-1 downto 0);
            fifo_clk        : in  std_logic;
            fifo_dataout    : out std_logic_vector((8*extend+input_bus_width_bits)-1 downto 0);
            fifo_rd         : in  std_logic;
            fifo_empty      : out std_logic);
   end component catiroc_usb_if;
-- @@@ Catiroc usb if:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*TDC][TDC:1]]
-- *** TDC

   component tdc
      generic (TDC_N : natural range 1 to 128 := 1);
      port (data_in_from_pins : in  std_logic_vector(TDC_N-1 downto 0);
            ovf               : in  std_logic;
            resetb            : in  std_logic;
            clk_sync          : in  std_logic;
            clk_f             : in  std_logic;
            clk_s             : in  std_logic;
            lock              : in  std_logic;
            tstamp            : out std_logic_vector(31 downto 0);
            main_rst          : in  std_logic;
            data_out_r        : out std_logic_vector(TDC_N*6-1 downto 0);
            data_out_f        : out std_logic_vector(TDC_N*6-1 downto 0);
            falling_out       : out std_logic_vector(TDC_N-1 downto 0);
            rising_out        : out std_logic_vector(TDC_N-1 downto 0);
            desout            : out std_logic_vector(7 downto 0));
   end component;
-- @@@ TDC:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*Inspection][Inspection:1]]
-- *** Inspection

   component inspection is
      port (input_i : in  inspec;
            sel     : in  std_logic_vector (15 downto 0);
            output  : out std_logic);
   end component inspection;
-- @@@ Inspection:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/common_package.org::*End%20package][End package:1]]
-- ** End package

end common_package;
-- @@@ End package:1 ends here
