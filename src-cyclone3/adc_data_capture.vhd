-----------------------------------------------------------------------
----- File Handle_adc_data.vhd
----- Top file for handling ADC 16 voies/14bit output
----- Author N.Alexis
----- Rev V1.0  Instanciation of component
-----
-----------------------------------------------------------------------

-- * Libraries
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Package
use work.cati_package.all;
use work.common_package.all;

-- * Entity
entity adc_data_capture is
   port (clk_fpga          : in  std_logic;  -- generic oscilator present on board
         main_rst          : in  std_logic;
         triggers          : in  std_logic_vector (15 downto 0);
         --ADC
         DCO               : in  std_logic_vector (1 downto 0);  -- clock to sample data
         FCO               : in  std_logic_vector (1 downto 0);
         adc_data          : in  std_logic_vector (15 downto 0);
         clock_adc         : in  std_logic;
         -- usb if
         clk_2232          : in  std_logic;  -- usb clock, 60 MHz
         usb_data_out_fifo : out std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
         usb_params        : in  std_logic_vector (15 downto 0);
         read_fifos        : in  std_logic;  --FIFO1 read request
         enable_data       : in  std_logic;  -- PC request w/r on fifo
         fifo_empty        : out std_logic);

end entity adc_data_capture;

-- * Architecture
architecture Behavioral of adc_data_capture is

   signal banks_datain                           : std_logic_vector(15 downto 0) := (others => '0');
   signal fifo_wreq                              : std_logic                     := '0';
   signal fifo_full                              : std_logic                     := '0';
   signal rst                                    : std_logic                     := '0';
   signal channel1_datain_b1, channel1_datain_b2 : std_logic_vector(15 downto 0);
   signal channel1_read_b1, channel1_read_b2     : std_logic;
   signal channel1_empty_b1, channel1_empty_b2   : std_logic;
   signal channel2_datain_b1, channel2_datain_b2 : std_logic_vector(15 downto 0);
   signal channel2_read_b1, channel2_read_b2     : std_logic;
   signal channel2_empty_b1, channel2_empty_b2   : std_logic;
   signal banksdata_fifo_state                   : dataout_fifo_state_type;
   signal samples_counter                        : unsigned(6 downto 0);
   signal channels_counter                       : unsigned(1 downto 0);

begin


   -- ** Rst
   --! @brief Module reset
   rst <= '1' when (main_rst = '1' or enable_data = '0') else '0';


   -- ** ADC Channels 0 to 7
   --! @brief ADC Data Capture
   --!
   --! Implements ...
   U_bank_0 : adc_data_capture_bank
      generic map (bank_id => '0')
      port map (clock_adc       => clock_adc,
                FCO             => FCO(0),
                rst             => rst,
                triggers        => triggers(7 downto 0),
                -- ADC
                adc_data        => adc_data(7 downto 0),
                channel1_datain => channel1_datain_b1,
                channel1_read   => channel1_read_b1,
                channel1_empty  => channel1_empty_b1,
                channel2_datain => channel2_datain_b1,
                channel2_read   => channel2_read_b1,
                channel2_empty  => channel2_empty_b1,
                usb_params      => usb_params(7 downto 0));


   -- ** ADC Channels 8 to 15
   --! @brief ADC Data Capture
   --!
   --! Implements ...
   U_bank_1 : adc_data_capture_bank
      generic map (bank_id => '1')
      port map (clock_adc       => clock_adc,
                FCO             => FCO(1),
                rst             => rst,
                triggers        => triggers(15 downto 8),
                -- ADC
                adc_data        => adc_data(15 downto 8),
                channel1_datain => channel1_datain_b2,
                channel1_read   => channel1_read_b2,
                channel1_empty  => channel1_empty_b2,
                channel2_datain => channel2_datain_b2,
                channel2_read   => channel2_read_b2,
                channel2_empty  => channel2_empty_b2,
                usb_params      => usb_params(15 downto 8));


   -- ** dfskjl
   process (clock_adc, rst)
   begin

      if (rst = '1') then

         banksdata_fifo_state <= standby;
         fifo_wreq            <= '0';
         channel1_read_b1     <= '0';
         channel2_read_b1     <= '0';
         channel1_read_b2     <= '0';
         channel2_read_b2     <= '0';
         banks_datain         <= (others => '0');
         samples_counter      <= (others => '0');
         channels_counter     <= (others => '0');

      elsif (clock_adc'event and clock_adc = '1') then

         case banksdata_fifo_state is

            when standby =>

               -- 0
               if (channels_counter = 0) then

                  if (channel1_empty_b1 = '0' and fifo_full = '0') then
                     fifo_wreq        <= '1';
                     banks_datain     <= channel1_datain_b1;
                     channel1_read_b1 <= '1';
                     if (samples_counter < 127) then
                        samples_counter      <= samples_counter +1;
                        banksdata_fifo_state <= write_tempo;
                     else
                        channels_counter     <= channels_counter + 1;
                        samples_counter      <= (others => '0');
                        banksdata_fifo_state <= write_tempo;
                     end if;
                  elsif (channel1_empty_b1 = '1' and samples_counter = 0) then
                     channels_counter <= channels_counter + 1;
                  end if;

               -- 1
               elsif(channels_counter = 1) then
                  if (channel2_empty_b1 = '0' and fifo_full = '0') then
                     fifo_wreq        <= '1';
                     banks_datain     <= channel2_datain_b1;
                     channel2_read_b1 <= '1';
                     if (samples_counter < 127) then
                        samples_counter      <= samples_counter +1;
                        banksdata_fifo_state <= write_tempo;
                     else
                        channels_counter     <= channels_counter + 1;
                        samples_counter      <= (others => '0');
                        banksdata_fifo_state <= write_tempo;
                     end if;
                  elsif (channel2_empty_b1 = '1' and samples_counter = 0) then
                     channels_counter <= channels_counter + 1;
                  end if;

               -- 2
               elsif(channels_counter = 2) then
                  if (channel1_empty_b2 = '0' and fifo_full = '0') then
                     fifo_wreq        <= '1';
                     banks_datain     <= channel1_datain_b2;
                     channel1_read_b2 <= '1';
                     if (samples_counter < 127) then
                        samples_counter      <= samples_counter +1;
                        banksdata_fifo_state <= write_tempo;
                     else
                        channels_counter     <= channels_counter + 1;
                        samples_counter      <= (others => '0');
                        banksdata_fifo_state <= write_tempo;
                     end if;
                  elsif (channel1_empty_b2 = '1' and samples_counter = 0) then
                     channels_counter <= channels_counter + 1;
                  end if;

               -- 3
               elsif(channels_counter = 3) then
                  if (channel2_empty_b2 = '0' and fifo_full = '0') then
                     fifo_wreq        <= '1';
                     banks_datain     <= channel2_datain_b2;
                     channel2_read_b2 <= '1';
                     if (samples_counter < 127) then
                        samples_counter      <= samples_counter +1;
                        banksdata_fifo_state <= write_tempo;
                     else
                        channels_counter     <= (others => '0');
                        samples_counter      <= (others => '0');
                        banksdata_fifo_state <= write_tempo;
                     end if;
                  elsif (channel2_empty_b2 = '1' and samples_counter = 0) then
                     channels_counter <= (others => '0');
                  end if;
               end if;

            when write_tempo =>

               channel1_read_b1     <= '0';
               channel2_read_b1     <= '0';
               channel1_read_b2     <= '0';
               channel2_read_b2     <= '0';
               fifo_wreq            <= '0';
               banks_datain         <= (others => '0');
               banksdata_fifo_state <= standby;

         end case;

      end if;

   end process;

   -- Main fifo to gather all the samples of all channels
   Output_fifo_adc : fifo_unifbanks
      port map (aclr    => rst,
                data    => banks_datain,
                rdclk   => clk_2232,
                rdreq   => read_fifos,
                wrclk   => clock_adc,
                wrreq   => fifo_wreq,
                q       => usb_data_out_fifo,
                rdempty => fifo_empty,
                wrfull  => fifo_full);

end architecture Behavioral;
