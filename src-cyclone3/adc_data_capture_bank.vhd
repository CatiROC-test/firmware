-----------------------------------------------------------------------
----- File Handle_bank.vhd
----- Top file for handling ADC 8 voies/14bit output
----- Author N.Alexis
----- Rev V1.0  Instanciation of component
-----
-----------------------------------------------------------------------

-- * Libraries
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Package
use work.cati_package.all;

-- * Entity
entity adc_data_capture_bank is
   generic(bank_id : std_logic := '0');
   port (clock_adc       : in  std_logic;
         FCO             : in  std_logic;  -- oscillator send by adc 50Mhz
         rst             : in  std_logic;
         triggers        : in  std_logic_vector (7 downto 0);
         --ADC IN
         adc_data        : in  std_logic_vector (7 downto 0);
         --ADC OUT
         channel1_datain : out std_logic_vector(15 downto 0);
         channel1_read   : in  std_logic;
         channel1_empty  : out std_logic;
         channel2_datain : out std_logic_vector(15 downto 0);
         channel2_read   : in  std_logic;
         channel2_empty  : out std_logic;
         -- usb
         usb_params      : in  std_logic_vector (7 downto 0));
end entity adc_data_capture_bank;

-- * Architecture
architecture Behavioral of adc_data_capture_bank is

   signal rx_inclock   : std_logic := '0';
   signal rx_readclock : std_logic := '0';
   signal rx_syncclock : std_logic := '0';
   signal pllLocked    : std_logic := '0';
   signal fifo_wreq    : std_logic := '0';
   signal fifo_full    : std_logic := '0';

   signal channels_datain : std_logic_vector(31 downto 0) := (others => '0');

begin

   -- ** Clock Management
   -- Generates clocks necessary to sync incoming data
   pll_channelsBank1 : pll_channels
      port map (areset => rst,
                inclk0 => FCO,
                c0     => rx_inclock,    -- 7 x input freq
                c1     => rx_syncclock,  -- 1 x input freq
                c2     => rx_readclock,  -- 2 x input freq
                locked => pllLocked);

   -- ** First group: channels 0 to 3
   U_group1 : adc_data_capture_bank_group
      generic map(channel_id => "00000000000000" & bank_id & '0')
      port map(rx_inclock            => rx_inclock,
               rx_readclock          => rx_readclock,
               rx_syncclock          => rx_syncclock,
               clock_adc             => clock_adc,
               rst                   => rst,
               trigger               => triggers(0),
               pll_locked            => pllLocked,
               --ADC
               adc_data              => adc_data(3 downto 0),
               enable                => usb_params(3 downto 0),
               -- usb
               channel_data_out_fifo => channel1_datain,
               read_channelfifos     => channel1_read,
               fifo_channel_empty    => channel1_empty);

   -- ** Second group: channels 4 to 7
   U_group2 : adc_data_capture_bank_group
      generic map(channel_id => "00000000000000" & bank_id & '1')
      port map(rx_inclock            => rx_inclock,
               rx_readclock          => rx_readclock,
               rx_syncclock          => rx_syncclock,
               clock_adc             => clock_adc,
               rst                   => rst,
               trigger               => triggers(1),
               pll_locked            => pllLocked,
               --ADC
               adc_data              => adc_data(7 downto 4),
               enable                => usb_params(7 downto 4),
               -- usb
               channel_data_out_fifo => channel2_datain,
               read_channelfifos     => channel2_read,
               fifo_channel_empty    => channel2_empty);

end architecture Behavioral;





--fifo_unifchannels_int : fifo_unifchannels PORT MAP (
--              aclr        => rst,
--              data        => channels_datain,
--              rdclk       => clock_toAdc_50Mhz,
--              rdreq       => read_channelsfifos,
--              wrclk       => clk_fromADC_50pll,
--              wrreq       => fifo_wreq,
--              q              => channels_data_out_fifo,
--              rdempty  => fifo_channels_empty,
--              wrfull   => fifo_full
--      );

--      GEN_Handle_channel: for i in 0 to 7 generate
--  Handle_channelX : Handle_channel
--  generic map(channel_id  => std_logic_vector(to_unsiged(i)) --000000000000" & bank_id & "000")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--        rst                      => rst,
--                trigger                  => triggers(i),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(i downto i),
--                enable                   => usb_params(i),
--        -- usb
--        channel_data_out_fifo    => channel1_datain,
--        read_channelfifos        => channel1_read,
--        fifo_channel_empty       => channel1_empty);
--   end generate GEN_REG;



-- Handle_channel3 : Handle_channel
--  generic map(channel_id  => "00000000000" & bank_id & "0011")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--                clock_toAdc_50Mhz        => clock_toAdc_50Mhz,
--        rst                      => rst,
--                trigger                  => triggers(2),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(2 downto 2),
--                enable                   => usb_params(2),
--        -- usb
--        channel_data_out_fifo    => channel3_datain,
--        read_channelfifos        => channel3_read,
--        fifo_channel_empty       => channel3_empty);
--
-- Handle_channel4 : Handle_channel
--  generic map(channel_id  => "00000000000" & bank_id & "0100")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--                clock_toAdc_50Mhz        => clock_toAdc_50Mhz,
--        rst                      => rst,
--                trigger                  => triggers(3),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(3 downto 3),
--                enable                   => usb_params(3),
--        -- usb
--        channel_data_out_fifo    => channel4_datain,
--        read_channelfifos        => channel4_read,
--        fifo_channel_empty       => channel4_empty);
--
-- Handle_channel5 : Handle_channel
--  generic map(channel_id  => "00000000000" & bank_id & "0101")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--                clock_toAdc_50Mhz        => clock_toAdc_50Mhz,
--        rst                      => rst,
--                trigger                  => triggers(4),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(4 downto 4),
--                enable                   => usb_params(4),
--        -- usb
--        channel_data_out_fifo    => channel5_datain,
--        read_channelfifos        => channel5_read,
--        fifo_channel_empty       => channel5_empty);
--
-- Handle_channel6 : Handle_channel
--  generic map(channel_id  => "00000000000" & bank_id & "0110")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--                clock_toAdc_50Mhz        => clock_toAdc_50Mhz,
--        rst                      => rst,
--                trigger                  => triggers(5),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(5 downto 5),
--                enable                   => usb_params(5),
--        -- usb
--        channel_data_out_fifo    => channel6_datain,
--        read_channelfifos        => channel6_read,
--        fifo_channel_empty       => channel6_empty);
--
-- Handle_channel7 : Handle_channel
--  generic map(channel_id  => "00000000000" & bank_id & "0111")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--                clock_toAdc_50Mhz        => clock_toAdc_50Mhz,
--        rst                      => rst,
--                trigger                  => triggers(6),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(6 downto 6),
--                enable                   => usb_params(6),
--        -- usb
--        channel_data_out_fifo    => channel7_datain,
--        read_channelfifos        => channel7_read,
--        fifo_channel_empty       => channel7_empty);
--
-- Handle_channel8 : Handle_channel
--  generic map(channel_id  => "00000000000" & bank_id & "1000")
--  port map(clock_adc_175Mhz      => serdes_inclock175,
--        clk_fromADC_50pll        => clk_fromADC_50pll,
--                clk_adc_25Mhz            => serdes_inclock25,
--                clock_toAdc_50Mhz        => clock_toAdc_50Mhz,
--        rst                      => rst,
--                trigger                  => triggers(7),
--                pll_locked               => pllLocked,
--        --ADC
--                adc_data                 => adc_data(7 downto 7),
--                enable                   => usb_params(7),
--        -- usb
--        channel_data_out_fifo    => channel8_datain,
--        read_channelfifos        => channel8_read,
--        fifo_channel_empty       => channel8_empty);

--read each channel fifo and store into a main fifo
--process (clk_fromADC_50pll,rst)
--begin
--  if (rst ='1') then
--    channelsdata_fifo_state <= standby;
--       fifo_wreq          <= '0';
--       channel1_read      <= '0';
--       channels_datain    <= (others => '0');
--
--  elsif (clk_fromADC_50pll'event and clk_fromADC_50pll ='1') then
--
--    case channelsdata_fifo_state is
--
--        when standby =>
--         if (channel1_empty = '0' and fifo_full = '0') then
--            fifo_wreq          <= '1';
--            channels_datain    <= channel1_datain;
--            channel1_read      <= '1';
--            channelsdata_fifo_state <= write_tempo;
--          else
--            channel1_read    <= '0';
--            fifo_wreq        <= '0';
--                              channels_datain  <= (others => '0');
--            channelsdata_fifo_state <= standby;
--          end if;
--
--        when write_tempo =>
--          channel1_read      <= '0';
--          fifo_wreq          <= '0';
--          channels_datain    <= (others => '0');
--          channelsdata_fifo_state <= standby;
--
--      end case;
--
--  end if;
--end process;
