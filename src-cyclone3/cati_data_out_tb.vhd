library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity cati_data_out_tb is

end entity cati_data_out_tb;

architecture fsdkl of cati_data_out_tb is

   -- component generics
   constant nb_channels : natural range 1 to 8 := 2;
   constant active_channels : std_logic_vector(7 downto 0) := "00000011";
   -- component ports
   signal Clk                      : std_logic                          := '0';
   signal main_rst                 : std_logic                          := '0';
   signal dataout, strobe, clk_out : std_logic                          := '0';
   signal init_standby_period      : unsigned(7 downto 0)               := (others => '0');
   signal fifo_dataout             : std_logic_vector(127 downto 0)     := (others => '0');
   signal fifo_rd                  : std_logic                          := '0';
   signal fifo_empty               : std_logic                          := '0';
   signal enable                   : std_logic                          := '0';
   signal emulated                 : std_logic                          := '0';
   signal cati_dataout             : std_logic                          := '0';  -- data from catiroc
   signal cati_strobe              : std_logic                          := '0';  -- strobe from catiroc

   component cati_data_out is
      generic (nb_channels : natural range 1 to 8;
              active_channels : std_logic_vector(7 downto 0));  -- active channels);
      port (clk_in       : in  std_logic;
            main_rst     : in  std_logic;
            emulated     : in  std_logic;
            enabled      : in  std_logic;  -- enable module
            cati_dataout : in  std_logic;  -- data from catiroc
            cati_strobe  : in  std_logic;  -- strobe from catiroc
            clk_out      : out  std_logic;
            dataout      : out  std_logic;
            strobe       : out std_logic);
   end component cati_data_out;

  component Handle_cati_data_out_sim is
     port (clk_in            : in  std_logic;                     -- clk in @ X MHz
           main_rst          : in  std_logic;
  		 --cati
           clk_data_80       : in std_logic;                     -- clk out @ 80 MHz
           cati_dataout      : in std_logic;
           cati_strobe       : in std_logic;
  		 ---periferal
           fifo_dataout      : out    std_logic_vector(127 downto 0);
           fifo_rd           : in   std_logic;
           fifo_empty        : out  std_logic;
           enable            : in   std_logic);

  end component Handle_cati_data_out_sim;


begin  -- architecture
enable <= '1';
emulated <= '1';
   -- ** Main reset
   main : process (Clk)
   begin
      if Clk'event and Clk = '1' then
         if init_standby_period /= X"0F" then
            init_standby_period <= init_standby_period + 1;
            main_rst            <= '1';
         else
            main_rst <= '0';
         end if;
      end if;
   end process main;

   -- ** Cati Data Out
   DUT : cati_data_out
      generic map (nb_channels => nb_channels,
                   active_channels => active_channels )
      port map (clk_in       => Clk,
                main_rst     => main_rst,
                emulated     => emulated,
                enabled      => enable,
                cati_dataout => cati_dataout,
                cati_strobe  => cati_strobe,
                clk_out      => clk_out,
                dataout      => dataout,
                strobe       => strobe);

   -- ** Cati Data Out
   DUT_2 : handle_cati_data_out_sim
      port map (clk_in        => Clk,
                main_rst      => main_rst,
				--cati
                clk_data_80   => clk_out,
                cati_dataout  => dataout,
                cati_strobe   => strobe,
				--peripheral
                fifo_dataout  =>  fifo_dataout,   --: out    std_logic_vector(127 downto 0);
                fifo_rd       =>  fifo_rd,   --: in   std_logic;
                fifo_empty    =>  fifo_empty,   --: out  std_logic;
                enable        =>  enable);	  --: in   std_logic);




   -- clock generation
   Clk <= not Clk after 25 ns;

end architecture fsdkl;

configuration cati_data_out_tb_fsdkl_cfg of cati_data_out_tb is
   for fsdkl
   end for;
end cati_data_out_tb_fsdkl_cfg;
