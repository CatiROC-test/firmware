-----------------------------------------------------------------------
----- File Handle_adc_spi.vhd
----- Top file for handling communication with ADC
----- Author N.Alexis
----- Rev V1.0  Instanciation of component
-----
-----------------------------------------------------------------------

-- * Libraries
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Package
use work.cati_package.all;

-- * Entity
entity adc_slow_control is
   port (main_rst         : in    std_logic;
         --ADC
         pwdn             : out   std_logic;
         SCLK_DTP         : in    std_logic;
         SDIO_DFS         : inout std_logic;
         CSB              : out   std_logic_vector (1 downto 0);
         -- usb
         clk_2232         : in    std_logic;  -- usb clock, 60 MHz
         usb_SPI_out_fifo : out   std_logic_vector(7 downto 0);  -- usb Output bus (Send data to usb)
         usb_params_SPI   : in    std_logic_vector (23 downto 0);
         read_fifos_SPI   : in    std_logic;
         enable_SPI       : in    std_logic;
         fifo_empty_SPI   : out   std_logic);

end entity adc_slow_control;

-- * Architecture
architecture Behavioral of adc_slow_control is

   signal rst                   : std_logic                     := '0';
   signal write_toSPI_state     : write_toSPI_type;
   signal read_toSPI_state      : read_toSPI_type;
   signal instruction_data      : std_logic_vector(23 downto 0) := (others => '0');
   signal transmit_data_countor : unsigned (4 downto 0)         := (others => '0');
   signal read_countor          : unsigned (3 downto 0)         := (others => '0');
   signal start_completion      : std_logic                     := '0';
   signal completion_end        : std_logic                     := '0';
   signal data_read             : std_logic_vector(7 downto 0)  := (others => '0');
   signal write_toFifo          : std_logic_vector(7 downto 0)  := (others => '0');
   signal fifo_wreq             : std_logic                     := '0';

begin

   rst <= '1' when main_rst = '1' or enable_SPI = '0' else '0';

   -- ** WRITE TO SPI
   process(SCLK_DTP, rst)
   begin

      if (rst = '1') then
         pwdn                  <= '0';
         instruction_data      <= (others => '0');
         transmit_data_countor <= (others => '0');
         CSB                   <= "11";
         start_completion      <= '0';
         write_toSPI_state     <= standby;

      elsif(SCLK_DTP'event and SCLK_DTP = '0') then

         case write_toSPI_state is

            when standby =>

               pwdn                  <= '1';
               CSB                   <= "11";
               instruction_data      <= (others => '0');
               transmit_data_countor <= (others => '0');
               start_completion      <= '0';
               write_toSPI_state     <= Init;

            -- *** WRITE
            when Init =>

               instruction_data <= usb_params_SPI(16)& "00" & "00000" & usb_params_SPI(15 downto 0);
               if(usb_params_SPI(16) = '0') then
                  write_toSPI_state <= InstuctionWrite_phase;
               else
                  write_toSPI_state <= InstuctionRead_phase;
               end if;

            when InstuctionWrite_phase =>

               if (transmit_data_countor < 24) then
                  CSB                   <= "00";
                  SDIO_DFS              <= instruction_data(23);
                  instruction_data      <= instruction_data(22 downto 0) & '0';
                  transmit_data_countor <= transmit_data_countor + 1;
                  write_toSPI_state     <= InstuctionWrite_phase;
               else
                  instruction_data      <= (others => '0');
                  transmit_data_countor <= (others => '0');
                  CSB                   <= "11";
                  write_toSPI_state     <= waitfor_reset;
               end if;

            -- *** READ
            when InstuctionRead_phase =>

               CSB <= "00";
               if (transmit_data_countor < 15) then
                  SDIO_DFS              <= instruction_data(23);
                  instruction_data      <= instruction_data(22 downto 0) & '0';
                  transmit_data_countor <= transmit_data_countor + 1;
                  write_toSPI_state     <= InstuctionRead_phase;
               else
                  SDIO_DFS          <= instruction_data(23);
                  instruction_data  <= instruction_data(22 downto 0) & '0';
                  write_toSPI_state <= wait_completion;
               end if;

            when wait_completion =>

               SDIO_DFS         <= 'Z';
               start_completion <= '1';
               if (completion_end = '1') then
                  CSB               <= "11";
                  write_toSPI_state <= waitfor_reset;
               else
                  CSB                   <= "00";
                  instruction_data      <= (others => '0');
                  transmit_data_countor <= (others => '0');
                  write_toSPI_state     <= wait_completion;
               end if;

            when waitfor_reset =>

               CSB               <= "11";
               start_completion  <= '0';
               pwdn              <= '0';
               write_toSPI_state <= waitfor_reset;

         end case;

      end if;

   end process;

   -- ** READ FROM SPI
   process(SCLK_DTP, rst)
   begin

      if (rst = '1') then

         completion_end   <= '0';
         data_read        <= (others => '0');
         read_countor     <= (others => '0');
         fifo_wreq        <= '0';
         write_toFifo     <= (others => '0');
         read_toSPI_state <= standby;

      elsif(SCLK_DTP'event and SCLK_DTP = '1')then

         case read_toSPI_state is

            when standby =>

               if (start_completion = '1') then
                  if(read_countor < 8) then
                     data_read        <= data_read(6 downto 0) & SDIO_DFS;
                     read_countor     <= read_countor +1;
                     completion_end   <= '0';
                     fifo_wreq        <= '0';
                     read_toSPI_state <= standby;
                  else
                     fifo_wreq        <= '1';
                     write_toFifo     <= data_read;
                     data_read        <= data_read;
                     completion_end   <= '1';
                     read_toSPI_state <= waitfor_reset;
                  end if;
               else
                  completion_end   <= '0';
                  fifo_wreq        <= '0';
                  read_countor     <= (others => '0');
                  data_read        <= (others => '0');
                  read_toSPI_state <= standby;
               end if;

            when waitfor_reset =>

               completion_end   <= '0';
               fifo_wreq        <= '0';
               read_countor     <= (others => '0');
               data_read        <= (others => '0');
               read_toSPI_state <= waitfor_reset;

         end case;

      end if;

   end process;

   -- ** FIFO
   fifo_spi_inst : fifo_spi
      port map (aclr    => rst,
                data    => write_toFifo,
                rdclk   => clk_2232,
                rdreq   => read_fifos_SPI,
                wrclk   => SCLK_DTP,
                wrreq   => fifo_wreq,
                q       => usb_SPI_out_fifo,
                rdempty => fifo_empty_SPI,
                wrfull  => open);

end architecture Behavioral;
