library IEEE;
use IEEE.STD_LOGIC_1164.all;

package cati_package is

   --state signal for SPI communication
   type write_toSPI_type is (standby, Init, InstuctionWrite_phase, InstuctionRead_phase, wait_completion, waitfor_reset);

   type read_toSPI_type is (standby, waitfor_reset);

   --state signal for ADC data reception/channel
   type receive_ADCdata_type is (standby, Initfifo, fillFifo_data);

   component Cati_frame_fifo is
      port (sclr        : in  std_logic;
            data        : in  std_logic_vector (47 downto 0);
            almost_full : out std_logic;
            clock       : in  std_logic;
            rdreq       : in  std_logic;
            wrreq       : in  std_logic;
            q           : out std_logic_vector (47 downto 0);
            empty       : out std_logic;
            full        : out std_logic);
   end component Cati_frame_fifo;

   component discri_small_fifo is
      port (sclr  : in  std_logic;
            data  : in  std_logic_vector (63 downto 0);
            clock : in  std_logic;
            rdreq : in  std_logic;
            wrreq : in  std_logic;
            q     : out std_logic_vector (63 downto 0);
            empty : out std_logic;
            full  : out std_logic);
   end component discri_small_fifo;

   component Cati_unifFifo is
      port (sclr  : in  std_logic;
            data  : in  std_logic_vector (63 downto 0);
            clock : in  std_logic;
            rdreq : in  std_logic;
            wrreq : in  std_logic;
            q     : out std_logic_vector (63 downto 0);
            empty : out std_logic;
            full  : out std_logic);
   end component Cati_unifFifo;

   --ADC interface
   component adc is
      port (clk_fpga          : in    std_logic;  -- generic oscilator present on board
            clock_adc         : in    std_logic;
            main_rst          : in    std_logic;
            triggers          : in    std_logic_vector (15 downto 0);
            --ADC
            pwdn              : out   std_logic;
            DCO               : in    std_logic_vector (1 downto 0);  -- clock to sample data
            FCO               : in    std_logic_vector (1 downto 0);
            adc_data          : in    std_logic_vector (15 downto 0);
            SCLK_DTP          : in    std_logic;
            SDIO_DFS          : inout std_logic;
            CSB               : out   std_logic_vector (1 downto 0);
            -- usb
            clk_2232          : in    std_logic;  -- usb clock, 60 MHz
            usb_data_out_fifo : out   std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
            usb_params_data   : in    std_logic_vector (15 downto 0);
            read_fifos_data   : in    std_logic;  --FIFO1 read request
            enable_data       : in    std_logic;
            fifo_empty_data   : out   std_logic;
            usb_SPI_out_fifo  : out   std_logic_vector(7 downto 0);  -- usb Output bus (Send data to usb)
            usb_params_SPI    : in    std_logic_vector (23 downto 0);
            read_fifos_SPI    : in    std_logic;
            enable_SPI        : in    std_logic;
            fifo_empty_SPI    : out   std_logic);
   end component adc;

   component adc_slow_control is
      port (main_rst         : in    std_logic;
            --ADC
            pwdn             : out   std_logic;
            SCLK_DTP         : in    std_logic;
            SDIO_DFS         : inout std_logic;
            CSB              : out   std_logic_vector (1 downto 0);
            -- usb
            clk_2232         : in    std_logic;  -- usb clock, 60 MHz
            usb_SPI_out_fifo : out   std_logic_vector(7 downto 0);  -- usb Output bus (Send data to usb)
            usb_params_SPI   : in    std_logic_vector (23 downto 0);
            read_fifos_SPI   : in    std_logic;
            enable_SPI       : in    std_logic;
            fifo_empty_SPI   : out   std_logic);

   end component adc_slow_control;

   component fifo_spi is
      port(aclr    : in  std_logic := '0';
           data    : in  std_logic_vector (7 downto 0);
           rdclk   : in  std_logic;
           rdreq   : in  std_logic;
           wrclk   : in  std_logic;
           wrreq   : in  std_logic;
           q       : out std_logic_vector (7 downto 0);
           rdempty : out std_logic;
           wrfull  : out std_logic);
   end component fifo_spi;

   component adc_data_capture is
      port (clk_fpga          : in  std_logic;  -- generic oscilator present on board
            main_rst          : in  std_logic;
            triggers          : in  std_logic_vector (15 downto 0);
            --ADC
            DCO               : in  std_logic_vector (1 downto 0);  -- clock to sample data
            FCO               : in  std_logic_vector (1 downto 0);
            adc_data          : in  std_logic_vector (15 downto 0);
            clock_adc         : in  std_logic;
            -- usb
            clk_2232          : in  std_logic;  -- usb clock, 60 MHz
            usb_data_out_fifo : out std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
            usb_params        : in  std_logic_vector (15 downto 0);
            read_fifos        : in  std_logic;  --FIFO1 read request
            enable_data       : in  std_logic;
            fifo_empty        : out std_logic);
   end component adc_data_capture;

   component pll is
      port(areset : in  std_logic := '0';
           inclk0 : in  std_logic := '0';
           c0     : out std_logic;
           c1     : out std_logic;
           c2     : out std_logic;
           c3     : out std_logic;
           c4     : out std_logic;
           locked : out std_logic);
   end component pll;

   component pll_adc is
      port(areset             : in  std_logic                     := '0';
           inclk0             : in  std_logic                     := '0';
           c0                 : out std_logic;
           c1                 : out std_logic;
           locked             : out std_logic;
           --
           phasecounterselect : in  std_logic_vector (2 downto 0) := (others => '0');
           phasestep          : in  std_logic                     := '0';
           phaseupdown        : in  std_logic                     := '0';
           scanclk            : in  std_logic                     := '1';
           phasedone          : out std_logic);
   end component pll_adc;

   component adc_data_capture_bank is
      generic(bank_id : std_logic := '0');
      port (clock_adc       : in  std_logic;
            FCO             : in  std_logic;  -- oscillator send by adc 50Mhz
            rst             : in  std_logic;
            triggers        : in  std_logic_vector (7 downto 0);
            --ADC
            adc_data        : in  std_logic_vector (7 downto 0);
            channel1_datain : out std_logic_vector(15 downto 0);
            channel1_read   : in  std_logic;
            channel1_empty  : out std_logic;
            channel2_datain : out std_logic_vector(15 downto 0);
            channel2_read   : in  std_logic;
            channel2_empty  : out std_logic;
            -- usb
            usb_params      : in  std_logic_vector (7 downto 0));
   end component adc_data_capture_bank;

   component pll_channels is
      port (areset : in  std_logic := '0';
            inclk0 : in  std_logic := '0';
            c0     : out std_logic;
            c1     : out std_logic;
            c2     : out std_logic;
            locked : out std_logic);
   end component pll_channels;

   component adc_data_capture_bank_group is
      generic(channel_id : std_logic_vector(15 downto 0) := "0000000000000000");
      port (rx_inclock            : in  std_logic;
            rx_readclock          : in  std_logic;  -- oscillator send by adc 50Mhz
            rx_syncclock          : in  std_logic;
            clock_adc             : in  std_logic;
            rst                   : in  std_logic;
            trigger               : in  std_logic;
            pll_locked            : in  std_logic;
            --ADC
            adc_data              : in  std_logic_vector (3 downto 0);
            -- usb
            enable                : in  std_logic_vector (3 downto 0);
            channel_data_out_fifo : out std_logic_vector(15 downto 0);  -- usb Output bus (Send data to usb)
            read_channelfifos     : in  std_logic;  --FIFO1 read request
            fifo_channel_empty    : out std_logic);
   end component adc_data_capture_bank_group;

   component Altlvdsrx is
      port (rx_data_reset : in  std_logic;
            rx_in         : in  std_logic_vector (0 downto 0);
            rx_inclock    : in  std_logic;
            rx_readclock  : in  std_logic;
            rx_syncclock  : in  std_logic;
            rx_out        : out std_logic_vector (6 downto 0));
   end component Altlvdsrx;

   component fifo_unifbanks is
      port (aclr    : in  std_logic := '0';
            data    : in  std_logic_vector (15 downto 0);
            rdclk   : in  std_logic;
            rdreq   : in  std_logic;
            wrclk   : in  std_logic;
            wrreq   : in  std_logic;
            q       : out std_logic_vector (127 downto 0);
            rdempty : out std_logic;
            wrfull  : out std_logic);
   end component fifo_unifbanks;

   component adc_channel_fifo is
      port (aclr    : in  std_logic := '0';
            data    : in  std_logic_vector (15 downto 0);
            rdclk   : in  std_logic;
            rdreq   : in  std_logic;
            wrclk   : in  std_logic;
            wrreq   : in  std_logic;
            q       : out std_logic_vector (15 downto 0);
            rdempty : out std_logic;
            wrfull  : out std_logic);
   end component adc_channel_fifo;

   -- CatiRoc Interface
   component handle_cati is
      port (clk_fpga          : in  std_logic;  -- generic oscilator present on board
            test              : out std_logic_vector(10 downto 0);
            main_rst          : in  std_logic;
            clk_40Mhz_pll     : out std_logic;
            clk_160Mhz_pll    : out std_logic;
            --cati
            pwr_on            : out std_logic;
            resetb            : out std_logic;
            reset_cati_strobe : out std_logic;
            start_system      : out std_logic;
            sr_clk            : out std_logic_vector (0 downto 0);
            sr_in             : out std_logic;  -- serialized data to configure cati
            sr_rst            : out std_logic;  -- reset cati before sending configuration
            sr_select         : out std_logic;  -- to be able to write in cati
            sr_out            : in  std_logic;  -- serialized data to read back conf sen
            -- usb
            clk_2232          : in  std_logic;  -- usb clock, 60 MHz

            usb_data_out_fifo1 : out std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
            usb_data_forFifo1  : in  std_logic_vector (31 downto 0);

            usb_data_out_fifo2 : out std_logic_vector(127 downto 0);
            usb_params2        : in  std_logic_vector (7 downto 0);

            usb_data_out_fifoSC : out std_logic_vector(7 downto 0);
            SlowContr_forCati   : in  std_logic_vector(527 downto 0);

            read_fifos        : in  std_logic_vector(2 downto 0);  --FIFO1 read request
            usb_request_fifos : in  std_logic_vector(2 downto 0);  -- PC request w/r on fifo
            fifos_empty       : out std_logic_vector(2 downto 0);

            cati_dataout : in std_logic_vector (1 downto 0);
            cati_strobe  : in std_logic_vector (1 downto 0));
   end component handle_cati;

   component Handle_FIFO is
      port (clk_fpga     : in  std_logic;  -- generic oscilator present on board
            main_rst     : in  std_logic;
            -- usb
            clk_2232     : in  std_logic;  -- usb clock, 60 MHz
            usb_data_in  : in  std_logic_vector(31 downto 0);  -- usb Input bus (receive data from usb)
            usb_data_out : out std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
            usb_fifo1_rd : in  std_logic;  -- USB demande lecture de la fifo
            usb_wrreq    : in  std_logic;  --PC demande l'ordre decriture et de lecture dans la fifo
            fifo_empty   : out std_logic);
   end component Handle_FIFO;

   component cati_dataout_fifo is
      port (aclr    : in  std_logic := '0';
            data    : in  std_logic_vector (63 downto 0);
            rdclk   : in  std_logic;
            rdreq   : in  std_logic;
            wrclk   : in  std_logic;
            wrreq   : in  std_logic;
            q       : out std_logic_vector (63 downto 0);
            rdempty : out std_logic;
            wrfull  : out std_logic);
   end component cati_dataout_fifo;

--  component Handle_cati_data_out_2 is
--     port (clk_in            : in  std_logic;                     -- clk in @ X MHz
--           main_rst          : in  std_logic;
--               --cati
--           clk_data_80       : in std_logic;                     -- clk out @ 80 MHz
--           cati_dataout      : in std_logic;
--           cati_strobe       : in std_logic;
--               ---periferal
--           fifo_dataout      : out  std_logic_vector(127 downto 0);
--           fifo_rd           : in   std_logic;
--           fifo_empty        : out  std_logic;
--           enable            : in   std_logic);
--
--  end component Handle_cati_data_out_2;

   component Cati_output_fifo1 is
      port(aclr    : in  std_logic := '0';
           data    : in  std_logic_vector (31 downto 0);
           rdclk   : in  std_logic;
           rdreq   : in  std_logic;
           wrclk   : in  std_logic;
           wrreq   : in  std_logic;
           q       : out std_logic_vector (127 downto 0);
           rdempty : out std_logic;
           wrfull  : out std_logic);
   end component Cati_output_fifo1;

   component Cati_output_fifoSC is
      port(aclr    : in  std_logic := '0';
           data    : in  std_logic_vector (7 downto 0);
           rdclk   : in  std_logic;
           rdreq   : in  std_logic;
           wrclk   : in  std_logic;
           wrreq   : in  std_logic;
           q       : out std_logic_vector (7 downto 0);
           rdempty : out std_logic;
           wrfull  : out std_logic);
   end component Cati_output_fifoSC;

   component altddio_syncSrClk is
      port(aclr     : in  std_logic;
           datain_h : in  std_logic_vector (0 downto 0);
           datain_l : in  std_logic_vector (0 downto 0);
           oe       : in  std_logic;
           outclock : in  std_logic;
           dataout  : out std_logic_vector (0 downto 0));
   end component altddio_syncSrClk;

end cati_package;
