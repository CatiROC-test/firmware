-----------------------------------------------------------------------
----- File Handle_cati.vhd
----- Top file for handling Catiroc output/input
----- Author N.Alexis
----- Rev V1.0  Instanciation of component cati_output1
-----
-----------------------------------------------------------------------

-- * Libraries
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Package
use work.cati_package.all;

-- * Entity
entity adc is
   port (clk_fpga          : in    std_logic;  -- generic oscilator present on board
         main_rst          : in    std_logic;
         triggers          : in    std_logic_vector (15 downto 0);
         -- ADC if
         clock_adc         : in    std_logic;
         pwdn              : out   std_logic;
         DCO               : in    std_logic_vector (1 downto 0);  -- clock to sample data
         FCO               : in    std_logic_vector (1 downto 0);
         adc_data          : in    std_logic_vector (15 downto 0);
         SCLK_DTP          : in    std_logic;
         SDIO_DFS          : inout std_logic;
         CSB               : out   std_logic_vector (1 downto 0);
         -- usb if: P4
         clk_2232          : in    std_logic;  -- usb clock, 60 MHz
         usb_data_out_fifo : out   std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
         usb_params_data   : in    std_logic_vector (15 downto 0);
         read_fifos_data   : in    std_logic;
         enable_data       : in    std_logic;
         fifo_empty_data   : out   std_logic;
         -- usb if: P5
         usb_SPI_out_fifo  : out   std_logic_vector(7 downto 0);  -- usb Output bus (Send data to usb)
         usb_params_SPI    : in    std_logic_vector (23 downto 0);
         read_fifos_SPI    : in    std_logic;
         enable_SPI        : in    std_logic;
         fifo_empty_SPI    : out   std_logic);
end entity adc;

-- * Architecture
architecture Behavioral of adc is

begin

   -- ** P4: Slow Control, "07" Command
   --! @brief Slow Control Management
   --!
   --! Implements Sending control registers to ADC,
   --! and reading them back to usb
   U_adc_slow_control : adc_slow_control
      port map(main_rst         => main_rst,
               -- ADC if
               pwdn             => pwdn,              -- out
               SCLK_DTP         => SCLK_DTP,          -- in
               SDIO_DFS         => SDIO_DFS,          -- inout
               CSB              => CSB,               -- out
               -- usb if P4
               clk_2232         => clk_2232,
               usb_SPI_out_fifo => usb_SPI_out_fifo,  -- out 8 bits, to usb
               usb_params_SPI   => usb_params_SPI,
               read_fifos_SPI   => read_fifos_SPI,
               enable_SPI       => enable_SPI,
               fifo_empty_SPI   => fifo_empty_SPI);


   -- ** P5: Data Capture, "08" Command
   --! @brief ADC Sample Capture Management
   --!
   --! Implements ...
   U_adc_data_capture : adc_data_capture
      port map (clk_fpga          => clk_fpga,
                main_rst          => main_rst,
                triggers          => triggers,
                -- ADC if
                DCO               => DCO,
                FCO               => FCO,
                adc_data          => adc_data,
                clock_adc         => clock_adc,
                -- usb if P5
                clk_2232          => clk_2232,
                usb_data_out_fifo => usb_data_out_fifo,
                usb_params        => usb_params_data,
                read_fifos        => read_fifos_data,
                enable_data       => enable_data,
                fifo_empty        => fifo_empty_data);

end architecture Behavioral;
