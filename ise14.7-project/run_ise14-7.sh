#!/usr/bin/sh

export LANG=
export LM_LICENSE_FILE=5296@ccflex04.in2p3.fr,5296@ccflex05.in2p3.fr,5296@ccflex06.in2p3.fr
source /opt/Xilinx/14.7/ISE_DS/settings64.sh > /tmp/nohup.out

intsytyle=ise # xflow, silent

# IMPACT_CMD_FILE=../board/$(BOARD)/impact.cmd
UCF_FILE=./top.ucf
# UT_FILE=../board/$(BOARD)/top.ut
PRJ_FILE=./top.prj
XST_FILE=./top.xst
TOP_FILE=../src-kindex7/top.vhd

TOP=$(cat $XST_FILE | grep "[-]top" | sed s/-top//g )
SOURCES=$(cat $PRJ_FILE | grep -oE '[^[:blank:]]+[[:blank:]]*$$' | tr -d '"' )

# ifneq "$(basename $TOP_FILE)" "$TOP"
# $(error Design top must have the same name as top-level source file)
# endif

# cd ise14.7-project

#############
# SYNTHESIS #
#############

# In files
# $TOP.xst :: xst configuration
# Out files
# $TOP.syr ::

# xst -intstyle $intsytyle -ifn $TOP.xst -ofn $TOP.syr
# [[ $? != 0 ]] && printf "\nXST ERROR.\n" && exit 1

#############
# TRANSLATE #
#############

# Out files
# $TOP.bld ::

# ngdbuild -intstyle $intsytyle -dd _ngo -nt timestamp -uc $TOP.ucf -p xc6vlx240t-ff1156-1 $TOP.ngc $TOP.ngd

ngdbuild -intstyle ise -dd _ngo -sd cores/pll -sd cores/Cati_output_fifo1 -sd \
cores/Cati_output_fifoSC -sd cores/Cati_frame_fifo -sd cores/Cati_unifFifo -sd \
cores/fifo_128 -sd cores/fifo_8 -sd cores/cati_dataout_fifo -sd \
cores/discri_small_fifo -nt timestamp -uc top.ucf -p xc6vlx240t-ff1156-1 top.ngc top.ngd

Loading design module "cores/cati_dataout_fifo/cati_dataout_fifo.ngc"...
Loading design module "cores/discri_small_fifo/discri_small_fifo.ngc"...
Loading design module "cores/Cati_unifFifo/Cati_unifFifo.ngc"...
Loading design module "cores/Cati_frame_fifo/Cati_frame_fifo.ngc"...
Loading design module "cores/Cati_output_fifoSC/Cati_output_fifoSC.ngc"...
Loading design module "cores/Cati_output_fifo1/Cati_output_fifo1.ngc"...
Loading design module "cores/fifo_128/fifo_128.ngc"...
Loading design module "cores/fifo_8/fifo_8.ngc"...

[[ $? != 0 ]] && printf "\nTRANSLATE ERROR.\n" && exit 1

exit 0

###########
# MAPPING #
###########

# Out files
# $TOP.mrp :: map repport

map -intstyle $intsytyle -p xc6vlx240t-ff1156-1 -w -logic_opt off -ol high -t 1 -xt 0 -register_duplication off -r 4 -global_opt off -mt off -ir off -pr off -lc off -power off -o $TOP_map.ncd $TOP.ngd $TOP.pcf
[[ $? != 0 ]] && printf "\nMAPPING ERROR.\n" && exit 1

#########
# P & R #
#########

# Out files
# $TOP.par ::

par -w -intstyle $intsytyle -ol high -mt off $TOP_map.ncd $TOP.ncd $TOP.pcf
[[ $? != 0 ]] && printf "\nPLACE AND ROUTE ERROR.\n" && exit 1

#########
# TRCE #
#########

# Out files
# $TOP.twr ::

trce -intstyle $intsytyle -v 3 -s 1 -n 3 -fastpaths -xml $TOP.twx $TOP.ncd -o $TOP.twr $TOP.pcf
[[ $? != 0 ]] && printf "\nTRACE ERROR.\n" && exit 1



##########
# BitGen #
##########

# In files
# riffa_top_v6_pcie_v2_5.ut      :: bitgen config

bitgen -intstyle ise -f $TOP.ut $TOP.ncd
[[ $? != 0 ]] && printf "\nBITGEN ERROR.\n" && exit 1

##########
# Impact #
##########

# cd impact
# impact -batch mycmd.cmd

########
# Misc #
########

# riffa_top_v6_pcie_v2_5.cmd_log :: commands executed from within ise
