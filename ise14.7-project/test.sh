#!/usr/bin/sh

export LANG=
export LM_LICENSE_FILE=5296@ccflex04.in2p3.fr,5296@ccflex05.in2p3.fr,5296@ccflex06.in2p3.fr
source /opt/Xilinx/14.7/ISE_DS/settings64.sh > /tmp/nohup.out

/opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64/xst -intstyle ise -ifn "/home/csantos/Projects/apc/juno/CatiROC-test-firmware/ise14.7-project/top.xst" -ofn "/home/csantos/Projects/apc/juno/CatiROC-test-firmware/ise14.7-project/top.syr"

# ngdbuild -intstyle ise -dd _ngo -sd cores/Cati_output_fifo1 -sd cores/Cati_output_fifoSC -sd cores/Cati_frame_fifo -sd cores/Cati_unifFifo -sd cores/fifo_128 -sd cores/fifo_8 -sd cores/cati_dataout_fifo -sd cores/discri_small_fifo -sd cores/pll -nt timestamp -uc top.ucf -p xc7k325t-fbg676-1 top.ngc top.ngd

/opt/Xilinx/14.7/ISE_DS/ISE/bin/lin64/unwrapped/ngdbuild \
    -intstyle ise -dd _ngo -sd cores/pll -sd cores/Cati_output_fifo1 -sd \
    cores/Cati_output_fifoSC -sd cores/Cati_frame_fifo -sd cores/Cati_unifFifo -sd \
    cores/fifo_128 -sd cores/fifo_8 -sd cores/cati_dataout_fifo -sd \
    cores/discri_small_fifo -nt timestamp -uc top.ucf -p xc6vlx240t-ff1156-1 top.ngc top.ngd
