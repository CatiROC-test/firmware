#************************************************************
# THIS IS A WIZARD-GENERATED FILE.
#
# Version 13.1.0 Build 162 10/23/2013 SJ Full Version
#
#************************************************************

# Copyright (C) 1991-2013 Altera Corporation
# Your use of Altera Corporation's design tools, logic functions
# and other software and tools, and its AMPP partner logic
# functions, and any output files from any of the foregoing
# (including device programming or simulation files), and any
# associated documentation or information are expressly subject
# to the terms and conditions of the Altera Program License
# Subscription Agreement, Altera MegaCore Function License
# Agreement, or other applicable license agreement, including,
# without limitation, that your use is for the sole purpose of
# programming logic devices manufactured by Altera and sold by
# Altera or its authorized distributors.  Please refer to the
# applicable agreement for further details.



# Clock constraints

create_clock -name "clk_2232_name" -period 16.000ns [get_ports {clk_2232}]
create_clock -name "clk_fpga_name" -period 25.000ns [get_ports {clk_fpga}]
create_clock -name "clk_40Mhz_name" -period 25.000ns [get_ports {clk_40Mhz_pll}]
create_clock -name "clk_160Mhz_name" -period 6.250ns [get_ports {clk_160Mhz_pll}]

# create_clock -name "FCO[1]" -period 100.000ns [get_ports {FCO[1]]}]
# create_clock -name "FCO[0]" -period 100.000ns [get_ports {FCO[0]]}]

# Automatically constrain PLL and other generated clocks
derive_pll_clocks -create_base_clocks

# Automatically calculate clock uncertainty to jitter and other effects.
derive_clock_uncertainty

# tsu/th constraints

# tco constraints

# tpd constraints

# csb
set_false_path -from [get_clocks {clk_2232_name}] -to [get_clocks {clk_fpga_name}]
set_false_path -from [get_clocks {clk_2232_name}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_clocks {clk_2232_name}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[1]}]
set_false_path -from [get_clocks {clk_2232_name}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path -from [get_clocks {clk_2232_name}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[3]}]
set_false_path -from [get_clocks {clk_2232_name}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[4]}]

# set_false_path -from [get_cells {main_rst}] -to [get_clocks {clk_2232_name}]

set_false_path -from [get_cells {usb_if:U_usb_if|my_RegBank}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[0]}]
set_false_path -from [get_cells {usb_if:U_usb_if|my_RegBank}] -to [get_clocks {pll_inst|altpll_component|auto_generated|pll1|clk[3]}]

set_false_path -from [get_registers {catiroc:U1|catiroc_data_capture:U_Handle_dataout_top|rst}] -to [get_registers {catiroc:U1|catiroc_data_capture:U_Handle_dataout_top|cati_dataout_fifo:cati_dataout_fifo_inst*}]

set_false_path -from [get_registers {catiroc:U1|catiroc_data_capture:U_Handle_dataout_top|rst}] -to [get_registers {catiroc:U1|catiroc_data_capture:U_Handle_dataout_top|cati_dataout_fifo:cati_dataout_fifo_inst*}]

set_false_path -from [get_registers {usb_if:U_usb_if|disable_40MHz}] -to [get_pins {altddio_syncSrClk_inst|ALTDDIO_OUT_component|auto_generated|ddio_outa[0]|areset altddio_syncSrClk_shifted|ALTDDIO_OUT_component|auto_generated|ddio_outa[0]|areset}]
# csb