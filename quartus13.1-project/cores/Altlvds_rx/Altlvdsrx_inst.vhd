Altlvdsrx_inst : Altlvdsrx PORT MAP (
		rx_data_reset	 => rx_data_reset_sig,
		rx_in	 => rx_in_sig,
		rx_inclock	 => rx_inclock_sig,
		rx_readclock	 => rx_readclock_sig,
		rx_syncclock	 => rx_syncclock_sig,
		rx_out	 => rx_out_sig
	);
