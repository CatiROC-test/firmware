-- * Topv0

--! @file
--! @brief Topmost hierarchy level for v0.
--!
--! Its solely purpose is to wrap the top core module, common to v0 and v1.
--! It handles any difference between v0 and v1, namely:
--!
--! - clock selection
--! - any other ...
--!
--! @section clkselection Clock selection
--!
--! Forces using local oscillator clock
--!
--! @section anyother Any Other
--!
--! Any other customization comes here ...
--!
--! \class topv0

-- * Libraries

library ieee;

-- * Packages

use ieee.std_logic_1164.all;
use work.common_package.all;

-- * Entity

--! @brief Topmost hierarchy level for v0 entity.
--!
--! It includes all ports in top core module related to the hardware.\n
--! Ports in core module used for taking into account differences between v0 and
--! v1 are not mapped.\n\n
--!
--! Three groups of signals exists:
--!
--! - clocking
--! - interfaces (asic and usb)
--! - inspection lines
--!
--! @section sec_topv0_clocking Clocking
--!
--! All is clear, I guess ...
--!
--! @section sec_topv0_interfaces Interfaces
--!
--! Don’t know what to say ...
--!
--! @section sec_topv0_insplines Inspection lines
--!
--! Don’t know what to say ...

entity topv0 is
   -- ** Ports
   port (
      -- *** I/O if
      --!\name I/O if
      --!\{ Interface to usb external controller and to nb_asics asics
      usb_if         : inout rc_usb_if;  --! Usb interface
      catirocs       : inout rc_catiroc_array(0 to nb_asics-1);  --! all asics if

      --!\}

      -- *** Clocks
      --!\name Clocks
      --!\{
      --! I/0 clocking resources
      clk_oscillator : in    std_logic;  --! generic oscillator present on board 40MHz
      clk_gcu        : in    std_logic;  --! clock from gcu

      --!\}

      -- *** Inspection lines
      --!\name Inspection lines
      --!\{
      --! I/0 Testing points
      PX1            : out   std_logic;
      PX2            : out   std_logic;
      PX3            : in    std_logic;
      PX4            : out   std_logic
      );
--!\}
end entity topv0;

-- * Architecture

--! @brief Just instantiate the averager module.
--!
--! Details of the architecture.\n
--! Simpler that this, you die !.\n
--!
--! @section sec1 Section1
--!
--! All is clear, I guess ...
--!
--! @section sec2 Section2
--!
--! Don’t know what to say ...

architecture Behavioral of topv0 is

begin

   -- ** Top

   --! @brief Implements a simple, general purpose moving averaging.
   --!
   --! @details It delays the incoming sampling signals, sampling them in parallel.\n
   --! Then, takes the difference of the two parallel streams\n
   --! accumulating the difference.\n
   --! Finally, redimensions the results to compute the division by the window
   --! lenght
   --!
   --! @section sec1 Section1
   --!
   --! All is clear, I guess ...
   --!
   --! @section sec2 Section2
   --!
   --! Don’t know what to say ...

   u_top : entity work.top
      port map (usb_if         => usb_if,          -- usb if
                catirocs       => catirocs,        -- asics if
                clk_oscillator => clk_oscillator,  -- Local oscillator
                clk_gcu        => clk_gcu,         -- clock from gcu
                clk_select     => '1',  -- selects between clk_fpga (1) and clk_gcu (0)
                -- fpga_purpose_80
                PX1            => PX1,
                PX2            => PX2,
                PX3            => PX3,
                PX4            => PX4);

end architecture Behavioral;
