-- * TDC

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

-- * Entity

entity tdc is
  generic (TDC_N : natural range 1 to 128 := 1);
  port (data_in_from_pins : in  std_logic_vector(TDC_N-1 downto 0);
        ovf               : in  std_logic;
        resetb            : in  std_logic;
        clk_sync          : in  std_logic;
        clk_f             : in  std_logic;
        clk_s             : in  std_logic;
        lock              : in  std_logic;
        tstamp            : out std_logic_vector(31 downto 0);
        main_rst          : in  std_logic;
        data_out_r        : out std_logic_vector(TDC_N*6-1 downto 0);
        data_out_f        : out std_logic_vector(TDC_N*6-1 downto 0);
        falling_out       : out std_logic_vector(TDC_N-1 downto 0);
        rising_out        : out std_logic_vector(TDC_N-1 downto 0);
        desout            : out std_logic_vector(7 downto 0));
end tdc;

-- * Architecture

architecture Behavioral of tdc is

  type des_out_a is array (0 to TDC_N-1) of std_logic_vector(7 downto 0);
  type tdc_out_a is array (0 to TDC_N-1) of std_logic_vector(23 downto 0);
  type tdc_out_e_a is array (0 to TDC_N-1) of std_logic_vector(24 downto 0);
  type tdc_enc_a is array (0 to TDC_N-1) of std_logic_vector(5 downto 0);

  signal tdc_out_r                                                  : std_logic_vector(TDC_N-1 downto 0) := (others => '1');
  signal mask                                                       : std_logic_vector(TDC_N-1 downto 0) := (others => '0');
  signal des_out                                                    : des_out_a                          := (others => (others => '0'));
  signal tdc_out, tdc_redge, tdc_fedge                              : tdc_out_a                          := (others => (others => '0'));
  signal tdc_out_e                                                  : tdc_out_e_a                        := (others => (others => '0'));
  signal tdc_r_enc, tdc_f_enc                                       : tdc_enc_a                          := (others => (others => '0'));
  signal cnt_sync                                                   : std_logic_vector(47 downto 0)      := (others => '0');
  signal rst_io, rst_io_auto, rst_out                               : std_logic                          := '0';
  signal data_to_GT                                                 : std_logic_vector(24 downto 0)      := (others => '0');
  signal strobe_to_GT                                               : std_logic_vector(0 downto 0)       := (others => '0');
  signal trg_out, trg_or, trg_or_l, trg_or_p                        : std_logic_vector(0 downto 0)       := (others => '0');
  signal trg_cnt, trg_rate_cnt, trg_rate                            : std_logic_vector(25 downto 0)      := (others => '0');
  signal clk_sync_probe                                             : std_logic_vector(0 downto 0)       := (others => '0');
  signal tdc_enc_deb                                                : std_logic_vector(689 downto 0)     := (others => '0');
  signal clk_out                                                    : std_logic                          := '0';
  signal locked, rst_vio, rst_io_vio                                : std_logic_vector(0 downto 0)       := (others => '0');
  signal trigger_bx_1, trigger_bx_2                                 : std_logic_vector(19 downto 0)      := (others => '0');
  signal trigger, trigger_l                                         : std_logic_vector(0 downto 0)       := (others => '0');
  signal tdc_enc_empty_1                                            : std_logic_vector(399 downto 0)     := (others => '0');
  signal tdc_enc_empty_2                                            : std_logic_vector(289 downto 0)     := (others => '0');
  type layer_t is array (0 to 19) of std_logic_vector(4 downto 0);
  type layer_a is array (0 to 7) of layer_t;
  signal layer                                                      : layer_a                            := (others => (others => (others => '0')));
  signal layer_empty                                                : layer_t                            := (others => (others => '0'));
  type trigger_bx_t is array (0 to 7) of std_logic_vector(39 downto 0);
  signal trigger_bx                                                 : trigger_bx_t                       := (others => (others => '0'));
  signal l1_delay                                                   : std_logic_vector(6 downto 0);
  signal trg_delay                                                  : std_logic_vector(127 downto 0);
  signal trg_src                                                    : std_logic_vector(1 downto 0)       := (others => '0');
  signal coincidence                                                : std_logic_vector(3 downto 0)       := (others => '0');
  signal coincidence_deb, ttc_l1a_deb                               : std_logic_vector(0 downto 0)       := (others => '0');
  signal tp                                                         : std_logic_vector(1 downto 0)       := (others => '0');
  signal tp_cnt                                                     : std_logic_vector(15 downto 0)      := (others => '0');
  signal tp_d, tp_d_l, tp_prescale, tp_d_2, tp_d_2_l, tp_prescale_2 : std_logic                          := '0';
  signal rise, fall                                                 : std_logic_vector(0 to TDC_N-1)     := (others => '0');
  signal time_s                                                     : unsigned(31 downto 0)              := (others => '0');
  signal bitslip                                                    : std_logic_vector(0 downto 0)       := (others => '0');

begin

  --------- Reset SerDes -------------------------------------------------------------

  rst_des_p : process(main_rst, clk_s, lock)
    variable cnt : integer range 0 to 31 := 0;
  begin
    if main_rst = '1' then
      rst_io_auto <= '0';
      cnt         := 0;
    elsif (clk_s = '1' and clk_s'event) then
      if lock = '1' and cnt < 12 then
        rst_io_auto <= '1';
        rst_out     <= '1';
        cnt         := cnt + 1;
      elsif lock = '1' and cnt < 24 then
        rst_io_auto <= '0';
        rst_out     <= '1';
        cnt         := cnt + 1;
      else
        rst_io_auto <= '0';
        rst_out     <= '0';
        cnt         := cnt;
      end if;
    end if;
  end process;

  ------------------------------------------------------------------------------------

  des_g : for i in 0 to TDC_N-1 generate
    des_i : entity work.selectio_wiz_0
      port map (data_in_from_pins(0) => data_in_from_pins(i),
                clk_in               => clk_f,
                clk_div_in           => clk_s,
                io_reset             => rst_io,
                bitslip              => bitslip,
                data_in_to_device    => des_out(i));
  end generate;

  shift_word_p : process(clk_s, main_rst, des_out)
  begin
    if main_rst = '1' then
      tdc_out <= (others => (others => '0'));
    elsif clk_s'event and clk_s = '1' then
      for i in 0 to TDC_N-1 loop
        tdc_out(i)(15 downto 0)  <= tdc_out(i)(23 downto 8);
        tdc_out(i)(23 downto 16) <= des_out(i);
      end loop;
    end if;
  end process;

  tdc_out_e_g : for i in 0 to TDC_N-1 generate
  begin
    tdc_out_e(i) <= tdc_out(i) & tdc_out_r(i);
  end generate;

  tdc_edge_g_g : for i in 0 to TDC_N-1 generate
    tdc_edge_g : for j in 0 to 23 generate
      tdc_redge(i)(j) <= '1' when tdc_out_e(i)(j+1) = '1' and tdc_out_e(i)(j) = '0' else
                         '0';
      tdc_fedge(i)(j) <= '1' when tdc_out_e(i)(j+1) = '0' and tdc_out_e(i)(j) = '1' else
                         '0';
    end generate;
  end generate;

  encoder_p : process (clk_sync, rst_out, tdc_redge, tdc_fedge, tdc_out)
    type tdc_out_enc_a is array (0 to TDC_N-1) of integer range 0 to 24;
    variable tdc_rout_enc, tdc_fout_enc : tdc_out_enc_a                  := (others => 0);
    variable rise_v, fall_v             : std_logic_vector(0 to TDC_N-1) := (others => '0');
  begin
    if clk_sync'event and clk_sync = '1' then
      if rst_out = '1' then
        tdc_rout_enc := (others => 0);
        tdc_fout_enc := (others => 0);
        tdc_out_r    <= (others => '1');
        tdc_r_enc    <= (others => (others => '0'));
        tdc_f_enc    <= (others => (others => '0'));
        rise_v       := (others => '0');
        fall_v       := (others => '0');
        rise         <= (others => '0');
        fall         <= (others => '0');
      else
        for i in 0 to TDC_N-1 loop
          tdc_out_r(i)    <= tdc_out(i)(23);
          tdc_rout_enc(i) := 0;
          tdc_fout_enc(i) := 0;
          rise_v(i)       := '0';
          fall_v(i)       := '0';
          for j in 0 to 23 loop
            if tdc_redge(i)(23-j) = '1' then
              tdc_rout_enc(i) := 23-j;
              rise_v(i)       := '1';
            else
              tdc_rout_enc(i) := tdc_rout_enc(i);
              rise_v(i)       := rise_v(i);
            end if;
            if tdc_fedge(i)(23-j) = '1' then
              tdc_fout_enc(i) := 23-j;
              fall_v(i)       := '1';
            else
              tdc_fout_enc(i) := tdc_fout_enc(i);
              fall_v(i)       := fall_v(i);
            end if;
          end loop;
          if mask(i) = '0' then
            tdc_r_enc(i) <= std_logic_vector(to_unsigned(tdc_rout_enc(i), 6));
            tdc_f_enc(i) <= std_logic_vector(to_unsigned(tdc_fout_enc(i), 6));
            rise(i)      <= rise_v(i);
            fall(i)      <= fall_v(i);
          else
            tdc_r_enc(i) <= (others => '0');
            tdc_f_enc(i) <= (others => '0');
            rise(i)      <= '0';
            fall(i)      <= '0';
          end if;
        end loop;
      end if;
    end if;
  end process;

  rst_io <= rst_io_auto;

  -- OUTPUT

  data_out_gen : for i in 0 to TDC_N-1 generate
    data_out_r(6*(i+1)-1 downto 6*i) <= tdc_r_enc(i);
    data_out_f(6*(i+1)-1 downto 6*i) <= tdc_f_enc(i);
  end generate;

  polarity_out_gen : for i in 0 to TDC_N-1 generate
    falling_out(i) <= '1' when fall(i) = '1' else
                      '0';
    rising_out(i) <= '1' when rise(i) = '1' else
                     '0';
  end generate;

  -- Time Stamp

  tstamp_p : process(clk_sync, main_rst)
  begin
    if clk_sync'event and clk_sync = '1' then
      if main_rst = '1' or ovf = '1' or resetb = '0' then
        time_s <= (others => '0');
      else
        time_s <= time_s + 1;
      end if;
    end if;
  end process;

  tstamp <= std_logic_vector(time_s);

  desout <= des_out(0);

end Behavioral;
