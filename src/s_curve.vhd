-- * S-Curve

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;

-- * Entity

entity s_curve is
  generic (CounterLength : natural range 1 to 30 := 16;
           nb_channels   : natural range 1 to 16 := 1;
           asic_nb       : natural range 0 to 7  := 1);
  port (clk               : in  std_logic;  -- generic oscilator present on board
        global_reset      : in  std_logic;
        NewEventByChannel : in  std_logic_vector(nb_channels-1 downto 0);
        triggers          : in  std_logic_vector(nb_channels-1 downto 0);
        enable            : in  std_logic;
        params            : in  std_logic_vector(p6_params_width*8-1 downto 0);
        --
        fifo_clk          : in  std_logic;
        fifo_dataout      : out std_logic_vector(127 downto 0);
        fifo_rd           : in  std_logic;
        fifo_empty        : out std_logic);
end entity s_curve;

-- * Architecture

architecture Behavioral of s_curve is

  -- ** Signals

  -- *** Generic

  signal counter             : unsigned(CounterLength-1 downto 0)       := (others => '0');
  signal counter_integer     : integer                                  := 0;
  signal fifo_wr, fifo_clear : std_logic                                := '0';
  signal rst                 : std_logic                                := '1';
  signal asic_number         : std_logic_vector(2 downto 0)             := (others => '0');
-- signal triggers_enabled         : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_enabled_tmp     : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_enabled_tmp_del : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_latched         : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_latched_2       : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_top_rising      : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
  signal NewEventOrTrigger   : std_logic_vector(nb_channels-1 downto 0) := (others => '0');

  -- *** Array of event counts

  -- This array will store the number of counts in a period, by channel. It makes 28
  -- bits as adding four more bits for channel number is necessary.

  type fifo_data_array_type is array(0 to nb_channels-1) of unsigned(24 downto 0);
  signal fifo_data_array : fifo_data_array_type          := (others => '0' & X"000000");
  signal fifo_data       : std_logic_vector(31 downto 0) := (others => '0');

begin

  -- ** Generic

  -- Local reset signal.

  rst <= '1' when (global_reset = '1' or enable = '0') else '0';

  -- ** Mux

  --    Select one of two sources as the event to be count: whether the discriminator
  -- output, whether a new event as detected by the ASIC output capture mechanism.

  process (clk)
  begin
    if rising_edge(clk) then
      if params(16) = '0' then
        NewEventOrTrigger <= triggers;
      else
        NewEventOrTrigger <= NewEventByChannel(12) & NewEventByChannel(13) & NewEventByChannel(15) & NewEventByChannel(14) &
                             NewEventByChannel(10) & NewEventByChannel(11) & NewEventByChannel(9) & NewEventByChannel(8) &
                             NewEventByChannel(4) & NewEventByChannel(5) & NewEventByChannel(7) & NewEventByChannel(6) &
                             NewEventByChannel(2) & NewEventByChannel(3) & NewEventByChannel(1 downto 0);
      end if;
    end if;
  end process;

  -- Note how I need to swap individual bites. This is purely empiric, as I ignore
  --    why the channels are mixed. This is something to be fixed.

  -- ** Count trigger / data events

  -- Here I loop over a =counter=: during each of its periods, I +1 increase the
  --                =fifo_data_array= at each new event arrival. By the end of the =counter= period, I
  --                write the =fifo_data_array= value to a fifo, and I reset it.

  --                First, declare the counter period of =CounterLength= bits

  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        counter <= (others => '0');
      else
        counter <= counter + 1;
      end if;
    end if;
  end process;

  -- ** Count trigger / data events

-- Then, reset the array when counter i crosses its (i+1)th value; otherwise,
-- increase it upon reception of a new event.

-- Capture a falling edge async external event into a sync clock domain. The output
-- might have more than one clock cycle length.

  U_count : for i in 0 to nb_channels-1 generate
    process (clk)
    begin
      if rising_edge(clk) then
        if (rst = '1' or counter = (i+1)) then
          fifo_data_array(i) <= (others => '0');
        else
          if NewEventOrTrigger(i) = '1' then
            fifo_data_array(i) <= fifo_data_array(i) + 1;
          end if;
        end if;
      end if;
    end process;
  end generate U_count;

  -- ** Count trigger / data events

-- I count trigger edges, so I need to reduce a one or more clk cycles pulse, to
--    only a clock cycle.

-- Multiplex sending all 16 values in the array to a common fifo. Here, I have to
-- write events in the fifo sequentially. This is why I use the counter value. Note
-- how this value is in agreement with the previous reset value, by channel.

  asic_number <= std_logic_vector(to_unsigned(asic_nb, 3));

  counter_integer <= to_integer(counter);

  process (clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        fifo_data <= (others => '0');
        fifo_wr   <= '0';
      else
        case counter_integer is
          when 0 =>
            fifo_data <= asic_number & X"0" & std_logic_vector(fifo_data_array(0));
            fifo_wr   <= '1';
          when 1 =>
            fifo_data <= asic_number & X"1" & std_logic_vector(fifo_data_array(1));
            fifo_wr   <= '1';
          when 2 =>
            fifo_data <= asic_number & X"2" & std_logic_vector(fifo_data_array(2));
            fifo_wr   <= '1';
          when 3 =>
            fifo_data <= asic_number & X"3" & std_logic_vector(fifo_data_array(3));
            fifo_wr   <= '1';
          when 4 =>
            fifo_data <= asic_number & X"4" & std_logic_vector(fifo_data_array(4));
            fifo_wr   <= '1';
          when 5 =>
            fifo_data <= asic_number & X"5" & std_logic_vector(fifo_data_array(5));
            fifo_wr   <= '1';
          when 6 =>
            fifo_data <= asic_number & X"6" & std_logic_vector(fifo_data_array(6));
            fifo_wr   <= '1';
          when 7 =>
            fifo_data <= asic_number & X"7" & std_logic_vector(fifo_data_array(7));
            fifo_wr   <= '1';
          when 8 =>
            fifo_data <= asic_number & X"8" & std_logic_vector(fifo_data_array(8));
            fifo_wr   <= '1';
          when 9 =>
            fifo_data <= asic_number & X"9" & std_logic_vector(fifo_data_array(9));
            fifo_wr   <= '1';
          when 10 =>
            fifo_data <= asic_number & X"A" & std_logic_vector(fifo_data_array(10));
            fifo_wr   <= '1';
          when 11 =>
            fifo_data <= asic_number & X"B" & std_logic_vector(fifo_data_array(11));
            fifo_wr   <= '1';
          when 12 =>
            fifo_data <= asic_number & X"C" & std_logic_vector(fifo_data_array(12));
            fifo_wr   <= '1';
          when 13 =>
            fifo_data <= asic_number & X"D" & std_logic_vector(fifo_data_array(13));
            fifo_wr   <= '1';
          when 14 =>
            fifo_data <= asic_number & X"E" & std_logic_vector(fifo_data_array(14));
            fifo_wr   <= '1';
          when 15 =>
            fifo_data <= asic_number & X"F" & std_logic_vector(fifo_data_array(15));
            fifo_wr   <= '1';
          when others =>
            fifo_data <= (others => '0');
            fifo_wr   <= '0';
        end case;
      end if;
    end if;
  end process;

  -- ** Fifo

  Cati_output_fifo1_inst : entity work.Cati_output_fifo1
    port map (rst    => rst,
              --
              wr_clk => clk,
              din    => fifo_data,
              wr_en  => fifo_wr,
              --
              rd_clk => fifo_clk,
              rd_en  => fifo_rd,
              dout   => fifo_dataout,
              full   => open,
              empty  => fifo_empty);

end architecture Behavioral;
