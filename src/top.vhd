-- * Top

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- TODO: bring comments from vrog files to vhdl files, and handle them with doxygen

-- TODO: Migrate all doc from org here, under doxygen if possible

-- TODO: send disable_40MHz as parameters

-- * Libraries

library IEEE;
library UNISIM;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
use UNISIM.VComponents.all;

-- * entity

-- CatiROC slow control is made of 328 + 194 = 522 bit

entity top is
   -- ** Ports
   port (
      -- *** I/O If
      usb_if         : inout rc_usb_if;  -- Usb if
      catirocs       : inout rc_catiroc_array(0 to nb_asics-1);  -- all asics if
      -- *** Clocks I/O
      clk_oscillator : in    std_logic;  -- generic oscillator present on board 40MHz
      clk_gcu        : in    std_logic;  -- clock from gcu
      clk_select     : in    std_logic;  -- selects between clk_oscillator (1) and clk_gcu (0)
      -- *** Test points
      PX1            : out   std_logic;
      PX2            : out   std_logic;
      PX3            : in    std_logic;
      PX4            : out   std_logic);
end entity top;

-- * Architecture

architecture Behavioral of top is

   -- ** Signals

   -- Initial reset, global to all design, asserted high at power on during a
   -- time interval (thus its init high level), then switched off during normal
   -- operation.
   signal main_rst : std_logic := '1';

   -- Combines principal initial (main) reset along with the usb interrupt
   signal global_reset : std_logic := '1';

   -- *** Control delay signals

   type control_delay_type is (cd_1, cd_2, cd_3, cd_end);
   signal cd_state             : control_delay_type;
   signal phasedone, phasestep : std_logic             := '0';
   signal cd_counter           : unsigned(15 downto 0) := (others => '0');
   signal cd_counter_ref       : unsigned(15 downto 0) := (others => '0');
   signal cd_busy              : std_logic             := '1';

   -- *** Peripherals

   -- Note that in vhdl, signals must be fully constrained

   signal p1_if : rc_peripheral_if_p1(params(0 to 0));
   signal p2_if : rc_peripheral_if_p2(params(0 to nb_asics-1));
   signal p3_if : rc_peripheral_if_p3(params(0 to nb_asics-1));
   signal p4_if : rc_peripheral_if_p4(params(0 to 0));
   signal p5_if : rc_peripheral_if_p5(params(0 to 0));
   signal p6_if : rc_peripheral_if_p6(params(0 to 0));

   -- *** Signals in between s-curve manager and usb interface

   signal p6_fifo_datout : t_data(0 to nb_asics-1)(p6_data_width*8-1 downto 0) := (others => (others => '0'));
   signal p6_fifo_empty  : t_std_logic_vector_nb_asics                         := (others => '1');
   signal p6_fifo_read   : t_std_logic_vector_nb_asics                         := (others => '0');
   signal p6_fifo_clk    : std_logic                                           := '0';

   -- *** Remaining signals

   signal init_standby_period                       : unsigned(19 downto 0)        := (others => '0');
   signal fifos_empty                               : std_logic_vector(2 downto 0) := (others => '1');
   signal usb_SlowControlReady, clk_adc, pll_locked : std_logic                    := '0';
   signal disable_40MHz, gnd                        : std_logic                    := '0';
   signal p2_empty_tmp                              : std_logic                    := '0';
   signal discri_falling                            : t_nbasics_2bytes             := (others => (others => '0'));
   signal NewEventByChannel                         : t_nbasics_2bytes             := (others => (others => '0'));

   -- *** Clocks

   signal clk_40Mhz_pll_tmp, clk_10MHz_nonshifted_tmp : std_logic_vector(0 downto 0) := (others => '0');
   signal clk_80MHz_delayed                           : std_logic                    := '0';
   signal clk_40MHz, clk_80MHz, clk_10MHz, clk_160MHz : std_logic                    := '0';
   signal clk_480MHz, clk_120MHz                      : std_logic                    := '0';
   signal clk_main_i, clk_main                        : std_logic                    := '0';

   -- ** Alias

   alias StartSys        : std_logic is catirocs(0).StartSys;
   alias ExternalTrigger : std_logic is catirocs(0).trig_ext;

begin

   -- ** Inspections

   px1 <= '0';
   px2 <= '0';
   px4 <= '0';

   -- ** Main reset logic

   -- Uses =clk_fpga=, an osc on board, always present at startup, as long as
   -- =usb_clkout= gets activated only when the software gets connected to the board.

   -- This allows using =main_rst= as a synchronous, global reset signal at startup.

   U_main_rst : process (usb_if.clk_2232)
   begin
      if rising_edge(usb_if.clk_2232) then
         if (pll_locked = '0') then
            init_standby_period <= (others => '0');
            main_rst            <= '1';
         elsif (init_standby_period /= X"FFFFF") then
            init_standby_period <= init_standby_period + 1;
            main_rst            <= '1';
         else
            main_rst <= '0';
         end if;
      end if;
   end process U_main_rst;

   -- ** External trigger
   --
   -- Generates a periodic signal to be fed into the =trig_ext= input of CatiROC.
   -- Its frequency may be modified, and a bit selects its state (enabled or
   -- not), following the status of =p2_params(3)= bit.
   --
   -- I generate a single bit pulse with a period. I may increase this period
   -- using four bits, =p2_params(7 downto 5)=, lowering the frequency of this
   -- pulse.
   --
   -- This pulse is to be used as external trigger.
   --
   -- In order to take data synchronously with an external device (pulse
   -- generator, led, etc.), the input signal =io_fpga1= may be used too as an
   -- input to =trig_ext=.

   U_external_trigger : entity work.external_trigger
      port map (global_reset    => global_reset,
                p_if            => p2_if,
                StartSys        => StartSys,
                ExternalTrigger => ExternalTrigger,
                io_fpga1        => px3);

   -- ** USB Interface

   -- Input/Output USB to the PC.

   -- Implements two FSM

   --  - USB In  :: reacts upon an interrupt and updates the register bank
   --    - USB Out :: sends data back from peripherals to the pc

   -- Realize how in the usb interface block the command “09” will activate =p6_enable=
   -- but also =p2_enable=. This is necessary in order to activate the signal
   -- =NewEventByChannel=, used within the [[*P6: S Curve][P6: S Curve]] peripheral. This means that
   -- activating this peripheral with =p6_enable=, also requires activating
   -- =p2_enable=. However, this means data is produced too. To avoid this, we ’fake’
   -- set the =p2_empty= flag so that the [[*USB Interface][USB Interface]] module ignores data coming from
   -- peripheral 2 when =p6_enable= is activated.

   -- TODO: fix that
   p2_empty_tmp <= p2_if.empty or p6_if.enable;

   U_usb_if : entity work.usb_if
      generic map (RegBankLength => 70)
      port map (main_rst      => main_rst,
                usb_if        => usb_if,  -- usb ftdi interface
                -- Peripherals
                p1_if         => p1_if,   -- P1: simple counter
                p2_if         => p2_if,   -- P2: asic data
                p3_if         => p3_if,   -- P3: asic slow control
                p4_if         => p4_if,   -- P4: adc data
                p5_if         => p5_if,   -- P5: adc slow control
                p6_if         => p6_if,   -- P6: s-curve computing
                --outs
                global_reset  => global_reset,
                disable_40MHz => disable_40MHz);

   -- ** P1 - Counter generator

   -- Implements a peripheral to [[*USB Interface][USB Interface]] controller.

   -- ----------------------------------------------------------------

   -- Peripheral 1 implements a generic peripheral to "usb_if" controller.
   -- It corresponds to “01” command.

   -- It implements a set of four 8-bits up counters in parallel, forming a
   -- 32-bits word. Each 8-bits initializes to an increasing value: 0,1,2
   --    and 3. This forms an output increasing sequence with +1 values.

   U_P1 : entity work.p1_counter
    port map (clk          => clk_oscillator,
              global_reset => global_reset,
              p_if         => p1_if);

   -- ** P2 & P3 - CatiROC manager

   -- Here comes all what is related to ASIC management and data capture.

   -- Each catiroc manager implements commands P2 and P3 to capture data and send slow
   -- control to the ASIC. It stores data (slow control and events) back into two
   -- fifos.

   -- Then, two modules act as interface to the usb manager. Each of these implements
   -- a 8-to-1 multiplexer to concentrate data from previous 8 fifos to next one. Each
   --        of these modules will deal with slow control and event data.

   --    P2, P3: catiroc interface manager
   --       Implements a peripheral to "usb_if"

   --       - P2: Data Capture, 06 Command
   --       - P3: Slow Control, CC Command

   --       Note that =p2_params= feeds here the lowest weight (rightmost) byte in the group of
   --       =p2_params_width= bytes (for each asic). The two leftmost bytes (assuming
   --                                                                        =p2_params_width= = 3) are used away.

   -- - we use here =p2_params(7 downto 0)=, as =p2_params(23 downto 8)= are
   -- used for the configurable delay [[*Control delay][feature]]

   --  - <<cd_busy>> we use here =cd_busy= instead of =p2_enable= due to the
   --    configurable delay [[*Control delay][feature]]; during the time the clock is delayed,
   --    we need to avoid taking data, so we need to set the reset high
   --    during a longer time

   -- #+begin_src vhdl-tools
   --    not_cd_busy <= not(cd_busy);
   -- #+end_src

   --    Loop over the number of asics, instantiating one =catiroc= module each time, and
   --            feeding the right signals. Note the the parameter =num_of_discri_channels= is
   --            necessary to limit the fpga resources.

   u_catiroc_controller : entity work.catiroc
     port map (global_reset      => global_reset,
               catirocs          => catirocs,   -- all asics
               p2_if             => p2_if,      -- P2: asic data
               p3_if             => p3_if,      -- P3: asic slow control
               pll_locked        => pll_locked,
               -- Clocks
               clk_sc            => clk_10MHz,
               clk_2232          => clk_80MHz,  -- Read clock
               clk_80MHz         => clk_80MHz,
               clk_80MHz_delayed => clk_80MHz_delayed,
               clk_40MHz         => clk_40MHz,
               clk_480MHz        => clk_480MHz,
               clk_120MHz        => clk_120MHz,
               clk_160MHz        => clk_160MHz,
              -- outs
              discri_falling    => discri_falling,
              NewEventByChannel => NewEventByChannel);

   -- ** P6 - S Curve

   -- TODO: Integrate in one single module, same as asic sc and data capture

   --    S-Curve Measurements

   --       Implements a peripheral to [[*USB Interface][USB Interface]] controller.

   --  - P6: ...

   -- ----------------------------------------------------------------

   -- =triggers= is the output of the asic discriminator, one bit by channel, set to one
   -- when an event is detected. The =NewEventByChannel= contains the equivalent of
   -- =triggers=, but it considers events (by channel), as seen by the asic. It is
   -- generated within the data capture mechanism, in parallel to the event counter,
   -- also by channel.

   -- Here the goal is to declare a =CounterLength= bits time window, and to sum up the
   -- number of clock periods where a signal is high. This value is readout, to
   -- compute a pourcentage. A number of these values is averaged by the software.

   -- A set of these measurements is run by modifying one parameter (the threshold,
   -- common to all channels), tracing a curve: the s-curve.

   -- The procedure may be run in parallel for all 16 channels in all of the
   -- =nb_asics=. One asic’s channels are labelled using 4 bits, whereas the asic number
   -- uses 3 bits (0 to 7), which makes 7 bits. Each computed value is composed of
   -- these 7 bits, to identify the channel/asic, followed by 25 bits with the
   --                                                                     measurement. This means that =CounterLength= is limited to a maximum of 25.

   U_s_curve : for i in 0 to nb_asics-1 generate
      s_curve_1 : entity work.s_curve
         generic map (CounterLength => 20,
                      nb_channels   => 16,
                      asic_nb       => i)
         port map (clk               => clk_80Mhz,
                   global_reset      => global_reset,
                   triggers          => discri_falling(i),
                   NewEventByChannel => NewEventByChannel(i),
                   params            => p6_if.params(0),
                   enable            => p6_if.enable,
                   fifo_clk          => p6_fifo_clk,
                   fifo_dataout      => p6_fifo_datout(i),
                   fifo_rd           => p6_fifo_read(i),
                   fifo_empty        => p6_fifo_empty(i));
   end generate U_s_curve;

   -- *** Catiroc-usb If : s curve

   -- The multiplexor, to send =nb_asics= modules data to the usb controller.

   p6_fifo_clk <= clk_120MHz;

   U_scurve_usb_if : entity work.catiroc_usb_if_scurve
      port map (global_reset => global_reset,
                -- incoming data
                asic_clk     => p6_fifo_clk,
                asic_empty   => p6_fifo_empty,
                asic_rd      => p6_fifo_read,
                asic_data    => p6_fifo_datout,
                -- if to usb managager
                fifo_clk     => p6_if.clk,
                fifo_dataout => p6_if.data,
                fifo_rd      => p6_if.rd,
                fifo_empty   => p6_if.empty);

   -- ** Control delay

   -- This state machine controls the behaviour of the =c0= delayed clock in
   -- the [[*Control delay pll][pll]]. The goal here is to programmatically delay a clock
   -- by small steps (~ one hundred ps.). Then, this clock is to be used as
   -- the trigger input of a pulse generator. This makes that the generated
   -- pulses in the pulse generator are in sync with fpga / asic.

   -- To delay the clock, we are based on the P2 command (data taking). When
   -- we enable it, we start increasing the delay steps until we reach the
   -- maximum, given by the parameter =p2_params(23 downto 8)=. This makes we
   -- need to reset the pll based on =p2_enable= to remove any delay, and then
   -- we add a new one.

   -- As a consequence, during the time we set the correct delay (a few
   -- microseconds), we need to avoid start taking data. For that, we are
   -- using the [[cd_busy]] signal: as this is set to one, the [[*P2 & P3 - CatiROC manager][CatiROC manager]]
   -- block will be reset, setting the ASIC reset signal high.

   -- See application note “an507” from altera as a reference for details on
   -- next block, as well as for an overview of the resulting chronogram.

   process (clk_40MHz)
   begin
      if rising_edge(clk_40MHz) then
         cd_counter_ref <= unsigned(p2_if.params(0)(23 downto 8));
         if (global_reset = '1' or p2_if.enable = '0') then
            phasestep  <= '0';
            cd_counter <= (others => '0');
            cd_busy    <= '1';
            cd_state   <= cd_1;
         else
            case cd_state is
               when cd_1 =>
                  cd_busy <= '1';
                  if p2_if.enable = '1' then
                     phasestep <= '1';
                     cd_state  <= cd_2;
                  else
                     phasestep <= '0';
                     cd_state  <= cd_1;
                  end if;
               when cd_2 =>
                  cd_busy   <= '1';
                  phasestep <= '1';
                  if phasedone = '0' then
                     cd_state <= cd_3;
                  else
                     cd_state <= cd_2;
                  end if;
               when cd_3 =>
                  cd_busy <= '1';
                  if phasedone = '1' then
                     phasestep <= '0';
                     if cd_counter >= cd_counter_ref then
                        cd_state <= cd_end;
                     else
                        cd_state <= cd_1;
                     end if;
                     cd_counter <= cd_counter + 1;
                  else
                     phasestep <= '1';
                     cd_state  <= cd_3;
                  end if;
               when cd_end =>
                  cd_busy  <= '0';
                  cd_state <= cd_end;
            end case;
         end if;
      end if;
   end process;

   -- This block implements a pll. This is not currently used to drive the
   -- adc, but to provide two 10 MHz clocks. The first one, =c0= is to be
   -- delayed programmatically (see [[*Control delay][control delay]] for more on this). The second
   -- one, =c1=, is used as a reference to see how the first one is
   -- shifted. This is useful too to monitor an external pulse generator and
   -- see if it is in sync with the 40 MHz clock.

   -- To note that here we are resetting the pll with the P2 command
   -- (“06”). This is because we want to reset the delay in =c0=, and then
   -- set a new delay (see [[*Control delay][control delay]] for more on this).

   -- ** Clock management

   -- From =clk_fpga=, generate 40, 160, 10 and 40 MHz clocks. Additionally, produce
   -- 480 and 120 MHz clocks for dds use, and a 180_delayed clock for ASIC data
   -- capture.

   U_pll : if freq_source = 40 generate

    clk_main_i <= clk_oscillator;

   elsif freq_source = 42 generate

      U_pll_gcu : entity work.pll_gcu
        port map (clk_in1  => clk_oscillator,
                  clk_out1 => clk_main_i,
                  reset    => gnd,
                  locked   => open);

   end generate;

   U_bufgmux_main : BUFGMUX
      port map (I1 => clk_main_i,
                I0 => clk_gcu,
                S  => clk_select,
                O  => clk_main);

   U_main_pll : entity work.pll
      port map (CLK_IN1  => clk_main,
                -- Clock out ports
                CLK_OUT1 => clk_40MHz,
                CLK_OUT2 => clk_160MHz,
                CLK_OUT3 => clk_10MHz,
                CLK_OUT4 => clk_80MHz,
                CLK_OUT5 => clk_480MHz,
                CLK_OUT6 => clk_120MHz,
                CLK_OUT7 => clk_80MHz_delayed,
                -- Status and control signals
                RESET    => gnd,        -- async reset
                LOCKED   => pll_locked);

   -- *** Replicate 10 MHz out of the fpga

   -- ** FMC

   -- IBUFDS_loop_2 :
   --    for i in 0 to 9 generate
  --       U_IBUFDS_2 : IBUFDS
  --          port map (O  => gcu_fmc_in(i),
  --                    I  => gcu_fmc_in_p(i),
  --                    IB => gcu_fmc_in_n(i));
  --    end generate;

  --    OBUFDS_loop_1 :
  --       for i in 0 to 7 generate
  --          gcu_fmc_out(i) <= gcu_fmc_in(i);
  --          U_OBUFDS_1 : OBUFDS
  --             port map (I  => gcu_fmc_out(i),
  --                       O  => gcu_fmc_out_p(i),
  --                       OB => gcu_fmc_out_n(i));
  --       end generate;

end architecture Behavioral;
