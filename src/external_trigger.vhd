-- * External trigger

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std_unsigned.all;
use work.common_package.all;

-- * Entity

entity external_trigger is
  port (global_reset    : in    std_logic;
        p_if            : inout rc_peripheral_if_p2;
        StartSys        : in    std_logic;
        ExternalTrigger : out   std_logic;
        io_fpga1        : in    std_logic);
end entity external_trigger;

-- * Architecture

architecture Behavioral of external_trigger is

  -- ** Signals

  signal counter_trig_ext    : std_logic_vector(15 downto 0) := (others => '0');
  -- Temp trig ext to be oored with external signal
  signal trig_ext_inner, rst : std_logic                     := '0';

  -- ** Alias

  alias EnableExternalTrigger : std_logic is p_if.params(0)(9);
  alias enable                : std_logic is p_if.params(0)(8);

begin

  -- ** process

  rst <= global_reset or not(p_if.enable) or not(StartSys);

  U_trig_ext : process (p_if.clk) is
  begin
    if rising_edge(p_if.clk) then
      if rst = '1' then
        counter_trig_ext <= (others => '0');
        trig_ext_inner   <= '0';
      else
        if counter_trig_ext >= (p_if.params(0)(13 downto 10) & X"800") then
          trig_ext_inner   <= enable;
          counter_trig_ext <= (others => '0');
        else
          trig_ext_inner   <= '0';
          counter_trig_ext <= counter_trig_ext + 1;
        end if;
      end if;
    end if;
  end process U_trig_ext;

  ExternalTrigger <= trig_ext_inner or (io_fpga1 and EnableExternalTrigger);

end architecture Behavioral;
