-- * CatiROC usb interface
--
--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...
--
-- The goal of this module is to act as a 8 to 1 multiplexor. It takes data from
-- “nb_asics” fifos, each of “input_bus_width_bits” width data buses, and
-- stores it into a local fifo. The later is supposed to be read by the usb manager
-- interface.
--
-- The generics provide some flexibility here, so that this module is assumed to
-- perform regardless of the previous stage. For example, it may be used to move
-- event (128 bits) and slow control (8 bits) data from a previous state of catiroc
-- managers.
--
-- * Header
--
-- * Libraries

library IEEE;

-- * Packages

use work.common_package.all;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

use work.mux_fifo_pkg.all;

-- * Entity

-- We take “input_bus_width_bits” bits incoming data from “nb_asics” asic managers,
-- and we store all of this in a “input_bus_width_bits” bits data fifo. The fifo is
-- to be readout to the usb.

entity catiroc_usb_if_capture is
   port (global_reset    : in  std_logic;
         -- input interface
         asic_clk        : in  std_logic;
         asic_empty      : in  std_logic_vector;
         asic_rd         : out std_logic_vector;
         asic_data       : t_data;
         -- if to usb manager
         fifo_clk        : in  std_logic;
         fifo_dataout    : out std_logic_vector;
         fifo_rd         : in  std_logic;
         fifo_empty      : out std_logic);
end entity catiroc_usb_if_capture;

-- * Architecture

architecture Behavioral of catiroc_usb_if_capture is

  -- ** Signals

   signal datain      : t_mux_fifo_datain(asic_data'range)(asic_data(0)'range);
   signal dataout     : std_logic_vector(asic_data(0)'range) := (others => '0');
   signal wr_en, full : std_logic                                 := '0';

begin

  -- ** Multiplexor

   u_datain : for i in asic_data'range generate
      datain(i) <= asic_data(i);
   end generate u_datain;

   u_mux_fifo : entity work.mux_fifo
      generic map (enabled_channels   => p_if.params(0)(31 downto 24),
                   -- necessary because of ghdl, otherwise just remove
                   mux_fifo_bus_width => asic_data(0)'length,
                   extend             => 1)
      port map (rst     => global_reset,
                clk     => asic_clk,
                -- in if
                datain  => datain,
                empty   => asic_empty,
                rd_en   => asic_rd,
                -- out if
                dataout => dataout,
                wr_en   => wr_en,
                full    => full);

  -- ** Fifo

   U_fifo : entity work.fifo_64
     port map (rst    => global_reset,
               -- incoming data from nb_asics ASICS
               wr_clk => asic_clk,
               din    => fifo_datain,
               wr_en  => fifo_wr_en,
               full   => fifo_full,
               -- if to usb managager
               rd_clk => fifo_clk,
               dout   => fifo_dataout,
               rd_en  => fifo_rd,
               empty  => fifo_empty);

end architecture Behavioral;
