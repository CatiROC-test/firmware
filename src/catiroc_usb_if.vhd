-- * CatiROC usb interface
--
--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...
--
-- The goal of this module is to act as a 8 to 1 multiplexor. It takes data from
-- “nb_asics” fifos, each of “input_bus_width_bits” width data buses, and
-- stores it into a local fifo. The later is supposed to be read by the usb manager
-- interface.
--
-- The generics provide some flexibility here, so that this module is assumed to
-- perform regardless of the previous stage. For example, it may be used to move
-- event (128 bits) and slow control (8 bits) data from a previous state of catiroc
-- managers.
--
-- * Header
--
-- * Libraries

library IEEE;

-- * Packages

use work.common_package.all;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;

-- * Entity

-- We take “input_bus_width_bits” bits incoming data from “nb_asics” asic managers,
-- and we store all of this in a “input_bus_width_bits” bits data fifo. The fifo is
-- to be readout to the usb.

entity catiroc_usb_if is
  generic (nb_channels          : in natural                := 8;
           input_bus_width_bits :    natural range 1 to 128 := 128;
           extend               :    natural range 0 to 1   := 0);
  port (global_reset    : in  std_logic;
        EnabledChannels : in  t_std_logic_vector_nb_asics := (others => '1');
        -- input interface
        asic_clk        : in  std_logic;
        asic_empty      : in  std_logic_vector(0 to nb_channels-1);
        asic_rd         : out std_logic_vector(0 to nb_channels-1);
        asic_data       :     t_data;
        -- if to usb manager
        fifo_clk        : in  std_logic;
        fifo_dataout    : out std_logic_vector((8*extend+input_bus_width_bits)-1 downto 0);
        fifo_rd         : in  std_logic;
        fifo_empty      : out std_logic);
end entity catiroc_usb_if;

-- * Architecture

architecture Behavioral of catiroc_usb_if is

  -- ** Signals

  signal priority_flag, nb_asics_unsigned        : unsigned(3 downto 0)                                         := X"0";
  signal fifo_datain                             : std_logic_vector((8*extend+input_bus_width_bits)-1 downto 0) := (others => '0');
  signal fifo_wr_en, fifo_full                   : std_logic                                                    := '0';
  signal mystate                                 : mystate_type;
  signal asic_rd_inner                           : std_logic_vector(7 downto 0)                                 := (others => '0');
  signal enabledchannels_inner, asic_empty_inner : unsigned(7 downto 0)                                         := (others => '0');

  -- type t_data_usb is array (0 to 7) of std_logic_vector;
  signal asic_data_inner : t_data(0 to 7)(input_bus_width_bits-1 downto 0) := (others => (others => '0'));

begin

  -- ** Randomize

-- This signal increases and is decorrelated of the merge [[*Write fifo][process]]. As a
-- result, it will randomly give a chance to all channels to be readout.

-- First, I convert natural (1 to 8) generic to a 4 bits signal.

  nb_asics_unsigned <= to_unsigned(nb_asics-1, 4);

  -- Then, I increase the value of =priotiry_flag= up to the number of asics, as going
  --        beyond is useless.

  process(asic_clk)
  begin
    if (asic_clk'event and asic_clk = '1') then
      if priority_flag = nb_asics_unsigned then
        priority_flag <= (others => '0');
      else
        priority_flag <= priority_flag + 1;
      end if;
    end if;
  end process;

  -- ** Convert to 8 bits

-- Input =asic_empty= signal is =nb_asics= bits length: I convert it to 8 bits to use
-- an ’if’ construct.

--        Output =asic_rd_inner= is 8 bits: I resize it to =nb_asics= length, the size of =asic_rd=.

  asic_empty_inner      <= resize(unsigned(asic_empty), nb_asics);
  enabledchannels_inner <= resize(unsigned(EnabledChannels), 8);
  asic_rd               <= asic_rd_inner(nb_asics-1 downto 0);

  u_asic_data : for i in asic_data'range generate
    asic_data_inner(i) <= asic_data(i);
  end generate u_asic_data;

  -- ** Write fifo

-- TODO: fix this phrase
-- Fifo in asic manager must be in half trhougt mode.

-- Process implementing a state machine. Two state are sufficient to read from one
--            of the “nb_asics” previous fifos and write to next.

  --extend_bit_vector <= std_logic_vector(to_unsigned(extend, 1));
  --extend_bit <= extend_bit_vector(0);

  process (asic_clk)
  begin

    if rising_edge(asic_clk) then

      if (global_reset = '1') then

        fifo_wr_en    <= '0';
        asic_rd_inner <= (others => '0');
        mystate       <= mystate_state_1;

      else

        case mystate is

          when mystate_state_1 =>

            asic_rd_inner <= (others => '0');
            fifo_wr_en    <= '0';

            if (priority_flag = X"0" and asic_empty_inner(0) = '0' and fifo_full = '0' and EnabledChannels(0) = '1') then

              asic_rd_inner(0) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(0) & '0' & X"0" & "000";
              else
                fifo_datain <= asic_data_inner(0);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"1" and asic_empty_inner(1) = '0' and fifo_full = '0' and EnabledChannels(1) = '1') then

              asic_rd_inner(1) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(1) & '0' & X"0" & "001";
              else
                fifo_datain <= asic_data_inner(1);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"2" and asic_empty_inner(2) = '0' and fifo_full = '0' and EnabledChannels(2) = '1') then

              asic_rd_inner(2) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(2) & '0' & X"0" & "010";
              else
                fifo_datain <= asic_data_inner(2);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"3" and asic_empty_inner(3) = '0' and fifo_full = '0' and EnabledChannels(3) = '1') then

              asic_rd_inner(3) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(3) & '0' & X"0" & "011";
              else
                fifo_datain <= asic_data_inner(3);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"4" and asic_empty_inner(4) = '0' and fifo_full = '0' and EnabledChannels(4) = '1') then

              asic_rd_inner(4) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(4) & '0' & X"0" & "100";
              else
                fifo_datain <= asic_data_inner(4);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"5" and asic_empty_inner(5) = '0' and fifo_full = '0' and EnabledChannels(5) = '1') then

              asic_rd_inner(5) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(5) & '0' & X"0" & "101";
              else
                fifo_datain <= asic_data_inner(5);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"6" and asic_empty_inner(6) = '0' and fifo_full = '0' and EnabledChannels(6) = '1') then

              asic_rd_inner(6) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(6) & '0' & X"0" & "110";
              else
                fifo_datain <= asic_data_inner(6);
              end if;
              mystate <= mystate_state_2;

            elsif (priority_flag = X"7" and asic_empty_inner(7) = '0' and fifo_full = '0' and EnabledChannels(7) = '1') then

              asic_rd_inner(7) <= '1';
              if extend = 1 then
                fifo_datain <= asic_data_inner(7) & '0' & X"0" & "111";
              else
                fifo_datain <= asic_data_inner(7);
              end if;
              mystate <= mystate_state_2;

            else

              asic_rd_inner <= (others => '0');
              -- asic_to_read <= X"00";
              fifo_datain   <= (others => '1');
              mystate       <= mystate_state_1;

            end if;

          when mystate_state_2 =>

            fifo_wr_en    <= '1';
            asic_rd_inner <= (others => '0');
            mystate       <= mystate_state_1;

        end case;

      end if;

    end if;

  end process;

  -- ** Fifo

  -- Here, I select between one of these fifos following the value on the generic
  -- “input_bus_width_bits”, by using conditional code generation. This is necessary
  -- to make this module general, and useful for different configurations regarding
  -- data bus widths.

  -- *** Data readout

  U_fifo_sc : if input_bus_width_bits = 64 generate
    U_fifo : entity work.fifo_64
      port map (rst    => global_reset,
                -- incoming data from nb_asics ASICS
                wr_clk => asic_clk,
                din    => fifo_datain,
                wr_en  => fifo_wr_en,
                full   => fifo_full,
                -- if to usb managager
                rd_clk => fifo_clk,
                dout   => fifo_dataout,
                rd_en  => fifo_rd,
                empty  => fifo_empty);
  end generate;

  -- *** S Curve

  U_fifo_events : if input_bus_width_bits = 128 generate
    U_fifo : entity work.fifo_128
      port map (rst    => global_reset,
                -- incoming data from nb_asics ASICS
                wr_clk => asic_clk,
                din    => fifo_datain,
                wr_en  => fifo_wr_en,
                full   => fifo_full,
                -- if to usb managager
                rd_clk => fifo_clk,
                dout   => fifo_dataout,
                rd_en  => fifo_rd,
                empty  => fifo_empty);
  end generate;

  -- *** Slow control

  U_fifo_sc2 : if input_bus_width_bits = 8 generate
    U_fifo : entity work.fifo_8
      port map (rst    => global_reset,
                -- incoming data from nb_asics ASICS
                wr_clk => asic_clk,
                din    => fifo_datain,
                wr_en  => fifo_wr_en,
                full   => fifo_full,
                -- if to usb managager
                rd_clk => fifo_clk,
                dout   => fifo_dataout,
                rd_en  => fifo_rd,
                empty  => fifo_empty);
  end generate;

end architecture Behavioral;
