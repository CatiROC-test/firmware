-- * Usb Interface
--
--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- =Commands= are the full =RegBank= array. They are composed of the =order= (first byte)
-- followed by a variable length of =parameters=.
--
-- ** Intro
--
-- Implements comunication between generic FPGA logic fabric and a PC
-- through a [[*FT2232H][FT2232H]] controller from FTDI
--
-- The logic waits for an interrupt signal in the [[*USB in][USB in]] FSM, updating a
-- =RegBankLength= x 8 bits register bank (the =command=); then, acts accordingly,
-- following the contents of the first word in this bank (the =order=,
-- see [[*Orders][Orders]])
--
-- The [[*USB out][USB out]] FSM implements sending several sources of data back to the PC
--
-- *** Peripherals
--
-- It is possible to connect peripherals to this module. Each
-- peripheral shares a common interface:
--
--  - p1_datain :: in,  output from fifo
--  - p1_empty  :: in,  empty from fifo
--  - p1_rd     :: out, read to fifo
--  - p1_params :: out, parameters
--  - p1_enable :: out, enable signal
--
-- along with the generics
--
--  - p1_data_width   :: datin width, in bytes
--  - p1_params_width :: params register length, in bytes
--
-- It is assumed that each peripheral corresponds to a =command=, made up of an [[*Orders][order]]
-- followed by its =parameters=. The =p1_enable= signal enables the logic to fill-up
-- the peripheral fifo; the =p1_params= register provides a means to configure the
-- order.
--
-- *** Orders
--
-- =Orders= consist of the first byte in the full =RegBank= array, the =command=, which
-- is composed of the =order= (first byte), the command =parameters= and end by a =FF=
-- byte.
--
-- Currently, the following =orders= are implemented
--
--  - X"01" - peripheral 1 :: simple counter
--  - X"06" - peripheral 2 :: asic data
--  - X"CC" - peripheral 3 :: asic slow control
--  - X"07" - peripheral 4 :: ADC data
--  - X"08" - peripheral 5 :: ADC slow control
--  - X"09" - peripheral 6 :: asic S-Curve
--
--  - X"0A" - disable 40MHz clock
--  - X"05" - returns register bank
--  - X"04" - returns up counter
--  - X"0B" - readout control register
--
-- *** Peripheral parameters
--
--  - P2
--
--    + p2_params(23 downto 8) ::
--    + p2_params(7 downto 0) ::
--

-- TODO: Document with doxygen

-- * Libraries

library IEEE;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;

-- * Entity

-- USB IF Interface
-- Signals are described in table 3.5 of ref. FT2232H

entity usb_if is
  generic (RegBankLength : natural := 70);  -- bytes
  port (main_rst      : in    std_logic;
        usb_if        : inout rc_usb_if;    -- ftdi usb if
        -- Peripherals
        p1_if         : inout rc_peripheral_if_p1;
        p2_if         : inout rc_peripheral_if_p2;
        p3_if         : inout rc_peripheral_if_p3;
        p4_if         : inout rc_peripheral_if_p4;
        p5_if         : inout rc_peripheral_if_p5;
        p6_if         : inout rc_peripheral_if_p6;
        --outs
        disable_40MHz : out   std_logic;
        global_reset  : out   std_logic);
end usb_if;

-- * Architecture

architecture Behavioral of usb_if is

  -- ** Types

  -- State signals in the [[*USB in][USB in]] and [[*USB out][USB out]] FSMs.

  type read_state_type is (read_stand_by, read_state_1, read_state_2, read_wait_state);

  type write_state_type is (write_stand_by, write_readout, write_end,
                            write_wait_data_pulse, write_send_data_pulse,
                            write_take_data_pulse_2, write_control_register);

  -- Register bank to store slow control orders from usb.
  type RegBank is array(0 to RegBankLength*nb_asics) of t_Byte;

  -- ** Signals

  signal saved_word, data_taken     : std_logic_vector(127 downto 0)              := (others => '0');
  -- signal memo_fifo_dout             : std_logic_vector(143 downto 0)              := (others => '0');
  signal rst_write                  : std_logic                                   := '0';
  signal dina                       : t_Byte                                      := (others => '0');
  signal my_RegBank                 : RegBank                                     := (others => X"00");
  signal usb_rd_inner, interrupt    : std_logic                                   := '0';
  signal txe_one                    : std_logic                                   := '0';
  signal control_register           : std_logic_vector(7 downto 0)                := X"A5";
  signal pX_enable                  : std_logic_vector(nb_peripherals-1 downto 0) := (others => '0');
  signal addrw                      : natural range 0 to RegBankLength*nb_asics   := 0;
  signal addrrd, old_addrrb, MaxAdd : natural range 0 to RegBankLength*nb_asics   := 0;
  signal max_loop                   : natural range 0 to RegBankLength*nb_asics   := 0;
  signal pX_read                    : std_logic                                   := '0';

  -- p_if params

  signal p1_params : std_logic_vector(p1_params_width*8-1 downto 0) := (others => '0');
  signal p2_params : rc_params_p2(0 to nb_asics-1)                  := (others => (others => '0'));
  signal p3_params : rc_params_p3(0 to nb_asics-1)                  := (others => (others => '0'));
  signal p4_params : std_logic_vector(p4_params_width*8-1 downto 0) := (others => '0');
  signal p5_params : std_logic_vector(p5_params_width*8-1 downto 0) := (others => '0');
  signal p6_params : std_logic_vector(p6_params_width*8-1 downto 0) := (others => '0');

  -- State signals in the [[*USB in][USB in]] and [[*USB out][USB out]] FSMs.

  signal read_state  : read_state_type;
  signal write_state : write_state_type;

  -- ** Alias

  -- Identifies the board.
  alias BoardID : std_logic_vector(7 downto 0) is p2_if.params(0)(23 downto 16);

  -- Identifies events in a channel, as readout by the ASIC, in contrast to the
  --    triggers, which are seen by the ASIC discriminator.
  -- neweventsbychannel

begin

  -- ** Generic

  -- Static signal assignments

  usb_if.usb_siwua <= '1';
  -- usb_reset_fpga <= main_rst;          --'0'
  -- button         <= main_rst;          --'0'
  usb_if.usb_rd    <= usb_rd_inner;
  control_register <= X"04";

  -- Send the interrupt out of the controller. This is useful in non-peripheral
  -- blocks which are not driven by a =pX_enable= signal in order to reset them,
  -- erasing fifos, bringing state machines to known states, etc.

  -- Goes high at startup and when an usb interrupt happens.
  global_reset <= interrupt or main_rst;

  -- ** Usb in fsm
  --
  -- Receives data from PC.
  --
  -- This process waits for an interrupt (usb_rxf=0) and refreshes the
  -- register bank. Implements section 4, figure 4.4, "Read Timing" (upper
  -- half) of reference [[*FT2232H][FT2232H]]; details are given in section 4.4.1.

  U_USBIn : process (usb_if.clk_2232)
  begin
    if rising_edge(usb_if.clk_2232) then
      if (main_rst = '1') then
        usb_rd_inner  <= '1';
        usb_if.usb_oe <= '1';
        interrupt     <= '1';
        addrw         <= 0;
        read_state    <= read_stand_by;
      else
        case read_state is
          when read_stand_by =>
            usb_rd_inner  <= '1';
            usb_if.usb_oe <= '1';
            interrupt     <= '0';
            addrw         <= 0;
            if usb_if.usb_rxf = '0' then
              read_state <= read_state_1;
            else
              read_state <= read_stand_by;
            end if;
          when read_state_1 =>
            usb_rd_inner  <= '1';
            usb_if.usb_oe <= '0';
            interrupt     <= '1';
            read_state    <= read_state_2;
          when read_state_2 =>
            interrupt     <= '1';
            usb_if.usb_oe <= '0';
            if (usb_rd_inner = '0' and usb_if.usb_rxf = '0') then
              addrw             <= addrw + 1;
              my_RegBank(addrw) <= usb_if.usb_data;
              dina              <= usb_if.usb_data;
            end if;
            if usb_if.usb_rxf = '0' then
              usb_rd_inner <= '0';
              read_state   <= read_state_2;
            else
              usb_rd_inner <= '1';
              read_state   <= read_wait_state;
            end if;
          when read_wait_state =>
            usb_rd_inner  <= '1';
            usb_if.usb_oe <= '1';
            interrupt     <= '1';
            -- MaxAdd       <= '0'&addrw;
            MaxAdd        <= addrw;
            if dina = X"FF" then
              read_state <= read_stand_by;
            elsif usb_if.usb_rxf = '0' then
              read_state <= read_state_1;
            else
              read_state <= read_wait_state;
            end if;
        end case;
      end if;
    end if;
  end process;

  -- ** Peripherals

  -- This logic deals with the out signals to the peripherals from the [[*Orders][order]].

  -- *** perpherals clock domain
  --
  -- All peripherals opperate in the same clock domain, in both directions

  p1_if.clk <= usb_if.clk_2232;
  p2_if.clk <= usb_if.clk_2232;
  p3_if.clk <= usb_if.clk_2232;
  p4_if.clk <= usb_if.clk_2232;
  p5_if.clk <= usb_if.clk_2232;
  p6_if.clk <= usb_if.clk_2232;

  -- *** peripherals enable

  -- These signals enables the peripherals. When there is an =interrupt=, at
  -- =main=rst=, or when =rst_write= is high the peripherals are disabled.

  pX_enable <= "000000" when (main_rst = '1' or interrupt = '1' or rst_write = '1') else
               "000001" when my_RegBank(0) = X"01" else
               "000010" when my_RegBank(0) = X"06" else
               "000100" when my_RegBank(0) = X"CC" else
               "001000" when my_RegBank(0) = X"07" else
               "010000" when my_RegBank(0) = X"08" else
               "100000" when my_RegBank(0) = X"09" else
               "000000";

  p1_if.enable <= pX_enable(0);
  p2_if.enable <= pX_enable(1) or pX_enable(5);
  p3_if.enable <= pX_enable(2);
  p4_if.enable <= pX_enable(3);
  p5_if.enable <= pX_enable(4);
  p6_if.enable <= pX_enable(5);

  -- *** peripherals read

  -- This hack helps to handle just one =pX_read= signal in the [[*USB out][USB out]] state
  -- machine, avoiding several nested ifs.

  p1_if.rd <= pX_enable(0) and pX_read;
  p2_if.rd <= pX_enable(1) and pX_read;
  p3_if.rd <= pX_enable(2) and pX_read;
  p4_if.rd <= pX_enable(3) and pX_read;
  p5_if.rd <= pX_enable(4) and pX_read;
  p6_if.rd <= pX_enable(5) and pX_read;

  -- *** peripherals params

  -- *>* px params

  -- Here I create the parameters of each command. They are forced to 0
  -- when their corresponding command is not the current.

  -- The =px_params= array takes =px_params_width= bytes from the =RegBank= array
  -- starting at position 1 (as byte 0 is the command).

  -- p1 parameters
  U_p1_params : for i in 0 to p1_params_width-1 generate
    p1_params(p1_params'left-i*8 downto p1_params'left-i*8 - 7) <=
      my_RegBank(i+1);
  end generate;
  p1_if.params(0) <= p1_params when my_RegBank(0) = X"01" else (others => '0');

  -- Here, I need to send =p2_params_inner= even when not =my_RegBank(0) = X"06"=.
  -- Otherwise, when computing s-curve the =EnablePhysicalData= is zero, and no
  -- =NewEventByChannel= is available.

  -- p2 parameters
  U_p2_if_params : for i in 0 to nb_asics-1 generate
    -- Fill in array with contents in reg bank
    u_p2_params : for j in 0 to p2_params_width-1 generate
      p2_params(i)(p2_params(i)'left-j*8 downto p2_params(i)'left-j*8 - 7) <=
        my_RegBank(p2_params_width*i + j +1);
    end generate;
    -- Put array at its right place
    p2_if.params(i) <= p2_params(i) when my_RegBank(0) = X"06" else p2_if.params(i);
  end generate;

  -- p3 parameters
  U_p3_if_params : for i in 0 to nb_asics-1 generate
    -- Fill in array with contents in reg bank
    u_p3_params : for j in 0 to p3_params_width-1 generate
      p3_params(i)(p3_params(i)'left-j*8 downto p3_params(i)'left-j*8 - 7) <=
        my_RegBank(p3_params_width*i + j +1);
    end generate;
    -- Put array at its right place
    p3_if.params(i) <= p3_params(i) when my_RegBank(0) = X"CC" else p3_if.params(i);
  end generate;

  -- p4 parameters
  U_p4_params : for i in 0 to p4_params_width-1 generate
    p4_params(p4_params'left-i*8 downto p4_params'left-i*8 - 7) <=
      my_RegBank(i+1);
  end generate;
  p4_if.params(0) <= p4_params when my_RegBank(0) = X"07" else (others => '0');

  -- p5 parameters
  U_p5_params : for i in 0 to p5_params_width-1 generate
    p5_params(p5_params'left-i*8 downto p5_params'left-i*8 - 7) <=
      my_RegBank(i+1);
  end generate;
  p5_if.params(0) <= p5_params when my_RegBank(0) = X"08" else (others => '0');

  -- p6 parameters
  U_p6_params : for i in 0 to p6_params_width-1 generate
    p6_params(p6_params'left-i*8 downto p6_params'left-i*8 - 7) <=
      my_RegBank(i+1);
  end generate;
  p6_if.params(0) <= p6_params when my_RegBank(0) = X"09" else (others => '0');

  -- ** Disable 40MHz

  -- The order =“0A”= disables the 40MHz clock. Here I just set a bit.

  U_disable_40Hz : process (usb_if.clk_2232)
  begin
    if rising_edge(usb_if.clk_2232) then
      if my_RegBank(0) = X"0A" then
        disable_40MHz <= my_RegBank(1)(0);
      end if;
    end if;
  end process;

  -- ** Usb out fsm

  -- Sends data back from the FPGA to the PC.

  -- This process sends data to the pc throught the usb if. Implements
  -- section 4, figure 4.4, "Write Timing" (lower half) of reference
  -- [[*FT2232H][FT2232H]]; additional details are given in section 4.4.2.

  rst_write <= '0' when (my_RegBank(0) = X"0B" or my_RegBank(0) = X"05" or my_RegBank(0) = X"04" or
                         my_RegBank(0) = X"01" or my_RegBank(0) = X"06" or my_RegBank(0) = X"CC" or
                         my_RegBank(0) = X"07" or my_RegBank(0) = X"08" or my_RegBank(0) = X"09") else '1';

  -- When this signal is set high, the [[*USB out][USB out]] block is on hold and
  -- =pX_enable= is set to 0, effectively disabling all peripherals. Then, when
  -- a valid order appears, it goes low, releasing the data readout
  -- functionality.

  U_USBOut : process(usb_if.clk_2232)
  begin

    if rising_edge(usb_if.clk_2232) then

      -- *** Sync Reset
      if (interrupt = '1' or rst_write = '1' or main_rst = '1') then

        usb_if.usb_wr   <= '1';
        addrrd          <= 0;
        usb_if.usb_data <= (others => 'Z');
        txe_one         <= '1';
        pX_read         <= '0';
        if (my_RegBank(0) = X"01" or my_RegBank(0) = X"06" or my_RegBank(0)
            = X"CC" or my_RegBank(0) = X"07" or my_RegBank(0) = X"08" or
            my_RegBank(0) = X"09") then
          write_state <= write_wait_data_pulse;
        elsif my_RegBank(0) = X"0B" then
          -- X"0B" readout control register
          write_state <= write_control_register;
        else
          -- X"05" returns register bank
          -- X"04" returns a ramp
          write_state <= write_stand_by;
        end if;

      else

        case write_state is

          -- *** readout pulses
          when write_wait_data_pulse =>
            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '1');
            addrrd          <= 0;
            txe_one         <= '1';
            if (p1_if.empty = '0' and usb_if.usb_txe = '0') or
              (p2_if.empty = '0' and usb_if.usb_txe = '0') or
              (p3_if.empty = '0' and usb_if.usb_txe = '0') or
              (p4_if.empty = '0' and usb_if.usb_txe = '0') or
              (p5_if.empty = '0' and usb_if.usb_txe = '0') or
              (p6_if.empty = '0' and usb_if.usb_txe = '0') then
              pX_read     <= '0';
              write_state <= write_take_data_pulse_2;
            else
              pX_read     <= '0';
              write_state <= write_wait_data_pulse;
            end if;

          when write_take_data_pulse_2 =>
            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '1');
            addrrd          <= 0;
            pX_read         <= '1';
            txe_one         <= '1';
            if pX_enable(0) = '1' then
              data_taken(data_taken'left downto data_taken'left+1-p1_data_width*8) <= p1_if.data;
              max_loop                                                             <= p1_data_width;
            elsif pX_enable(1) = '1' then
              data_taken(data_taken'left downto data_taken'left+1-(p2_data_width+2)*8) <= p2_if.data & BoardID;
              max_loop                                                                 <= p2_data_width+2;
            elsif pX_enable(2) = '1' then
              data_taken(data_taken'left downto data_taken'left+1-(p3_data_width+2)*8) <= p3_if.data & BoardID;
              max_loop                                                                 <= p3_data_width+2;
            elsif pX_enable(3) = '1' then
              data_taken(data_taken'left downto
                         data_taken'left+1-p4_data_width*8) <=
                p4_if.data(15 downto 0) & p4_if.data(31 downto 16) &
                p4_if.data(47 downto 32) & p4_if.data(63 downto 48) &
                p4_if.data(79 downto 64) & p4_if.data(95 downto 80) &
                p4_if.data(111 downto 96) & p4_if.data(127 downto 112);
              max_loop <= p4_data_width;
            elsif pX_enable(4) = '1' then
              data_taken(data_taken'left downto data_taken'left+1-p5_data_width*8) <= p5_if.data;
              max_loop                                                             <= p5_data_width;
            else
              data_taken(data_taken'left downto data_taken'left+1-p6_data_width*8) <= p6_if.data;
              max_loop                                                             <= p6_data_width;
            end if;
            write_state <= write_send_data_pulse;

          when write_send_data_pulse =>

            pX_read <= '0';
            if (addrrd = max_loop and usb_if.usb_txe = '0') then
              usb_if.usb_wr   <= '1';
              txe_one         <= '1';
              usb_if.usb_data <= (others => '1');
              write_state     <= write_wait_data_pulse;
            elsif usb_if.usb_txe = '0' then
              addrrd          <= addrrd + 1;
              usb_if.usb_wr   <= '0';
              txe_one         <= '1';
              usb_if.usb_data <= data_taken(data_taken'left downto data_taken'left+1-8);
              saved_word      <= data_taken;
              data_taken      <= data_taken(data_taken'left+1-9 downto 0) & X"FF";
              write_state     <= write_send_data_pulse;
            else
              if (addrrd > 0 and txe_one = '1') then
                addrrd <= addrrd - 1;
              end if;
              txe_one         <= '0';
              usb_if.usb_wr   <= '1';
              usb_if.usb_data <= (others => '1');
              data_taken      <= saved_word;
              write_state     <= write_send_data_pulse;
            end if;

          -- *** readout register bank
          when write_stand_by =>
            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '0');
            addrrd          <= addrrd;
            if usb_if.usb_txe = '0' then
              write_state <= write_readout;
            else
              write_state <= write_stand_by;
            end if;

          when write_readout =>
            if (addrrd = MaxAdd and my_RegBank(0) = X"05") then
              usb_if.usb_wr   <= '1';
              usb_if.usb_data <= (others => '0');
              addrrd          <= 0;
              write_state     <= write_end;
            elsif usb_if.usb_txe = '0' then
              usb_if.usb_wr <= '0';
              if my_RegBank(0) = X"05" then
                -- usb_if.usb_data <= my_RegBank(to_integer(addrrd));
                usb_if.usb_data <= my_RegBank(addrrd);
              else
                usb_if.usb_data <= (others => '0');  -- std_logic_vector(addrrd(7 downto 0));
              end if;
              addrrd      <= addrrd + 1;
              old_addrrb  <= addrrd;
              write_state <= write_readout;
            else
              usb_if.usb_wr   <= '1';
              usb_if.usb_data <= (others => '0');
              addrrd          <= old_addrrb;
              write_state     <= write_readout;
            end if;

          when write_end =>
            usb_if.usb_wr   <= '1';
            usb_if.usb_data <= (others => '0');
            addrrd          <= 0;
            write_state     <= write_end;

          -- *** readout control register
          when write_control_register =>
            usb_if.usb_wr   <= '0';
            usb_if.usb_data <= control_register;
            write_state     <= write_end;

        end case;

      end if;

    end if;

  end process;

end architecture Behavioral;

-- * References

-- ** FT2232H

-- The communication is based on the FT245 Style Synchronous FIFO Interface,
-- as detailed in the document [[file:~/Projects/apc/juno/CatiROC-test-firmware/doc/DS_FT2232H.pdf][DS_FT2232H.pdf]].
