-- * catiroc_slowcontrol

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use work.common_package.all;

-- * Entity

entity catiroc_slowcontrol is
  port (clk_sc       : in    std_logic;            -- slow control clock
        clk_80MHz    : in    std_logic;
        global_reset : in    std_logic;
        catirocs     : inout rc_catiroc_array;     -- all asics
        p_if         : inout rc_peripheral_if_p3;  -- peripheral interface
        -- out
        resetb       : out   std_logic;
        start_system : out   std_logic);
end entity catiroc_slowcontrol;

-- * Architecture

architecture Behavioral of catiroc_slowcontrol is

  -- ** Signals

  signal fifo_dataout : t_data(0 to nb_asics-1)(p3_data_width*8-1 downto 0) := (others => (others => '0'));
  signal fifo_empty   : t_std_logic_vector_nb_asics                         := (others => '1');
  signal fifo_rd      : t_std_logic_vector_nb_asics                         := (others => '0');
  signal fifo_clk     : std_logic                                           := '0';

begin

  -- ** nb_asics slow control instances

  U_catiroc_sc : for i in 0 to nb_asics-1 generate
    u_catiroc_sc_1ch : entity work.catiroc_slowcontrol_1ch
      port map (clk_sc       => clk_sc,
                global_reset => global_reset,
                resetb       => resetb,
                start_system => start_system,
                catiroc      => catirocs(i),
                enable       => p_if.enable,
                params       => p_if.params(i),
                --
                fifo_clk     => fifo_clk,
                fifo_empty   => fifo_empty(i),
                fifo_rd      => fifo_rd(i),
                fifo_dataout => fifo_dataout(i));
  end generate;

  -- TODO: replace empty, rd, data and clk by a type

  -- ** multiplexor nb_asics to 1

  fifo_clk <= clk_80MHz;

  -- Interfaces the output of all catiroc managers to the usb interface. This is
  --    necessary to act as a multiplexer from “nb_asics” to one. This means that this
  --    modules reads data from “nb_asics”, “input_bus_width_bits”-bits data fifos and
  --    tramsfers it to a single “input_bus_width_bits”-bits data fifo to be accessed
  --    from the [[*USB Interface][USB Interface]].

  --    This instance transfers the slow control registers read from the “sc_out” of
  --    the ASICS. P3 command.

  -- The multiplexor, to send data from =nb_asics= modules to the [[*USB Interface][USB Interface]]. I do
  --    use =extend= here, so the data i/o data bus width is not respected: at the output,
  --    we bet one more byte including the asic number from where the data originates.

  -- TODO: Try a single peripheral type, and derive all remaining types from
  -- here. then, enter a peripheral, avoiding 4 single signals

  U_catiroc_usb_if_sc : entity work.catiroc_usb_if
    generic map (nb_channels          => nb_asics,
                 input_bus_width_bits => p3_data_width*8,
                 extend               => 1)
    port map (global_reset => global_reset,
              -- incoming data from nb_asics ASICS
              asic_clk     => fifo_clk,
              asic_empty   => fifo_empty,
              asic_rd      => fifo_rd,
              asic_data    => fifo_dataout,
              -- if to usb managager
              fifo_clk     => p_if.clk,
              fifo_dataout => p_if.data,
              fifo_rd      => p_if.rd,
              fifo_empty   => p_if.empty);

end architecture Behavioral;
