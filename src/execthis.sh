#!/usr/sbin/sh

nb_asics=$1
nb_discri_ch=$2
regbanklength=$3
topfile=$4

sed -i.delete "s/nb_asics.*: natural.*;/nb_asics : natural range 1 to 8 := $nb_asics;/" $topfile
sed -i.delete "s/num_of_discri_channels.*: natural.*;/num_of_discri_channels : natural range 1 to 16 := $nb_discri_ch;/" $topfile
sed -i.delete "s/RegBankLength.*: natural.*;/RegBankLength : natural := $regbanklength;/" $topfile
sed -i.delete "s/val\_evt\s\{1,\}: out\s\{1,\}std\_logic;/val_evt_p : out std_logic;\nval_evt_n : out std_logic;/" $topfile

emacs $topfile --batch --eval="(progn (setq vhdl-basic-offset 3)(delete-trailing-whitespace)(vhdl-beautify-buffer))"
