-- * Common package

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use ieee.numeric_std.all;

-- * Package

package common_package is

   -- ** Signals

   -- ** Constants

   -- ** Generics

   constant c_nb_discri_channels : natural range 1 to 16 := 16;

   --! Architecture simple description
   constant freq_source : natural range 1 to 42 := 40;

   constant nb_peripherals : natural range 1 to 8 := 6;
   constant nb_asics       : natural range 1 to 8 := 8;

   constant p1_data_width   : natural range 1 to 16 := 16;  -- bytes
   constant p1_params_width : natural range 1 to 4  := 4;   -- bytes

   constant p2_data_width   : natural range 1 to 16 := 8;  -- bytes
   constant p2_params_width : natural range 1 to 4  := 4;  -- bytes

   constant p3_data_width   : natural range 1 to 8  := 1;   -- bytes
   constant p3_params_width : natural range 1 to 66 := 66;  -- bytes

   constant p4_data_width   : natural range 1 to 16 := 16;  -- bytes
   constant p4_params_width : natural range 1 to 8  := 2;   -- bytes

   constant p5_data_width   : natural range 1 to 16 := 1;  -- bytes
   constant p5_params_width : natural range 1 to 8  := 3;  -- bytes

   constant p6_data_width   : natural range 1 to 16 := 16;  -- bytes
   constant p6_params_width : natural range 1 to 8  := 4;   -- bytes

   -- ** Types

   -- *** Misc

   type t_asic_data is array (natural range <>) of std_logic_vector;

   type t_nbasics_2bytes is array(0 to nb_asics-1) of std_logic_vector(15 downto 0);

   subtype t_std_logic_vector_nb_asics is std_logic_vector(nb_asics-1 downto 0);

   type TriggerCounters is array(0 to 31) of unsigned(15 downto 0);

   subtype t_Byte is std_logic_vector(7 downto 0);

   type dataout_fifo_state_type2 is (standby, write_tempo);

-- state signal in for wriiting in fifo
   -- type write_fifo_state is (write_standby, write_end, write_dataSignals, write_toFifo, write_countor);

-- state signal in SLOW control fsmfreq_source            : natural range 1 to 42 := 40

--type slow_control_read_state is (write_sl_standby,write_sl_reset,write_sl);

-- state signal in for wriiting in fifo
   type dataout_fifo_state_type is (standby, write_tempo);

   type mystate_type is (mystate_state_1, mystate_state_2);

   type inspec is array (7 downto 0) of std_logic_vector (23 downto 0);

   type t_data is array (natural range <>) of std_logic_vector;

   -- type t_fifo_base is array (0 to nb_asics-1) of std_logic_vector;

   -- -- subtype t_fifo_base_p2 is t_fifo_base;

   -- type t_fifo_p2 is record
   --    fifo_clk     : std_logic;
   --    fifo_dataout : t_fifo_base(p2_data_width*8-1 downto 0);
   --    fifo_empty   : t_std_logic_vector_nb_asics;
   --    fifo_rd      : t_std_logic_vector_nb_asics;
   -- end record t_fifo_in;

   -- *** Peripherals

   -- TODO: Try a single peripheral type, and derive all remaining types from here

   type rc_params_p1 is array (natural range <>) of std_logic_vector(p1_params_width*8-1 downto 0);
   type rc_params_p2 is array (natural range <>) of std_logic_vector(p2_params_width*8-1 downto 0);
   type rc_params_p3 is array (natural range <>) of std_logic_vector(p3_params_width*8-1 downto 0);
   type rc_params_p4 is array (natural range <>) of std_logic_vector(p4_params_width*8-1 downto 0);
   type rc_params_p5 is array (natural range <>) of std_logic_vector(p5_params_width*8-1 downto 0);
   type rc_params_p6 is array (natural range <>) of std_logic_vector(p6_params_width*8-1 downto 0);

   -- subtype rc_params_p1 is array (0 to 0) of std_logic_vector(p1_params_width*8-1 downto 0);

   type rc_peripheral_if_p1 is record
      clk    : std_logic;
      -- in
      params : rc_params_p1;
      enable : std_logic;
      -- out
      rd     : std_logic;
      data   : std_logic_vector(p1_data_width*8-1 downto 0);
      empty  : std_logic;
   end record rc_peripheral_if_p1;

   type rc_peripheral_if_p2 is record
      clk    : std_logic;
      -- in
      params : rc_params_p2;
      enable : std_logic;
      -- out
      rd     : std_logic;
      data   : std_logic_vector((p2_data_width+1)*8-1 downto 0);
      empty  : std_logic;
   end record rc_peripheral_if_p2;

   type rc_peripheral_if_p3 is record
      clk    : std_logic;
      -- in
      params : rc_params_p3;
      enable : std_logic;
      -- out
      rd     : std_logic;
      data   : std_logic_vector((p3_data_width+1)*8-1 downto 0);
      empty  : std_logic;
   end record rc_peripheral_if_p3;

   type rc_peripheral_if_p4 is record
      clk    : std_logic;
      -- in
      params : rc_params_p4;
      enable : std_logic;
      -- out
      rd     : std_logic;
      data   : std_logic_vector(p4_data_width*8-1 downto 0);
      empty  : std_logic;
   end record rc_peripheral_if_p4;

   type rc_peripheral_if_p5 is record
      clk    : std_logic;
      -- in
      params : rc_params_p5;
      enable : std_logic;
      -- out
      rd     : std_logic;
      data   : std_logic_vector(p5_data_width*8-1 downto 0);
      empty  : std_logic;
   end record rc_peripheral_if_p5;

   type rc_peripheral_if_p6 is record
      clk    : std_logic;
      -- in
      params : rc_params_p6;
      enable : std_logic;
      -- out
      rd     : std_logic;
      data   : std_logic_vector(p6_data_width*8-1 downto 0);
      empty  : std_logic;
   end record rc_peripheral_if_p6;

   -- subtype rc_peripheral_if_p1 is
   --    rc_peripheral_if(data(p1_data_width*8-1 downto 0), params(0 to 0));

   -- subtype rc_peripheral_if_p2 is
   --    rc_peripheral_if(data((p2_data_width+1)*8-1 downto 0), params(0 to nb_asics-1));

   -- subtype rc_peripheral_if_p3 is
   --    rc_peripheral_if(data((p3_data_width+1)*8-1 downto 0), params(0 to nb_asics-1));

   -- subtype rc_peripheral_if_p4 is
   --    rc_peripheral_if(data(p4_data_width*8-1 downto 0), params(0 to 0));

   -- subtype rc_peripheral_if_p5 is
   --    rc_peripheral_if(data(p5_data_width*8-1 downto 0), params(0 to 0));

   -- subtype rc_peripheral_if_p6 is
   --    rc_peripheral_if(data(p6_data_width*8-1 downto 0), params(0 to 0));

   -- type rc_peripherals is record
   --    p1_if : rc_peripheral_if_p1;
   --    p2_if : rc_peripheral_if_p2;
   --    p3_if : rc_peripheral_if_p3;
   --    p4_if : rc_peripheral_if_p4;
   --    p5_if : rc_peripheral_if_p5;
   --    p6_if : rc_peripheral_if_p6;
   -- end record rc_peripherals;

   -- *** Usb if

   type rc_usb_if is record
      usb_rxf   : std_logic;
      usb_siwua : std_logic;
      usb_txe   : std_logic;
      clk_2232  : std_logic;                     -- usb clock, 60 MHz
      usb_oe    : std_logic;
      usb_rd    : std_logic;
      usb_wr    : std_logic;
      usb_data  : std_logic_vector(7 downto 0);  -- usb IO data bus
   end record rc_usb_if;

   -- *** Diff pair

   type rc_diff_pair is record
      p : std_logic;
      n : std_logic;
   end record rc_diff_pair;

   type rc_diff_pair_array is array (natural range <>) of rc_diff_pair;

   -- *** Catiroc

   type rc_catiroc_sc is record
      sr_clk    : std_logic;
      sr_in     : std_logic;            -- serialized data to configure cati
      sr_rst    : std_logic;  -- reset cati before sending configuration
      sr_select : std_logic;            -- to be able to write in cati
      sr_out    : std_logic;  -- serialized data to read back conf sen
   end record rc_catiroc_sc;

   type rc_catiroc is record
      cati_dataout : rc_diff_pair_array(1 downto 0);
      cati_strobe  : std_logic_vector (1 downto 0);
      sc           : rc_catiroc_sc;     -- slow control related
      val_evt      : rc_diff_pair_array(0 to 0);
      clk_40MHz    : rc_diff_pair_array(0 to 0);
      clk_160MHz   : rc_diff_pair_array(0 to 0);
      triggers     : std_logic_vector (15 downto 0);
      trig_ext     : std_logic;
      pwr_on       : std_logic;
      resetb       : std_logic;
      StartSys     : std_logic;
      ovf          : std_logic;
   end record rc_catiroc;

   type rc_catiroc_array is array (natural range <>) of rc_catiroc;

end common_package;
