-- * catiroc_slowcontrol_1ch

--! @file
--! @brief Example peripheral implementing a simple up counter
--!
--! This example peripheral gives an example of use of a simple counter
--! It initializes four
--! ble, ble ...
--! bly, bly ...

-- * Libraries

library IEEE;
library UNISIM;

-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
use UNISIM.VComponents.all;

-- * Entity

entity catiroc_slowcontrol_1ch is
  port (clk_sc       : in    std_logic;   -- generic oscilator present on board
        global_reset : in    std_logic;
        catiroc      : inout rc_catiroc;  -- cati
        resetb       : out   std_logic;
        start_system : out   std_logic;
        enable       : in    std_logic;
        params       : in    std_logic_vector(p3_params_width*8-1 downto 0);
        --
        fifo_clk     : in    std_logic;
        fifo_rd      : in    std_logic;
        fifo_empty   : out   std_logic;
        fifo_dataout : out   std_logic_vector(p3_data_width*8-1 downto 0));
end entity catiroc_slowcontrol_1ch;

-- * Architecture

architecture Behavioral of catiroc_slowcontrol_1ch is

  -- ** Types

  type slow_control_write_state is (write_sl_standby, write_sl_resetb,
                                    write_sl_reset, write_sl, write_sl_tempo_1, write_sl_tempo_2, write_sl_probe,
                                    write_sl_run);

  -- ** Signals

  signal fifo_data_in                 : std_logic_vector (7 downto 0)   := (others => '0');
  signal reset_catiCount              : std_logic_vector (3 downto 0)   := (others => '0');
  signal Slow_ControlCount            : unsigned (11 downto 0)          := (others => '0');
  signal Slow_Control_reg             : std_logic_vector (327 downto 0) := (others => '0');
  signal Probe_reg                    : std_logic_vector (193 downto 0) := (others => '0');
  signal start_readback_SC            : std_logic                       := '0';
  signal start_readback_PB            : std_logic                       := '0';
  signal readbackcount                : std_logic_vector(3 downto 0)    := (others => '0');
  signal write_sl_state               : slow_control_write_state;
  signal fifo_wr                      : std_logic                       := '0';
  signal active_srclk                 : std_logic                       := '0';
  signal probeRead_count              : unsigned (7 downto 0)           := (others => '0');
  signal fifo_clear, not_active_srclk : std_logic                       := '0';
  signal gnd_1bit                     : std_logic                       := '0';
  signal vcc_1bit                     : std_logic                       := '1';
  -- signal wrfull            : std_logic                       := '0';

  -- signal sresetb : std_logic := '0';
  -- signal ssr_rst : std_logic := '0';
  -- signal sr_clk_tmp : std_logic_vector (0 downto 0) := (others => '0');

begin

  -- ** SR to ASIC

  -- Sends slow control registers to ASIC

  process (clk_sc)
  begin

    if rising_edge(clk_sc) then

      if (global_reset = '1' or enable = '0') then

        resetb               <= '1';
        catiroc.sc.sr_in     <= '0';
        catiroc.sc.sr_rst    <= '1';
        catiroc.sc.sr_select <= '0';
        reset_catiCount      <= (others => '0');
        Slow_Control_reg     <= (others => '0');
        Probe_reg            <= (others => '0');
        Slow_ControlCount    <= (others => '0');
        start_readback_SC    <= '0';
        start_readback_PB    <= '0';
        start_system         <= '1';
        active_srclk         <= '0';
        write_sl_state       <= write_sl_standby;

      else

        case write_sl_state is

          when write_sl_standby =>
            start_system      <= '1';
            catiroc.sc.sr_in  <= '0';
            reset_catiCount   <= (others => '0');
            Slow_ControlCount <= (others => '0');
            start_readback_SC <= '0';
            start_readback_PB <= '0';
            active_srclk      <= '0';

            Slow_Control_reg     <= params(527 downto 200);
            Probe_reg            <= params(199 downto 6);
            resetb               <= '1';
            catiroc.sc.sr_rst    <= '1';
            catiroc.sc.sr_select <= '1';
            write_sl_state       <= write_sl_resetb;

          when write_sl_resetb =>

            if(reset_catiCount = X"F") then
              resetb          <= '1';
              reset_catiCount <= (others => '0');
              write_sl_state  <= write_sl_reset;
            else
              reset_catiCount <= std_logic_vector(unsigned(reset_catiCount) + 1);
              resetb          <= '0';
              write_sl_state  <= write_sl_resetb;
            end if;

          when write_sl_reset =>

            if(reset_catiCount = X"F") then
              catiroc.sc.sr_rst <= '1';
              reset_catiCount   <= (others => '0');
              write_sl_state    <= write_sl;
            else
              reset_catiCount   <= std_logic_vector(unsigned(reset_catiCount) + 1);
              catiroc.sc.sr_rst <= '0';
              write_sl_state    <= write_sl_reset;
            end if;


          when write_sl =>

            if(Slow_ControlCount < 656) then  --656 328x2 (2x SlControl + 8bit) -- ca marche avec 664 (656 + 8bit)
              active_srclk      <= '1';
              catiroc.sc.sr_in  <= Slow_Control_reg(327);
              Slow_Control_reg  <= Slow_Control_reg(326 downto 0) & Slow_Control_reg(327);
              Slow_ControlCount <= Slow_ControlCount + 1;
              if (Slow_ControlCount < 328) then  ---1er bit de relecture on a remplis les 328 premiers
                start_readback_SC <= '0';
                write_sl_state    <= write_sl;
              else
                start_readback_SC <= '1';
                write_sl_state    <= write_sl;
              end if;

            else

              active_srclk      <= '0';
              Slow_ControlCount <= (others => '0');
              write_sl_state    <= write_sl_tempo_1;

            end if;

          when write_sl_tempo_1 =>

            start_readback_SC    <= '0';
            catiroc.sc.sr_select <= '0';
            write_sl_state       <= write_sl_tempo_2;

          when write_sl_tempo_2 =>

            write_sl_state <= write_sl_probe;

          when write_sl_probe =>

            if(Slow_ControlCount < 388) then     --388 194x2 (2x PorbeReg)
              active_srclk      <= '1';
              catiroc.sc.sr_in  <= Probe_reg(193);
              Probe_reg         <= Probe_reg(192 downto 0) & Probe_reg(193);
              Slow_ControlCount <= Slow_ControlCount + 1;
                                        --      start_readback_PB <= '1';
              if (Slow_ControlCount < 194) then  ---1er bit de relecture on a remplis les 194 premiers
                start_readback_PB <= '0';
                write_sl_state    <= write_sl_probe;
              else
                start_readback_PB <= '1';
                write_sl_state    <= write_sl_probe;
              end if;
            else
              active_srclk      <= '0';
              Slow_ControlCount <= (others => '0');
              write_sl_state    <= write_sl_run;
            end if;

          when write_sl_run =>

            start_readback_PB <= '0';
            start_system      <= '1';
            write_sl_state    <= write_sl_run;

        end case;

      end if;

    end if;

  end process;

  -- ** SR Readout

  -- Readout of slow control registers back to usb

  process (clk_sc)
  begin

    if rising_edge(clk_sc) then

      if (global_reset = '1' or enable = '0') then

        fifo_data_in    <= (others => '0');
        readbackcount   <= (others => '0');
        probeRead_count <= (others => '0');
        fifo_wr         <= '0';
        fifo_clear      <= '1';

      else

        fifo_clear <= '0';

        if (start_readback_SC = '1') then

          fifo_data_in <= fifo_data_in(6 downto 0) & catiroc.sc.sr_out;
          if (readbackcount = X"7") then
            fifo_wr       <= '1';
            readbackcount <= (others => '0');
          else
            fifo_wr       <= '0';
            readbackcount <= std_logic_vector(unsigned(readbackcount) + 1);
          end if;

        elsif (start_readback_PB = '1') then

          if (probeRead_count < 192) then

            fifo_data_in    <= fifo_data_in(6 downto 0) & catiroc.sc.sr_out;
            probeRead_count <= probeRead_count + 1;
            if (readbackcount = X"7") then
              fifo_wr       <= '1';
              readbackcount <= (others => '0');
            else
              fifo_wr       <= '0';
              readbackcount <= std_logic_vector(unsigned(readbackcount) + 1);
            end if;

          else

            if (readbackcount = X"1") then
              fifo_data_in  <= fifo_data_in(0) & catiroc.sc.sr_out & B"000000";
              fifo_wr       <= '1';
              readbackcount <= (others => '0');
            else
              fifo_data_in  <= fifo_data_in(6 downto 0) & catiroc.sc.sr_out;
              fifo_wr       <= '0';
              readbackcount <= std_logic_vector(unsigned(readbackcount) + 1);
            end if;

          end if;

        else

          fifo_data_in    <= (others => '0');
          fifo_wr         <= '0';
          readbackcount   <= (others => '0');
          probeRead_count <= (others => '0');

        end if;

      end if;

    end if;

  end process;

  U_oddr : ODDR
    generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE",
                INIT         => '0',
                SRTYPE       => "SYNC")
    -- generic map(INIT => '0')
    port map (Q  => catiroc.sc.sr_clk,
              C  => clk_sc,
              CE => vcc_1bit,
              D1 => vcc_1bit,
              D2 => gnd_1bit,
              R  => not_active_srclk,
              S  => gnd_1bit);

  not_active_srclk <= not(active_srclk);

  -- ** Fifo

  Cati_output_SC_inst : entity work.Cati_output_fifoSC
    port map (rst    => fifo_clear,
              --
              wr_clk => clk_sc,
              din    => fifo_data_in,
              wr_en  => fifo_wr,
              --
              rd_clk => fifo_clk,
              rd_en  => fifo_rd,
              dout   => fifo_dataout,
              full   => open,
              empty  => fifo_empty);

end architecture Behavioral;
