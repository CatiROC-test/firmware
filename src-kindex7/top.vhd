-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Xilinx%20libraries][Xilinx libraries:1]]
-- ** Xilinx libraries

library UNISIM;
-- @@@ Xilinx libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Xilinx%20packages][Xilinx packages:1]]
-- ** Xilinx packages

use work.juno_mezzanine_package.all;
use UNISIM.VComponents.all;
-- @@@ Xilinx packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Entity][Entity:1]]
-- * Entity
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Generics][Generics:1]]
-- ** Generics

entity top is
-- @@@ Generics:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Generics:%208%20asics][Generics: 8 asics:1]]
-- *** Generics: 8 asics

   generic (RegBankLength          : natural               := 70;  -- bytes
            nb_asics               : natural range 1 to 8  := 8;   -- bytes
            num_of_discri_channels : natural range 1 to 16 := 16;  -- nb of channels
            freq_source            : natural range 1 to 42 := 40;  -- nb of channels
-- @@@ Generics: 8 asics:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Generics:%20remaining][Generics: remaining:1]]
-- *** Generics: remaining

            p1_data_width   : natural range 1 to 16 := 16;  -- bytes
            p2_data_width   : natural range 1 to 16 := 8;   -- bytes
            p3_data_width   : natural range 1 to 8  := 1;   -- bytes
            p4_data_width   : natural range 1 to 16 := 16;
            p5_data_width   : natural range 1 to 16 := 1;
            p6_data_width   : natural range 1 to 16 := 16;
--
            p1_params_width : natural range 1 to 4  := 4;   -- bytes
            p2_params_width : natural range 1 to 4  := 4;
            p3_params_width : natural range 1 to 66 := 66;
            p4_params_width : natural range 1 to 8  := 2;
            p5_params_width : natural range 1 to 8  := 3;
            p6_params_width : natural range 1 to 8  := 4);  -- bytes
-- @@@ Generics: remaining:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Begin][Begin:1]]
-- ** Begin

   port (
-- @@@ Begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Usb%20if][Usb if:1]]
-- ** Usb if

      usb_rxf  : in    std_logic;
      usb_txe  : in    std_logic;
      clk_2232 : in    std_logic;                     -- usb clock, 60 MHz
      usb_oe   : out   std_logic;
      usb_rd   : out   std_logic;
      usb_wr   : out   std_logic;
      usb_data : inout std_logic_vector(7 downto 0);  -- usb IO data bus
-- @@@ Usb if:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno%20mezzanine][Juno mezzanine:1]]
-- ** Juno mezzanine

-- GCU/FMC
-- gcu_fmc_out_p    : out std_logic_vector(7 downto 0);
-- gcu_fmc_out_n    : out std_logic_vector(7 downto 0);
-- gcu_fmc_in_p     : in  std_logic_vector(9 downto 0);
-- gcu_fmc_in_n     : in  std_logic_vector(9 downto 0);
      cati_dataout_p   : in  std_logic_vector (2*nb_asics-1 downto 0);
      cati_dataout_n   : in  std_logic_vector (2*nb_asics-1 downto 0);
      val_evt_p        : out std_logic;
      val_evt_n        : out std_logic;
      clk_40MHz_out_p  : out std_logic;
      clk_40MHz_out_n  : out std_logic;
      clk_160MHz_out_p : out std_logic;
      clk_160MHz_out_n : out std_logic;
-- Points Test
      PX1              : out std_logic;
      PX2              : out std_logic;
      PX3              : in  std_logic;
      PX4              : out std_logic;
-- @@@ Juno mezzanine:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Rest][Rest:1]]
-- ** Rest

--cati
      pwr_on   : out std_logic;
      resetb   : out std_logic;
      StartSys : out std_logic;
      sr_clk   : out std_logic;

-- serialized data to configure cati
      sr_in       : out std_logic_vector(nb_asics-1 downto 0);
      sr_rst      : out std_logic;  -- reset cati before sending configuration,
      sr_select   : out std_logic;      -- to be able to write in cati,
      sr_out      : in  std_logic_vector(nb_asics-1 downto 0);
      triggers    : in  std_logic_vector (16*nb_asics-1 downto 0);
      trig_ext    : out std_logic;
      ovf         : in  std_logic;      -- overflow of coarse time
-- ASIC 0
      cati_strobe : in  std_logic_vector (2*nb_asics-1 downto 0);
-- generic oscillator present on board 40MHz
      clk_fpga    : in  std_logic);
-- @@@ Rest:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*end][end:1]]
-- ** end

end entity top;
-- @@@ end:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of top is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Signals][Signals:1]]
-- ** Signals
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Control%20delay%20signals][Control delay signals:1]]
-- *** Control delay signals

   type control_delay_type is (cd_1, cd_2, cd_3, cd_end);
   signal cd_state             : control_delay_type;
   signal phasedone, phasestep : std_logic             := '0';
   signal cd_counter           : unsigned(15 downto 0) := (others => '0');
   signal cd_counter_ref       : unsigned(15 downto 0) := (others => '0');
   signal cd_busy              : std_logic             := '1';
-- @@@ Control delay signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Ports][Ports:1]]
-- *** Ports
-- @@@ Ports:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P1][P1:1]]
-- **** P1

   signal p1_datain        : std_logic_vector(p1_data_width*8-1 downto 0);
   signal p1_params        : std_logic_vector(p1_params_width*8-1 downto 0);
   signal p1_rd, p1_enable : std_logic := '0';
   signal p1_empty         : std_logic := '1';
-- @@@ P1:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2][P2:1]]
-- **** P2

-- constant p2_data_width_extended : natural range 1 to 18 := p2_data_width + 1;
-- @@@ P2:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2][P2:2]]
-- **** P2

   signal p2_datain        : std_logic_vector((p2_data_width+1)*8-1 downto 0);
   signal p2_params        : std_logic_vector(p2_params_width*nb_asics*8-1 downto 0);
   signal p2_rd, p2_enable : std_logic := '0';
   signal p2_empty         : std_logic := '1';
-- @@@ P2:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P3][P3:1]]
-- **** P3

-- constant p3_data_width_extended : natural range 1 to 18 := ;
-- @@@ P3:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P3][P3:2]]
-- **** P3

   signal p3_datain        : std_logic_vector((p3_data_width + 1)*8-1 downto 0);
   signal p3_params        : std_logic_vector(p3_params_width*nb_asics*8-1 downto 0);
   signal p3_rd, p3_enable : std_logic := '0';
   signal p3_empty         : std_logic := '1';
-- @@@ P3:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P4][P4:1]]
-- **** P4

   signal p4_datain        : std_logic_vector(p4_data_width*8-1 downto 0);
   signal p4_params        : std_logic_vector(p4_params_width*8-1 downto 0);
   signal p4_rd, p4_enable : std_logic := '0';
   signal p4_empty         : std_logic := '1';
-- @@@ P4:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P5][P5:1]]
-- **** P5

   signal p5_datain        : std_logic_vector(p5_data_width*8-1 downto 0);
   signal p5_params        : std_logic_vector(p5_params_width*8-1 downto 0);
   signal p5_rd, p5_enable : std_logic := '0';
   signal p5_empty         : std_logic := '1';
-- @@@ P5:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P6][P6:1]]
-- **** P6

   signal p6_datain        : std_logic_vector(p6_data_width*8-1 downto 0);
   signal p6_params        : std_logic_vector(p6_params_width*8-1 downto 0);
   signal p6_rd, p6_enable : std_logic := '0';
   signal p6_empty         : std_logic := '1';
-- @@@ P6:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Signals%20in%20between%20catiroc%20manager%20and%20catiroc/usb%20interface][Signals in between catiroc manager and catiroc/usb interface:1]]
-- *** Signals in between catiroc manager and catiroc/usb interface

   signal p2_data_fifo                 : std_logic_vector(p2_data_width*nb_asics*8-1 downto 0) := (others => '0');
   signal p3_data_fifo                 : std_logic_vector(p3_data_width*nb_asics*8-1 downto 0) := (others => '0');
   signal p2_empty_fifo, p3_empty_fifo : std_logic_vector(nb_asics-1 downto 0)                 := (others => '1');
   signal p2_read_fifo, p3_read_fifo   : std_logic_vector(nb_asics-1 downto 0)                 := (others => '0');
-- @@@ Signals in between catiroc manager and catiroc/usb interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Signals%20in%20between%20s-curve%20manager%20and%20usb%20interface][Signals in between s-curve manager and usb interface:1]]
-- *** Signals in between s-curve manager and usb interface

   signal p6_data_fifo  : std_logic_vector(128*nb_asics-1 downto 0) := (others => '0');
   signal p6_empty_fifo : std_logic_vector(nb_asics-1 downto 0)     := (others => '1');
   signal p6_read_fifo  : std_logic_vector(nb_asics-1 downto 0)     := (others => '0');
-- @@@ Signals in between s-curve manager and usb interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Inspection%20signals][Inspection signals:1]]
-- *** Inspection signals

   signal px1_input   : inspec := (others => (others => '0'));
   signal px2_input   : inspec := (others => (others => '0'));
   signal px1_sel_tmp : std_logic_vector(15 downto 0);
   signal px2_sel_tmp : std_logic_vector(15 downto 0);
-- @@@ Inspection signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Remaining%20signals][Remaining signals:1]]
-- *** Remaining signals

   signal cati_dataout_sampled                        : std_logic_vector (2*nb_asics-1 downto 0);
   signal cati_strobe_sampled                         : std_logic_vector (2*nb_asics-1 downto 0);
   signal cati_dataout_in                             : std_logic_vector (2*nb_asics-1 downto 0);
   signal cati_strobe_in                              : std_logic_vector (2*nb_asics-1 downto 0);
   signal clk_40Mhz_pll_tmp, clk_10MHz_nonshifted_tmp : std_logic_vector(0 downto 0);
   signal vcc, gnd                                    : std_logic_vector(0 downto 0);
-- signal gcu_fmc_out                                 : std_logic_vector(7 downto 0);
-- signal gcu_fmc_in                                  : std_logic_vector(9 downto 0);
   signal clk_40MHz_out, not_cd_busy                  : std_logic                                := '0';
   signal clk_160MHz_out, global_reset, interrupt     : std_logic;
   signal trig                                        : std_logic_vector(127 downto 0)           := (others => '0');
   signal init_standby_period                         : unsigned(19 downto 0)                    := (others => '0');
   signal main_rst                                    : std_logic                                := '1';
   signal cati_output1                                : std_logic_vector(127 downto 0)           := (others => '0');
   signal usb_data_forFifo1                           : std_logic_vector(31 downto 0)            := (others => '0');
   signal cati_output2                                : std_logic_vector(127 downto 0)           := (others => '0');
   signal usb_params2                                 : std_logic_vector(7 downto 0)             := (others => '0');
   signal cati_output3                                : std_logic_vector(7 downto 0)             := (others => '0');
   signal SlowContr_forCati                           : std_logic_vector(527 downto 0)           := (others => '0');
   signal read_fifos                                  : std_logic_vector(2 downto 0)             := (others => '0');
   signal fifos_empty                                 : std_logic_vector(2 downto 0)             := (others => '1');
   signal usb_SlowControlReady, clk_adc, locked_sig   : std_logic                                := '0';
   signal clk_40MHz, clk_80MHz, clk_10MHz, clk_160MHz : std_logic                                := '0';
   signal clk_80MHz_delayed                           : std_logic                                := '0';
   signal disable_40MHz                               : std_logic                                := '0';
   signal resetb_tmp, StartSys_tmp, sr_clk_tmp        : std_logic_vector(nb_asics-1 downto 0)    := (others => '0');
   signal p2_empty_tmp                                : std_logic;
   signal sr_rst_tmp, sr_select_tmp                   : std_logic_vector(nb_asics-1 downto 0)    := (others => '0');
   signal discri_falling                              : std_logic_vector(nb_asics*16-1 downto 0) := (others => '0');
   signal clk_gcu, external_trigger_main_rst          : std_logic;
-- @@@ Remaining signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Remaining%20signals][Remaining signals:2]]
-- *** Remaining signals

   alias BoardID : std_logic_vector(7 downto 0) is p2_params(23 downto 16);
-- @@@ Remaining signals:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Remaining%20signals][Remaining signals:3]]
-- *** Remaining signals

   type NewEventByChannel_type is array(0 to nb_asics-1) of std_logic_vector(15 downto 0);
   signal NewEventByChannel : NewEventByChannel_type := (others => X"0000");
-- @@@ Remaining signals:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- *** Juno

   signal cati_dataout           : std_logic_vector (2*nb_asics-1 downto 0);
   signal clk_480MHz, clk_120MHz : std_logic := '0';
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Generic%20logic][Generic logic:1]]
-- ** Generic logic

-- s_triggers <= triggers;
-- trig_ext <= ovf;
   pwr_on <= '1';
-- @@@ Generic logic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*PX][PX:1]]
-- *** PX

   PX4 <= '0';
-- @@@ PX:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Inspections][Inspections:1]]
-- *** Inspections

   Inspec_loop_1 :
   for i in 0 to nb_asics-1 generate
      px1_input(i)(0) <= cati_dataout_in(2*i);
      -- px1_input(i)(1) <= cati_dataout_in(2*i+1);
      Inspec_loop_2 :
      for j in 1 to 16 generate
         px1_input(i)(j) <= discri_falling ((16 * i) + (j - 1));
      end generate;
      px2_input(i)(0) <= cati_strobe_in(2*i);
      -- px2_input(i)(1) <= cati_strobe_in(2*i+1);
      Inspec_loop_3 :
      for j in 1 to 16 generate
         px2_input(i)(j) <= discri_falling ((16 * i) + (j - 1));
      end generate;
   end generate;

   PX1_Inst : inspection
      port map (input_i => px1_input,
                sel     => px1_sel_tmp,
                output  => px1);

   PX2_Inst : inspection
      port map (input_i => px2_input,
                sel     => px2_sel_tmp,
                output  => px2);
-- @@@ Inspections:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*xilinx][xilinx:1]]
-- **** xilinx

-- raz_ext  <= '0';
-- read_ext <= '0';
-- hold_ext <= '0';
-- @@@ xilinx:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*juno][juno:1]]
-- *** juno

   U_OBUFDS_1 : OBUFDS
      port map (I  => StartSys_tmp(0),
                O  => val_evt_p,
                OB => val_evt_n);
-- @@@ juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Main%20reset%20logic][Main reset logic:2]]
-- ** Main reset logic

   U_main : process (clk_2232)
   begin
      if clk_2232'event and clk_2232 = '1' then
         if (locked_sig = '0') then
            init_standby_period <= (others => '0');
            main_rst            <= '1';
         elsif (init_standby_period /= X"FFFFF") then
            init_standby_period <= init_standby_period + 1;
            main_rst            <= '1';
         else
            main_rst <= '0';
         end if;
      end if;
   end process U_main;
-- @@@ Main reset logic:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Main%20reset%20logic][Main reset logic:3]]
-- ** Main reset logic

   global_reset <= interrupt or main_rst;
-- @@@ Main reset logic:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- *** Juno

   U_external_trigger : external_trigger
      port map (clk                   => clk_2232,
                main_rst              => external_trigger_main_rst,
                trig_ext              => trig_ext,
                params                => p2_params(13 downto 10),
                EnableExternalTrigger => p2_params(9),
                enable                => p2_params(8),
                io_fpga1              => px3);

   external_trigger_main_rst <= global_reset or not(p2_enable) or not(StartSys_tmp(0));
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*USB%20Interface][USB Interface:1]]
-- ** USB Interface
-- @@@ USB Interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Generics][Generics:1]]
-- *** Generics

   p2_empty_tmp <= p2_empty or p6_enable;

   U_usb_if : usb_if
      generic map (RegBankLength   => RegBankLength,
                   nb_asics        => nb_asics,
                   -- data
                   p1_data_width   => p1_data_width,
                   p2_data_width   => p2_data_width,
                   p3_data_width   => p3_data_width,
                   p4_data_width   => p4_data_width,
                   p5_data_width   => p5_data_width,
                   p6_data_width   => p6_data_width,
                   -- params
                   p1_params_width => p1_params_width,
                   p2_params_width => p2_params_width,
                   p3_params_width => p3_params_width,
                   p4_params_width => p4_params_width,
                   p5_params_width => p5_params_width,
                   p6_params_width => p6_params_width)
-- @@@ Generics:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Generic%20signals][Generic signals:1]]
-- *** Generic signals

      port map (main_rst      => main_rst,
                interrupt_out => interrupt,
                BoardID       => BoardID,
                disable_40MHz => disable_40MHz,
-- @@@ Generic signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*FTDI%20interface][FTDI interface:1]]
-- *** FTDI interface

                usb_rxf  => usb_rxf,
                usb_txe  => usb_txe,
                clk_2232 => clk_2232,
                usb_oe   => usb_oe,
                usb_rd   => usb_rd,
                usb_wr   => usb_wr,
                usb_data => usb_data,
-- @@@ FTDI interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*xilinx][xilinx:1]]
-- ***** xilinx

                usb_siwua => open,
-- @@@ xilinx:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P1:%20simple%20counter][P1: simple counter:1]]
-- *** P1: simple counter

                p1_datain => p1_datain,
                p1_params => p1_params,
                p1_rd     => p1_rd,
                p1_empty  => p1_empty,
                p1_enable => p1_enable,
-- @@@ P1: simple counter:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2:%20asic%20data][P2: asic data:1]]
-- *** P2: asic data

                p2_datain => p2_datain,
                p2_params => p2_params,
                p2_rd     => p2_rd,
-- @@@ P2: asic data:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2:%20asic%20data][P2: asic data:2]]
-- *** P2: asic data

                p2_empty => p2_empty_tmp,
-- @@@ P2: asic data:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2:%20asic%20data][P2: asic data:3]]
-- *** P2: asic data

                p2_enable => p2_enable,
-- @@@ P2: asic data:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P3:%20asic%20slow%20control][P3: asic slow control:1]]
-- *** P3: asic slow control

                p3_datain => p3_datain,
                p3_params => p3_params,
                p3_rd     => p3_rd,
                p3_empty  => p3_empty,
                p3_enable => p3_enable,
-- @@@ P3: asic slow control:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P4:%20adc%20data][P4: adc data:1]]
-- *** P4: adc data

                p4_datain => (others => '1'),
                p4_params => open,
                p4_rd     => open,
                p4_empty  => '1',
                p4_enable => open,
-- @@@ P4: adc data:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P5:%20adc%20slow%20control][P5: adc slow control:1]]
-- *** P5: adc slow control

                p5_datain => (others => '1'),
                p5_params => open,
                p5_rd     => open,
                p5_empty  => '1',
                p5_enable => open,
-- @@@ P5: adc slow control:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- **** Juno

                px1_sel => px1_sel_tmp,
                px2_sel => px2_sel_tmp,
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P6:%20s-curve%20computing][P6: s-curve computing:1]]
-- *** P6: s-curve computing

                p6_datain => p6_datain,
                p6_params => p6_params,
                p6_rd     => p6_rd,
                p6_empty  => p6_empty,
                p6_enable => p6_enable);
-- @@@ P6: s-curve computing:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P1:%20Counter%20generator][P1: Counter generator:1]]
-- ** P1: Counter generator

   U_P1 : p1_counter
      port map (clk_fpga     => clk_2232,
                main_rst     => global_reset,
                -- usb
                clk_2232     => clk_2232,
                usb_data_in  => p1_params,
                usb_data_out => p1_datain,
                usb_fifo1_rd => p1_rd,
                usb_wrreq    => p1_enable,
                fifo_empty   => p1_empty);
-- @@@ P1: Counter generator:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2%20&%20P3%20-%20CatiROC%20manager][P2 & P3 - CatiROC manager:1]]
-- ** P2 & P3 - CatiROC manager
-- @@@ P2 & P3 - CatiROC manager:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Diff%20to%20single%20ended][Diff to single ended:1]]
-- *** Diff to single ended

   IBUFDS_loop_1 :
   for i in 0 to 2*nb_asics-1 generate
      U_IBUFDS_1 : IBUFDS
         port map (O  => cati_dataout(i),
                   I  => cati_dataout_p(i),
                   IB => cati_dataout_n(i));
   end generate;
-- @@@ Diff to single ended:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Data%20capture][Data capture:1]]
-- *** Data capture

   U_dataout_sample : process(clk_80MHz_delayed)
   begin
      if clk_80MHz_delayed'event and clk_80MHz_delayed = '1' then
         cati_dataout_sampled <= cati_dataout;
         cati_strobe_sampled  <= cati_strobe;
      end if;
   end process;
-- @@@ Data capture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Data%20capture][Data capture:2]]
-- *** Data capture

   U_dataout_in : process(clk_80MHz)
   begin
      if clk_80MHz'event and clk_80MHz = '1' then
         cati_dataout_in <= cati_dataout_sampled;
         cati_strobe_in  <= cati_strobe_sampled;
      end if;
   end process;
-- @@@ Data capture:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Catiroc%20manager][Catiroc manager:1]]
-- *** Catiroc manager

   not_cd_busy <= not(cd_busy);
-- @@@ Catiroc manager:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Catiroc%20manager][Catiroc manager:2]]
-- *** Catiroc manager

   U_catiroc : for i in 0 to nb_asics-1 generate
      U0_catiroc : catiroc
         generic map (num_of_discri_channels => num_of_discri_channels,
                      p2_data_width          => p2_data_width)
         port map (main_rst          => global_reset,
                   -- input data stream / strobe
                   cati_dataout      => cati_dataout_in(2*(i+1)-1 downto 2*i),
                   cati_strobe       => cati_strobe_in(2*(i+1)-1 downto 2*i),
                   -- Static values
                   triggers          => triggers(16*(i+1)-1 downto 16*i),
                   ovf               => ovf,
                   --
                   -- CatiROC
                   --
                   NewEvent          => open,  -- all events combined
                   NewEventByChannel => NewEventByChannel(i),
                   resetb            => resetb_tmp(i),
                   StartSys          => StartSys_tmp(i),
                   pll_locked        => locked_sig,
                   discri_falling    => discri_falling(16*(i+1)-1 downto 16*i),
-- @@@ Catiroc manager:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Read%20clock][Read clock:1]]
-- **** Read clock
-- @@@ Read clock:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- ***** Juno

                   clk_2232 => clk_80MHz,
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Clocks][Clocks:1]]
-- **** Clocks

                   clk_80MHz => clk_80MHz,
                   clk_40MHz => clk_40MHz,
-- @@@ Clocks:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- ***** Juno

                   clk_480MHz => clk_480MHz,
                   clk_120MHz => clk_120MHz,
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P2%20-%20Data%20capture%20interface][P2 - Data capture interface:1]]
-- **** P2 - Data capture interface

                   p2_params => p2_params(p2_params_width*8*i+7 downto p2_params_width*8*i),
                   p2_datain => p2_data_fifo(p2_data_width*8*(i+1)-1 downto p2_data_width*8*i),
                   p2_empty  => p2_empty_fifo(i),
                   p2_read   => p2_read_fifo(i),
-- @@@ P2 - Data capture interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- ***** Juno

                   p2_enable => p2_enable,
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P3%20-%20Slow%20control%20interface][P3 - Slow control interface:1]]
-- **** P3 - Slow control interface

                   clk_10MHz => clk_10MHz,
                   sr_clk    => sr_clk_tmp(i),
                   sr_in     => sr_in(i),
                   sr_rst    => sr_rst_tmp(i),
                   sr_select => sr_select_tmp(i),
                   sr_out    => sr_out(i),
                   p3_empty  => p3_empty_fifo(i),
                   p3_read   => p3_read_fifo(i),
                   p3_datain => p3_data_fifo(p3_data_width*8*(i+1)-1 downto p3_data_width*8*i),
                   p3_enable => p3_enable,
                   p3_params => p3_params(p3_params'left-i*528 downto p3_params'left-i*528-528+1));
   end generate U_catiroc;
-- @@@ P3 - Slow control interface:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P3%20-%20Slow%20control%20interface][P3 - Slow control interface:2]]
-- **** P3 - Slow control interface

   resetb    <= resetb_tmp(0);
   process (clk_40MHz)
   begin
      if (clk_40MHz'event and clk_40MHz = '1') then
         StartSys <= StartSys_tmp(0);
      end if;
   end process;
   sr_clk    <= sr_clk_tmp(0);
   sr_rst    <= sr_rst_tmp(0);
   sr_select <= sr_select_tmp(0);
-- @@@ P3 - Slow control interface:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Catiroc-usb%20If%20:%20data%20capture][Catiroc-usb If : data capture:1]]
-- ** Catiroc-usb If : data capture
-- @@@ Catiroc-usb If : data capture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- *** Juno

   U_catiroc_usb_if_dc : catiroc_usb_if
      generic map (nb_asics             => nb_asics,
                   input_bus_width_bits => p2_data_width*8,
                   extend               => 1)
      port map (main_rst        => global_reset,
                EnabledChannels => p2_params(31 downto 24),
                -- incoming data from nb_asics ASICS
                asic_clk        => clk_80MHz,
                asic_empty      => p2_empty_fifo,  -- nb_asics
                asic_rd         => p2_read_fifo,   -- nb_asics
                asic_data       => p2_data_fifo,   -- nb_asics
                -- if to usb managager
                fifo_clk        => clk_2232,
                fifo_dataout    => p2_datain,
                fifo_rd         => p2_rd,
                fifo_empty      => p2_empty);
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Catiroc-usb%20If%20:%20slow%20control][Catiroc-usb If : slow control:1]]
-- ** Catiroc-usb If : slow control
-- @@@ Catiroc-usb If : slow control:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- *** Juno

   U_catiroc_usb_if_sc : catiroc_usb_if
      generic map (nb_asics             => nb_asics,
                   input_bus_width_bits => p3_data_width*8,
                   extend               => 1)
      port map (main_rst        => global_reset,
                EnabledChannels => X"FF",
                -- incoming data from nb_asics ASICS
                asic_clk        => clk_80MHz,
                asic_empty      => p3_empty_fifo,  -- nb_asics
                asic_rd         => p3_read_fifo,   -- nb_asics
                asic_data       => p3_data_fifo,   -- nb_asics
                -- if to usb managager
                fifo_clk        => clk_2232,
                fifo_dataout    => p3_datain,
                fifo_rd         => p3_rd,
                fifo_empty      => p3_empty);
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*P6:%20S%20Curve][P6: S Curve:1]]
-- ** P6: S Curve

   U_s_curve : for i in 0 to nb_asics-1 generate
      s_curve_1 : s_curve
         generic map (CounterLength => 20,
                      nb_channels   => 16,
                      asic_nb       => i)
         port map (clk               => clk_80Mhz,
                   main_rst          => global_reset,
                   triggers          => discri_falling(16*(i+1)-1 downto 16*i),
                   -- triggers(16*(i+1)-1 downto 16*i),
                   NewEventByChannel => NewEventByChannel(i),
                   -- usb if
                   clk_2232          => clk_2232,
                   p_enable          => p6_enable,
                   p_params          => p6_params,
                   p_datain          => p6_data_fifo(128*(i+1)-1 downto 128*i),
                   p_rd              => p6_read_fifo(i),
                   p_empty           => p6_empty_fifo(i));
   end generate U_s_curve;
-- @@@ P6: S Curve:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- *** Juno

   U_scurve_usb_if : catiroc_usb_if
      generic map (nb_asics             => nb_asics,
                   input_bus_width_bits => 128,
                   extend               => 0)
      port map (main_rst        => global_reset,
                EnabledChannels => X"FF",
                -- incoming data
                asic_clk        => clk_2232,
                asic_empty      => p6_empty_fifo,  -- nb_asics
                asic_rd         => p6_read_fifo,   -- nb_asics
                asic_data       => p6_data_fifo,   -- nb_asics
                -- if to usb managager
                fifo_clk        => clk_2232,
                fifo_dataout    => p6_datain,
                fifo_rd         => p6_rd,
                fifo_empty      => p6_empty);
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Control%20delay][Control delay:1]]
-- ** Control delay

   process (clk_40MHz)
   begin
      if (clk_40MHz'event and clk_40MHz = '0') then
         cd_counter_ref <= unsigned(p2_params(23 downto 8));
         if (global_reset = '1' or p2_enable = '0') then
            phasestep  <= '0';
            cd_counter <= (others => '0');
            cd_busy    <= '1';
            cd_state   <= cd_1;
         else
            case cd_state is
               when cd_1 =>
                  cd_busy <= '1';
                  if p2_enable = '1' then
                     phasestep <= '1';
                     cd_state  <= cd_2;
                  else
                     phasestep <= '0';
                     cd_state  <= cd_1;
                  end if;
               when cd_2 =>
                  cd_busy   <= '1';
                  phasestep <= '1';
                  if phasedone = '0' then
                     cd_state <= cd_3;
                  else
                     cd_state <= cd_2;
                  end if;
               when cd_3 =>
                  cd_busy <= '1';
                  if phasedone = '1' then
                     phasestep <= '0';
                     if cd_counter >= cd_counter_ref then
                        cd_state <= cd_end;
                     else
                        cd_state <= cd_1;
                     end if;
                     cd_counter <= cd_counter + 1;
                  else
                     phasestep <= '1';
                     cd_state  <= cd_3;
                  end if;
               when cd_end =>
                  cd_busy  <= '0';
                  cd_state <= cd_end;
            end case;
         end if;
      end if;
   end process;
-- @@@ Control delay:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Main%20pll][Main pll:1]]
-- *** Main pll
-- @@@ Main pll:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- **** Juno

   U_main_pll_40 : if freq_source = 40 generate
      U_main_pll : pll
         port map (CLK_IN1  => clk_fpga,
                   -- Clock out ports
                   CLK_OUT1 => clk_40MHz,
                   CLK_OUT2 => clk_160MHz,
                   CLK_OUT3 => clk_10MHz,
                   CLK_OUT4 => clk_80MHz,
                   CLK_OUT5 => clk_480MHz,
                   CLK_OUT6 => clk_120MHz,
                   CLK_OUT7 => clk_80MHz_delayed,
                   -- Status and control signals
                   RESET    => '0',     -- async reset
                   LOCKED   => locked_sig);
   end generate;
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:2]]
-- **** Juno

   U_main_pll_42 : if freq_source = 42 generate
      U_pll_gcu : pll_gcu
         port map (clk_in1  => clk_fpga,
                   clk_out1 => clk_gcu,
                   reset    => '0',
                   locked   => open);

      U_main_pll : pll
         port map (CLK_IN1  => clk_gcu,
                   -- Clock out ports
                   CLK_OUT1 => clk_40MHz,
                   CLK_OUT2 => clk_160MHz,
                   CLK_OUT3 => clk_10MHz,
                   CLK_OUT4 => clk_80MHz,
                   CLK_OUT5 => clk_480MHz,
                   CLK_OUT6 => clk_120MHz,
                   CLK_OUT7 => clk_80MHz_delayed,
                   -- Status and control signals
                   RESET    => '0',     -- async reset
                   LOCKED   => locked_sig);
   end generate;
-- @@@ Juno:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Replicate%2040%20MHz%20clock%20out%20of%20the%20fpga][Replicate 40 MHz clock out of the fpga:1]]
-- *** Replicate 40 MHz clock out of the fpga
-- @@@ Replicate 40 MHz clock out of the fpga:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- **** Juno

   U_oddr_1 : ODDR
      generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE",
                  INIT         => '0',
                  SRTYPE       => "SYNC")
      port map (Q  => clk_40MHz_out,
                C  => clk_40MHz,
                CE => '1',
                D1 => '1',
                D2 => '0',
                R  => disable_40MHz,
                S  => '0');
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:2]]
-- **** Juno

   U_OBUFDS_2 : OBUFDS
      port map (I  => clk_40MHz_out,
                O  => clk_40MHz_out_p,
                OB => clk_40MHz_out_n);
-- @@@ Juno:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Replicate%20160%20MHz%20clock%20out%20of%20the%20fpga][Replicate 160 MHz clock out of the fpga:1]]
-- *** Replicate 160 MHz clock out of the fpga
-- @@@ Replicate 160 MHz clock out of the fpga:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:1]]
-- **** Juno

   U_oddr_2 : ODDR
      generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE",
                  INIT         => '0',
                  SRTYPE       => "SYNC")
      port map (Q  => clk_160MHz_out,
                C  => clk_160MHz,
                CE => '1',
                D1 => '1',
                D2 => '0',
                R  => '0',
                S  => '0');
-- @@@ Juno:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Juno][Juno:2]]
-- **** Juno

   U_OBUFDS_3 : OBUFDS
      port map (I  => clk_160MHz_out,
                O  => clk_160MHz_out_p,
                OB => clk_160MHz_out_n);
-- @@@ Juno:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*Replicate%2010%20MHz%20out%20of%20the%20fpga][Replicate 10 MHz out of the fpga:1]]
-- *** Replicate 10 MHz out of the fpga
-- @@@ Replicate 10 MHz out of the fpga:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*FMC][FMC:1]]
-- ** FMC

-- IBUFDS_loop_2 :
--    for i in 0 to 9 generate
--       U_IBUFDS_2 : IBUFDS
--          port map (O  => gcu_fmc_in(i),
--                    I  => gcu_fmc_in_p(i),
--                    IB => gcu_fmc_in_n(i));
--    end generate;

--    OBUFDS_loop_1 :
--       for i in 0 to 7 generate
--          gcu_fmc_out(i) <= gcu_fmc_in(i);
--          U_OBUFDS_1 : OBUFDS
--             port map (I  => gcu_fmc_out(i),
--                       O  => gcu_fmc_out_p(i),
--                       OB => gcu_fmc_out_n(i));
--       end generate;
-- @@@ FMC:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/top.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
