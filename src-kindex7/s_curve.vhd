-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

library UNISIM;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

use work.juno_mezzanine_package.all;
use UNISIM.VComponents.all;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Entity][Entity:1]]
-- * Entity

entity s_curve is
   generic (CounterLength : natural range 1 to 30 := 16;
            nb_channels   : natural range 1 to 16 := 1;
            asic_nb       : natural range 0 to 7  := 1);
   port (clk               : in  std_logic;  -- generic oscilator present on board
         main_rst          : in  std_logic;
         NewEventByChannel : in  std_logic_vector(nb_channels-1 downto 0);
         triggers          : in  std_logic_vector(nb_channels-1 downto 0);
         -- usb
         clk_2232          : in  std_logic;
         p_params          : in  std_logic_vector(31 downto 0);
         p_datain          : out std_logic_vector(127 downto 0);
         p_rd              : in  std_logic;
         p_enable          : in  std_logic;
         p_empty           : out std_logic);
end entity s_curve;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of s_curve is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Signals][Signals:1]]
-- ** Signals
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Generic][Generic:1]]
-- *** Generic

   signal counter             : unsigned(CounterLength-1 downto 0)       := (others => '0');
   signal counter_integer     : integer                                  := 0;
   signal fifo_wr, fifo_clear : std_logic                                := '0';
   signal rst                 : std_logic                                := '1';
   signal asic_number         : std_logic_vector(2 downto 0)             := (others => '0');
-- signal triggers_enabled         : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_enabled_tmp     : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_enabled_tmp_del : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_latched         : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_latched_2       : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- signal triggers_top_rising      : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
   signal NewEventOrTrigger   : std_logic_vector(nb_channels-1 downto 0) := (others => '0');
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Array%20of%20event%20counts][Array of event counts:1]]
-- *** Array of event counts

   type fifo_data_array_type is array(0 to nb_channels-1) of unsigned(24 downto 0);
   signal fifo_data_array : fifo_data_array_type          := (others => '0' & X"000000");
   signal fifo_data       : std_logic_vector(31 downto 0) := (others => '0');
-- @@@ Array of event counts:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Generic][Generic:1]]
-- ** Generic

   rst <= '1' when (main_rst = '1' or p_enable = '0') else '0';
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Mux][Mux:1]]
-- ** Mux

   process (clk)
   begin
      if (clk = '1' and clk'event) then
         if p_params(16) = '0' then
            NewEventOrTrigger <= triggers;
         else
            NewEventOrTrigger <= NewEventByChannel(12) & NewEventByChannel(13) & NewEventByChannel(15) & NewEventByChannel(14) &
                                 NewEventByChannel(10) & NewEventByChannel(11) & NewEventByChannel(9) & NewEventByChannel(8) &
                                 NewEventByChannel(4) & NewEventByChannel(5) & NewEventByChannel(7) & NewEventByChannel(6) &
                                 NewEventByChannel(2) & NewEventByChannel(3) & NewEventByChannel(1 downto 0);
         end if;
      end if;
   end process;
-- @@@ Mux:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Count%20trigger%20/%20data%20events][Count trigger / data events:1]]
-- ** Count trigger / data events

   process (clk)
   begin
      if (clk = '1' and clk'event) then
         if rst = '1' then
            counter <= (others => '0');
         else
            counter <= counter + 1;
         end if;
      end if;
   end process;
-- @@@ Count trigger / data events:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Count%20trigger%20/%20data%20events][Count trigger / data events:2]]
-- ** Count trigger / data events

   U_count : for i in 0 to nb_channels-1 generate
      process (clk)
      begin
         if (clk = '1' and clk'event) then
            if (rst = '1' or counter = (i+1)) then
               fifo_data_array(i) <= (others => '0');
            else
               if NewEventOrTrigger(i) = '1' then
                  fifo_data_array(i) <= fifo_data_array(i) + 1;
               end if;
            end if;
         end if;
      end process;
   end generate U_count;
-- @@@ Count trigger / data events:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Count%20trigger%20/%20data%20events][Count trigger / data events:3]]
-- ** Count trigger / data events

   asic_number <= std_logic_vector(to_unsigned(asic_nb, 3));

   counter_integer <= to_integer(counter);

   process (clk)
   begin
      if (clk = '1' and clk'event) then
         if rst = '1' then
            fifo_data <= (others => '0');
            fifo_wr   <= '0';
         else
            case counter_integer is
               when 0 =>
                  fifo_data <= asic_number & X"0" & std_logic_vector(fifo_data_array(0));
                  fifo_wr   <= '1';
               when 1 =>
                  fifo_data <= asic_number & X"1" & std_logic_vector(fifo_data_array(1));
                  fifo_wr   <= '1';
               when 2 =>
                  fifo_data <= asic_number & X"2" & std_logic_vector(fifo_data_array(2));
                  fifo_wr   <= '1';
               when 3 =>
                  fifo_data <= asic_number & X"3" & std_logic_vector(fifo_data_array(3));
                  fifo_wr   <= '1';
               when 4 =>
                  fifo_data <= asic_number & X"4" & std_logic_vector(fifo_data_array(4));
                  fifo_wr   <= '1';
               when 5 =>
                  fifo_data <= asic_number & X"5" & std_logic_vector(fifo_data_array(5));
                  fifo_wr   <= '1';
               when 6 =>
                  fifo_data <= asic_number & X"6" & std_logic_vector(fifo_data_array(6));
                  fifo_wr   <= '1';
               when 7 =>
                  fifo_data <= asic_number & X"7" & std_logic_vector(fifo_data_array(7));
                  fifo_wr   <= '1';
               when 8 =>
                  fifo_data <= asic_number & X"8" & std_logic_vector(fifo_data_array(8));
                  fifo_wr   <= '1';
               when 9 =>
                  fifo_data <= asic_number & X"9" & std_logic_vector(fifo_data_array(9));
                  fifo_wr   <= '1';
               when 10 =>
                  fifo_data <= asic_number & X"A" & std_logic_vector(fifo_data_array(10));
                  fifo_wr   <= '1';
               when 11 =>
                  fifo_data <= asic_number & X"B" & std_logic_vector(fifo_data_array(11));
                  fifo_wr   <= '1';
               when 12 =>
                  fifo_data <= asic_number & X"C" & std_logic_vector(fifo_data_array(12));
                  fifo_wr   <= '1';
               when 13 =>
                  fifo_data <= asic_number & X"D" & std_logic_vector(fifo_data_array(13));
                  fifo_wr   <= '1';
               when 14 =>
                  fifo_data <= asic_number & X"E" & std_logic_vector(fifo_data_array(14));
                  fifo_wr   <= '1';
               when 15 =>
                  fifo_data <= asic_number & X"F" & std_logic_vector(fifo_data_array(15));
                  fifo_wr   <= '1';
               when others =>
                  fifo_data <= (others => '0');
                  fifo_wr   <= '0';
            end case;
         end if;
      end if;
   end process;
-- @@@ Count trigger / data events:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Fifo][Fifo:1]]
-- ** Fifo
-- @@@ Fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*Xilinx%20fifo][Xilinx fifo:1]]
-- *** Xilinx fifo

   Cati_output_fifo1_inst : Cati_output_fifo1
      port map (rst    => rst,
                wr_clk => clk,
                rd_clk => clk_2232,
                din    => fifo_data,
                wr_en  => fifo_wr,
                rd_en  => p_rd,
                dout   => p_datain,
                full   => open,
                empty  => p_empty);
-- @@@ Xilinx fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/s_curve.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
