-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/inspection.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/inspection.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/inspection.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/inspection.org::*Entity][Entity:1]]
-- * Entity

entity inspection is
   port (input_i : in  inspec;
         sel     : in  std_logic_vector (15 downto 0);
         output  : out std_logic);
end inspection;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/inspection.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of inspection is

   signal selec_asic   : std_logic_vector (7 downto 0) := (others => '0');
   signal selec_signal : std_logic_vector (7 downto 0) := (others => '0');
   signal inp_tmp      : std_logic_vector(23 downto 0);
   signal sig_tmp      : std_logic;
begin

   selec_asic   <= sel (15 downto 8);
   selec_signal <= sel (7 downto 0);

--inp_tmp <= input_i(to_integer(signed(selec_asic)));
   with selec_asic select inp_tmp <=
      input_i(0)      when X"00",
      input_i(1)      when X"01",
      input_i(2)      when X"02",
      input_i(3)      when X"03",
      input_i(4)      when X"04",
      input_i(5)      when X"05",
      input_i(6)      when X"06",
      input_i(7)      when X"07",
      --inp_tmp when others;
      (others => '0') when others;
--sig_tmp <=   inp_tmp(to_integer(signed(selec_signal)));
   with selec_signal select sig_tmp <=
      inp_tmp(0)  when X"00",
      inp_tmp(1)  when X"01",
      inp_tmp(2)  when X"02",
      inp_tmp(3)  when X"03",
      inp_tmp(4)  when X"04",
      inp_tmp(5)  when X"05",
      inp_tmp(6)  when X"06",
      inp_tmp(7)  when X"07",
      inp_tmp(8)  when X"08",
      inp_tmp(9)  when X"09",
      inp_tmp(10) when X"0A",
      inp_tmp(11) when X"0B",
      inp_tmp(12) when X"0C",
      inp_tmp(13) when X"0D",
      inp_tmp(14) when X"0E",
      inp_tmp(15) when X"0F",
      inp_tmp(16) when X"10",
--    inp_tmp(17) when X"11",
--    inp_tmp(18) when X"12",
--    inp_tmp(19) when X"13",
--    inp_tmp(20) when X"14",
--    inp_tmp(21) when X"15",
--    inp_tmp(22) when X"16",
--    inp_tmp(23) when X"17",
      --sig_tmp when others;
      '0'         when others;

   output <= sig_tmp;
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/inspection.org::*end][end:1]]
-- ** end

end Behavioral;
-- @@@ end:1 ends here
