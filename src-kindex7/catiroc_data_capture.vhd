-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

library UNISIM;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

use work.juno_mezzanine_package.all;
use UNISIM.VComponents.all;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Entity][Entity:1]]
-- * Entity

entity catiroc_data_capture is
   generic (num_of_discri_channels : natural range 1 to 16;
            p2_data_width          : natural range 1 to 16);
   port(Clk_usb           : in  std_logic;
        ovf               : in  std_logic;
        triggers          : in  std_logic_vector (15 downto 0);
        test              : out std_logic_vector(10 downto 0);
        NewEvent          : out std_logic;
        NewEventByChannel : out std_logic_vector(15 downto 0);
        clk_80Mhz         : in  std_logic;
        clk_40Mhz         : in  std_logic;
        clk_480MHz        : in  std_logic;
        clk_120MHz        : in  std_logic;
        pll_locked        : in  std_logic;
        discri_falling    : out std_logic_vector(15 downto 0);
        main_rst          : in  std_logic;
        resetb            : out std_logic;
        StartSys          : out std_logic;
        usb_params2       : in  std_logic_vector (7 downto 0);
        fifo_dataout      : out std_logic_vector(p2_data_width*8-1 downto 0);
        fifo_rd           : in  std_logic;
        fifo_empty        : out std_logic;
        p2_enable         : in  std_logic;
        cati_dataout      : in  std_logic_vector (1 downto 0);
        cati_strobe       : in  std_logic_vector (1 downto 0));
end entity catiroc_data_capture;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of catiroc_data_capture is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:1]]
-- * Signals

   type dataout_fifo_state_typee is (standby, write_tempo, write_tempo2);
-- component generics
   constant nb_channels                          : natural range 1 to 8                                  := 3;
   constant active_channels                      : std_logic_vector(7 downto 0)                          := "00000001";
   constant Seuil_fifos_empty                    : integer                                               := 100;
-- component ports
   signal dataout, strobe                        : std_logic                                             := '0';
   signal dataout2, strobe2                      : std_logic                                             := '0';
   signal init_standby_period                    : unsigned(7 downto 0)                                  := (others => '0');
-- internals signals
   signal main_fifo_datain, main_fifo_datain_tmp : std_logic_vector(63 downto 0)                         := (others => '0');
   signal main_fifo_wr                           : std_logic                                             := '0';
   signal main_fifo_full, rst_counter            : std_logic                                             := '0';
   signal fifo_output1_empty, resetb_inner       : std_logic                                             := '0';
   signal fifo_output1_rd                        : std_logic                                             := '0';
   signal fifo_output1_data                      : std_logic_vector(63 downto 0)                         := (others => '0');
   signal fifo_output2_empty                     : std_logic                                             := '0';
   signal fifo_output2_rd                        : std_logic                                             := '0';
   signal fifo_output2_data                      : std_logic_vector(63 downto 0)                         := (others => '0');
   signal fifo_output3_empty                     : std_logic                                             := '0';
   signal fifo_output3_rd                        : std_logic                                             := '0';
   signal rst_discri_data, rst_physical_data     : std_logic                                             := '1';
   signal rst_special_data                       : std_logic                                             := '1';
   signal fifo_output3_data                      : std_logic_vector(63 downto 0)                         := (others => '0');
   signal cati_standby_period                    : unsigned(7 downto 0)                                  := (others => '0');
   signal counter                                : unsigned(24 downto 0)                                 := (others => '0');
   signal ChannelNb                              : unsigned(4 downto 0)                                  := (others => '0');
   signal reset_cati                             : std_logic                                             := '0';
   signal dataout_fifo_state                     : dataout_fifo_state_typee;
   signal rst, NewEvent1, NewEvent2              : std_logic                                             := '0';
   signal priority_flag                          : unsigned(1 downto 0)                                  := "00";
   signal cati_reset_delayed                     : std_logic_vector(31 downto 0)                         := (others => '0');
   signal tdc_data_f, tdc_data_r                 : std_logic_vector(6*num_of_discri_channels-1 downto 0) := (others => '0');
   signal tstamp                                 : std_logic_vector(31 downto 0)                         := (others => '0');
   signal tstamp_u                               : unsigned(31 downto 0)                                 := (others => '0');
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:2]]
-- * Signals

   alias ReduceDiscriminatorData : std_logic is usb_params2(5);
-- @@@ Signals:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:3]]
-- * Signals

   alias EnableSpecialData : std_logic is usb_params2(6);
-- @@@ Signals:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:4]]
-- * Signals

   alias EnablePhysicalData : std_logic is usb_params2(4);
-- @@@ Signals:4 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:5]]
-- * Signals

   alias EnableDiscriminatorData : std_logic is usb_params2(3);
-- @@@ Signals:5 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:6]]
-- * Signals

   alias DecodeGray     : std_logic is usb_params2(2);
   alias nbBitselect    : std_logic is usb_params2(1);
   alias EmulateCatiROC : std_logic is usb_params2(0);
-- @@@ Signals:6 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Signals][Signals:7]]
-- * Signals

   signal NewEventByChannel_0_to_7  : std_logic_vector(15 downto 0) := (others => '0');
   signal NewEventByChannel_8_to_15 : std_logic_vector(15 downto 0) := (others => '0');
-- @@@ Signals:7 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20rate%20measurement%20signals][Data rate measurement signals:1]]
-- ** Data rate measurement signals

   signal TriggerCounters_10ms                                     : TriggerCounters               := (others => X"0000");
   signal TriggerCounters_up                                       : TriggerCounters               := (others => X"0000");
   signal triggers_latched, triggers_latched_2, triggers_latched_3 : std_logic_vector(15 downto 0) := (others => '0');
   signal triggers_enabled_tmp                                     : std_logic_vector(15 downto 0) := (others => '0');
   signal triggers_enabled_tmp_del                                 : std_logic_vector(15 downto 0) := (others => '0');
   signal triggers_top_rising, triggers_top_falling                : std_logic_vector(15 downto 0) := (others => '0');
   signal Counter10msTop                                           : std_logic                     := '0';
   signal Counter10ms                                              : unsigned(19 downto 0)         := (others => '0');
-- @@@ Data rate measurement signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*begin][begin:1]]
-- * begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Reset][Reset:1]]
-- * Reset

   rest_cati : process (clk_80Mhz)
   begin
      if clk_80Mhz'event and clk_80Mhz = '1' then
         if main_rst = '1' or p2_enable = '0' then  --or reset_cati = '1' then
            rst                 <= '1';
            resetb_inner        <= '0';
            StartSys            <= '0';
            cati_standby_period <= (others => '0');
         else
            if cati_standby_period <= X"40" then
               cati_standby_period <= cati_standby_period + 1;
               rst                 <= '1';
               StartSys            <= '0';
               resetb_inner        <= '0';
            elsif cati_standby_period <= X"A0" then
               cati_standby_period <= cati_standby_period + 1;
               rst                 <= '0';
               StartSys            <= '0';
               resetb_inner        <= '0';
            elsif cati_standby_period < X"FF" then
               cati_standby_period <= cati_standby_period + 1;
               rst                 <= '0';
               StartSys            <= '0';
               resetb_inner        <= '1';
            else
               rst          <= '0';
               StartSys     <= '1';
               resetb_inner <= '1';
            end if;
         end if;
      end if;
   end process rest_cati;

   resetb <= resetb_inner;
-- @@@ Reset:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Capture%20data%20from%20ASIC][Capture data from ASIC:1]]
-- * Capture data from ASIC
-- @@@ Capture data from ASIC:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Source%201:%20Channels%200%20to%207][Source 1: Channels 0 to 7:1]]
-- ** Source 1: Channels 0 to 7

   rst_physical_data <= '1' when (rst = '1' or EnablePhysicalData = '0') else '0';
-- @@@ Source 1: Channels 0 to 7:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Pattern%20generator][Pattern generator:1]]
-- *** Pattern generator

   cati_dataout_pattern_1 : cati_data_out
      generic map (nb_channels     => 1,
                   active_channels => active_channels)
      port map (clk_in       => clk_80Mhz,
                rst          => rst_physical_data,
                emulated     => EmulateCatiROC,
                cati_dataout => cati_dataout(0),
                cati_strobe  => cati_strobe(0),
                clk_out      => open,
                dataout      => dataout,
                strobe       => strobe);
-- @@@ Pattern generator:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20capture][Data capture:1]]
-- *** Data capture

   Handle_cati_data_1 : handle_cati_data_out
      generic map (output_nb     => '0',
                   lenght_strobe => 40)
      port map (rst               => rst_physical_data,
                test              => open,
                NewEvent          => NewEvent1,
                NewEventByChannel => NewEventByChannel_0_to_7,
                --cati
                clk_80Mhz         => clk_80Mhz,
                cati_dataout      => dataout,
                cati_strobe       => strobe,
                nbBitselect       => nbBitselect,
                decode_gray       => DecodeGray,
                --peripheral
                fifo_dataout      => fifo_output1_data,
                fifo_rd           => fifo_output1_rd,
                fifo_empty        => fifo_output1_empty);
-- @@@ Data capture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Source%202:%20Channels%208%20to%2015][Source 2: Channels 8 to 15:1]]
-- ** Source 2: Channels 8 to 15
-- @@@ Source 2: Channels 8 to 15:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Pattern%20generator][Pattern generator:1]]
-- *** Pattern generator

   cati_dataout_pattern_2 : cati_data_out
      generic map (nb_channels     => 1,
                   active_channels => active_channels)
      port map (clk_in       => clk_80Mhz,
                rst          => rst_physical_data,
                emulated     => EmulateCatiROC,
                cati_dataout => cati_dataout(1),
                cati_strobe  => cati_strobe(1),
                clk_out      => open,
                dataout      => dataout2,
                strobe       => strobe2);
-- @@@ Pattern generator:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20capture][Data capture:1]]
-- *** Data capture

   Handle_cati_data_2 : handle_cati_data_out
      generic map (output_nb     => '1',
                   lenght_strobe => 40)
      port map (rst               => rst_physical_data,
                test              => test,
                NewEvent          => NewEvent2,
                NewEventByChannel => NewEventByChannel_8_to_15,
                --cati
                clk_80MHz         => clk_80Mhz,
                cati_dataout      => dataout2,
                cati_strobe       => strobe2,
                nbBitselect       => nbBitselect,
                decode_gray       => DecodeGray,
                --peripheral
                fifo_dataout      => fifo_output2_data,
                fifo_rd           => fifo_output2_rd,
                fifo_empty        => fifo_output2_empty);
-- @@@ Data capture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*New%20event%20oring][New event oring:1]]
-- ** New event oring

   process (clk_80MHz) is
   begin
      if clk_80MHz'event and clk_80MHz = '1' then
         NewEvent <= NewEvent1 or NewEvent2;
      end if;
   end process;
-- @@@ New event oring:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Source%203:%20Discriminator%20data][Source 3: Discriminator data:1]]
-- ** Source 3: Discriminator data
-- @@@ Source 3: Discriminator data:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Trigger%20events%20and%20TDC][Trigger events and TDC:1]]
-- *** Trigger events and TDC
-- @@@ Trigger events and TDC:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Juno%20mezzanine][Juno mezzanine:1]]
-- **** Juno mezzanine

   U0_tdc : tdc
      generic map (TDC_N => num_of_discri_channels)
      port map (data_in_from_pins => triggers,
                ovf               => ovf,
                resetb            => resetb_inner,
                clk_sync          => clk_40MHz,
                clk_s             => clk_120MHz,
                clk_f             => clk_480MHz,
                lock              => pll_locked,
                tstamp            => tstamp,
                main_rst          => main_rst,
                data_out_f        => tdc_data_f,
                data_out_r        => tdc_data_r,
                falling_out       => triggers_top_falling,
                rising_out        => triggers_top_rising);

   discri_falling <= triggers_top_falling;
-- @@@ Juno mezzanine:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20capture][Data capture:1]]
-- *** Data capture

   rst_discri_data <= '1' when (rst = '1' or EnableDiscriminatorData = '0') else '0';

   U_discri_data : discriminator_data
      generic map (num_of_discri_channels => num_of_discri_channels)
      port map (rst                     => rst_discri_data,
                ovf                     => ovf,
                resetb                  => resetb_inner,
                clk_80MHz               => clk_80Mhz,
                clk_40MHz               => clk_80Mhz,
                discri_rising           => triggers_top_rising,
                discri_falling          => triggers_top_falling,
                discri_data_f           => tdc_data_f,
                discri_data_r           => tdc_data_r,
                tstamp                  => tstamp,
                ReduceDiscriminatorData => ReduceDiscriminatorData,
                -- fifo if
                fifo_dataout            => fifo_output3_data,
                fifo_rd                 => fifo_output3_rd,
                fifo_empty              => fifo_output3_empty);
-- @@@ Data capture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Source%204:%20Data%20Rate][Source 4: Data Rate:1]]
-- ** Source 4: Data Rate
-- @@@ Source 4: Data Rate:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20Rate:%2010%20ms%20counter][Data Rate: 10 ms counter:1]]
-- *** Data Rate: 10 ms counter

   U_10ms : process (clk_80MHz) is
   begin
      if clk_80MHz'event and clk_80MHz = '1' then
         if main_rst = '1' then
            Counter10msTop <= '0';
            Counter10ms    <= (others => '0');
         else
            if Counter10ms < 800000 then
               Counter10msTop <= '0';
               Counter10ms    <= Counter10ms + 1;
            else
               Counter10msTop <= '1';
               Counter10ms    <= (others => '0');
            end if;
         end if;
      end if;
   end process U_10ms;
-- @@@ Data Rate: 10 ms counter:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20Rate:%20counting][Data Rate: counting:1]]
-- *** Data Rate: counting

   U_data_rate_discri : for i in 0 to 15 generate
      U_data_rate_discri_channel : process (clk_80MHz) is
      begin
         if clk_80MHz'event and clk_80MHz = '1' then
            if main_rst = '1' then
               TriggerCounters_up(i)   <= (others => '0');
               TriggerCounters_10ms(i) <= (others => '0');
            else
               if Counter10msTop = '1' then
                  TriggerCounters_up(i)   <= X"0000";
                  TriggerCounters_10ms(i) <= TriggerCounters_up(i);
               elsif triggers_top_rising(i) = '1' then
                  TriggerCounters_up(i) <= TriggerCounters_up(i) + 1;
               end if;
            end if;
         end if;
      end process U_data_rate_discri_channel;
   end generate U_data_rate_discri;
-- @@@ Data Rate: counting:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20Rate:%20counting][Data Rate: counting:2]]
-- *** Data Rate: counting

   U_data_rate_physical : for i in 16 to 31 generate
      TriggerCounters_up(i)   <= (others => '0');
      TriggerCounters_10ms(i) <= (others => '0');
   end generate U_data_rate_physical;
-- @@@ Data Rate: counting:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Data%20Rate:%20disable%20feature][Data Rate: disable feature:1]]
-- *** Data Rate: disable feature

   rst_special_data <= '1' when (rst = '1' or EnableSpecialData = '0') else '0';
-- @@@ Data Rate: disable feature:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*New%20event%20by%20channel][New event by channel:1]]
-- * New event by channel

   NewEventByChannel <= NewEventByChannel_8_to_15(15 downto 8) &
                        NewEventByChannel_0_to_7(7 downto 0);
-- @@@ New event by channel:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Merge%20data%20from%204%20sources][Merge data from 4 sources:1]]
-- * Merge data from 4 sources

   process(clk_80Mhz)

   begin
-- @@@ Merge data from 4 sources:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Reset%20state][Reset state:1]]
-- ** Reset state

      if (clk_80Mhz'event and clk_80Mhz = '1') then

         if rst = '1' then

            fifo_output1_rd    <= '0';
            fifo_output2_rd    <= '0';
            fifo_output3_rd    <= '0';
            main_fifo_datain   <= (others => '0');
            main_fifo_wr       <= '0';
            rst_counter        <= '0';
            dataout_fifo_state <= standby;
            priority_flag      <= "00";
            ChannelNb          <= (others => '0');

         else

            case dataout_fifo_state is
-- @@@ Reset state:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Standby][Standby:1]]
-- ** Standby

               when standby =>

                  ChannelNb        <= (others => '0');
                  main_fifo_wr     <= '0';
                  main_fifo_datain <= (others => '0');
                  rst_counter      <= '0';
                  if priority_flag = "10" then
                     priority_flag <= "00";
                  else
                     priority_flag <= priority_flag + 1;
                  end if;
                  if (priority_flag = "00" and fifo_output1_empty = '0' and main_fifo_full = '0' and EnablePhysicalData = '1') then
                     main_fifo_datain_tmp <= fifo_output1_data;
                     fifo_output1_rd      <= '1';
                     fifo_output2_rd      <= '0';
                     fifo_output3_rd      <= '0';
                     dataout_fifo_state   <= write_tempo;
                  elsif (priority_flag = "01" and fifo_output2_empty = '0' and main_fifo_full = '0' and EnablePhysicalData = '1') then
                     main_fifo_datain_tmp <= fifo_output2_data;
                     fifo_output1_rd      <= '0';
                     fifo_output2_rd      <= '1';
                     fifo_output3_rd      <= '0';
                     dataout_fifo_state   <= write_tempo;
                  elsif (priority_flag = "10" and fifo_output3_empty = '0' and main_fifo_full = '0' and EnableDiscriminatorData = '1') then
                     main_fifo_datain_tmp <= fifo_output3_data;
                     fifo_output1_rd      <= '0';
                     fifo_output2_rd      <= '0';
                     fifo_output3_rd      <= '1';
                     dataout_fifo_state   <= write_tempo;
                  else
                     main_fifo_datain_tmp <= X"00000000000000B0";
                     fifo_output1_rd      <= '0';
                     fifo_output2_rd      <= '0';
                     fifo_output3_rd      <= '0';
                     dataout_fifo_state   <= standby;
                  end if;
-- @@@ Standby:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Sources%201-3:%20Physical%20event%20words%20+%20discriminator%20data][Sources 1-3: Physical event words + discriminator data:1]]
-- ** Sources 1-3: Physical event words + discriminator data

               when write_tempo =>

                  if priority_flag = "10" then
                     priority_flag <= "00";
                  else
                     priority_flag <= priority_flag + 1;
                  end if;
                  fifo_output1_rd  <= '0';
                  fifo_output2_rd  <= '0';
                  fifo_output3_rd  <= '0';
                  ChannelNb        <= (others => '0');
                  --
                  main_fifo_wr     <= '1';
                  main_fifo_datain <= main_fifo_datain_tmp;
                  rst_counter      <= '0';
                  --
                  if counter = '1' & X"FFFFFF" then
                     dataout_fifo_state <= write_tempo2;
                  else
                     dataout_fifo_state <= standby;
                  end if;
-- @@@ Sources 1-3: Physical event words + discriminator data:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Source%204%20:%20Special%20word][Source 4 : Special word:1]]
-- ** Source 4 : Special word

               when write_tempo2 =>

                  fifo_output1_rd <= '0';
                  fifo_output2_rd <= '0';
                  fifo_output3_rd <= '0';
                  if main_fifo_full = '0' then
                     main_fifo_wr     <= '1';
                     ChannelNb        <= ChannelNb + 1;
                     main_fifo_datain <= '0' &
                                         '1' &
                                         X"0" &
                                         "00" &
                                         X"000000" &
                                         "000" & X"00" &
                                         std_logic_vector(ChannelNb) &
                                         std_logic_vector(TriggerCounters_10ms(to_integer(ChannelNb)));
                     if ChannelNb = '1' & X"F" then
                        dataout_fifo_state <= standby;
                        rst_counter        <= '1';
                     else
                        dataout_fifo_state <= write_tempo2;
                        rst_counter        <= '0';
                     end if;
                  else
                     main_fifo_wr <= '0';
                  end if;

            end case;

         end if;

      end if;

   end process;
-- @@@ Source 4 : Special word:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Counter][Counter:1]]
-- * Counter

   process(clk_80Mhz)
   begin
      if (clk_80Mhz'event and clk_80Mhz = '1') then
         if (rst_counter = '1' or rst_special_data = '1') then
            counter <= (others => '0');
         else
            if counter < '1' & X"FFFFFF" then
               counter <= counter + 1;
            end if;
         end if;
      end if;
   end process;
-- @@@ Counter:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Big%20FIFO][Big FIFO:1]]
-- * Big FIFO
-- @@@ Big FIFO:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*Xilinx%20fifo][Xilinx fifo:1]]
-- ** Xilinx fifo

   cati_dataout_fifo_inst : cati_dataout_fifo
      port map (rst    => rst,
                wr_clk => clk_80Mhz,
                rd_clk => clk_usb,
                din    => main_fifo_datain,
                wr_en  => main_fifo_wr,
                rd_en  => fifo_rd,
                dout   => fifo_dataout,
                full   => main_fifo_full,
                empty  => fifo_empty);
-- @@@ Xilinx fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_data_capture.org::*end][end:1]]
-- * end

end architecture Behavioral;
-- @@@ end:1 ends here
