-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

library UNISIM;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

use work.juno_mezzanine_package.all;
use UNISIM.VComponents.all;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Entity][Entity:1]]
-- * Entity

entity p1_counter is
   port (clk_fpga     : in  std_logic;  -- generic oscilator present on board
         main_rst     : in  std_logic;
         -- usb
         clk_2232     : in  std_logic;  -- usb clock, 60 MHz
         usb_data_in  : in  std_logic_vector(31 downto 0);  -- usb Input bus (receive data from usb)
         usb_data_out : out std_logic_vector(127 downto 0);  -- usb Output bus (Send data to usb)
         usb_fifo1_rd : in  std_logic;  -- USB demande lecture de la fifo
         usb_wrreq    : in  std_logic;  --PC demande l'ordre decriture et de lecture dans la fifo
         fifo_empty   : out std_logic);
end entity p1_counter;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of p1_counter is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Signals][Signals:1]]
-- ** Signals

   signal fifo1_clear        : std_logic                     := '0';
   signal fifo1_full         : std_logic                     := '0';
   signal fifo1_almostFull   : std_logic                     := '0';
   signal fifo1_wrreq        : std_logic                     := '0';
   signal fifo1_dataIn       : std_logic_vector(31 downto 0) := (others => '0');
   signal fifo1_dataIn1      : unsigned(7 downto 0)          := (others => '0');
   signal fifo1_dataIn2      : unsigned(7 downto 0)          := X"01";
   signal fifo1_dataIn3      : unsigned(7 downto 0)          := X"02";
   signal fifo1_dataIn4      : unsigned(7 downto 0)          := X"03";
   signal fill_fifo1_counter : unsigned(31 downto 0)         := (others => '0');

   type command1_state_type is (command1_state_1, command1_state_2);
   signal command1_state : command1_state_type;
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Generic][Generic:1]]
-- ** Generic

   fifo1_clear <= not(usb_wrreq);
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Write%20Fifo][Write Fifo:1]]
-- ** Write Fifo

   process(clk_fpga)
   begin

      if (clk_fpga = '1' and clk_fpga'event) then  --On met les données d'ecriture sur le bus de la fifo sur front

         if (main_rst = '1' or usb_wrreq = '0') then

            fifo1_dataIn1 <= (others => '0');
            fifo1_dataIn2 <= X"01";
            fifo1_dataIn3 <= X"02";
            fifo1_dataIn4 <= X"03";

            fifo1_dataIn <= (others => '0');
            fifo1_wrreq  <= '0';

            fill_fifo1_counter <= (others => '0');
            command1_state     <= command1_state_1;

         else

            case command1_state is

               when command1_state_1 =>

                  fifo1_wrreq <= '0';
                  if (fill_fifo1_counter < unsigned(usb_data_in(31 downto 2) & "00")) and (fifo1_full = '0') then
                     command1_state <= command1_state_2;
                  end if;

               when command1_state_2 =>

                  if (fifo1_full = '0') then
                     fifo1_dataIn       <= std_logic_vector(fifo1_dataIn1) & std_logic_vector(fifo1_dataIn2) & std_logic_vector(fifo1_dataIn3) & std_logic_vector(fifo1_dataIn4);
                     fifo1_dataIn1      <= fifo1_dataIn1 + 4;
                     fifo1_dataIn2      <= fifo1_dataIn2 + 4;
                     fifo1_dataIn3      <= fifo1_dataIn3 + 4;
                     fifo1_dataIn4      <= fifo1_dataIn4 + 4;
                     fill_fifo1_counter <= fill_fifo1_counter + 4;
                     fifo1_wrreq        <= '1';
                     command1_state     <= command1_state_1;
                  else
                     fifo1_wrreq    <= '0';
                     command1_state <= command1_state_2;
                  end if;

            end case;

         end if;

      end if;  -- clk

   end process;
-- @@@ Write Fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*Xilinx%20fifo][Xilinx fifo:1]]
-- *** Xilinx fifo

   Cati_output_fifo1_inst : Cati_output_fifo1
      port map (rst    => fifo1_clear,
                wr_clk => clk_fpga,
                rd_clk => clk_2232,
                din    => fifo1_dataIn,
                wr_en  => fifo1_wrreq,
                rd_en  => usb_fifo1_rd,
                dout   => usb_data_out,
                full   => fifo1_full,
                empty  => fifo_empty);
-- @@@ Xilinx fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/p1_counter.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
