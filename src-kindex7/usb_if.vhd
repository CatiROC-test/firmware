-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Entity][Entity:1]]
-- * Entity

entity usb_if is
   generic (RegBankLength   : natural;   -- RegBankLength bytes
            nb_asics        : natural;   -- bytes
            --
            p1_data_width   : natural;   -- bytes
            p2_data_width   : natural;   -- bytes
            p3_data_width   : natural;   -- bytes
            p4_data_width   : natural;   -- bytes
            p5_data_width   : natural;   -- bytes
            p6_data_width   : natural;   -- bytes
            --
            p1_params_width : natural;   -- bytes
            p2_params_width : natural;   -- bytes
            p3_params_width : natural;
            p4_params_width : natural;
            p5_params_width : natural;
            p6_params_width : natural);  -- bytes);  -- bytes
   port (main_rst      : in    std_logic;
         interrupt_out : out   std_logic;
         BoardID       : in    std_logic_vector(7 downto 0);
         -- usb if
         usb_rxf       : in    std_logic;
         usb_txe       : in    std_logic;
         clk_2232      : in    std_logic;
         usb_oe        : out   std_logic;
         usb_rd        : out   std_logic;
         usb_wr        : out   std_logic;
         usb_siwua     : out   std_logic;
         usb_data      : inout std_logic_vector(7 downto 0);
         --
         disable_40MHz : out   std_logic;
         --
         -- Peripheral 1
         p1_datain     : in    std_logic_vector(p1_data_width*8-1 downto 0);
         p1_params     : out   std_logic_vector(p1_params_width*8-1 downto 0);
         p1_rd         : out   std_logic;
         p1_empty      : in    std_logic;
         p1_enable     : out   std_logic;
         -- Peripheral 2
         p2_datain     : in    std_logic_vector((p2_data_width+1)*8-1 downto 0);
         p2_params     : out   std_logic_vector(p2_params_width*8*nb_asics-1 downto 0);
         p2_rd         : out   std_logic;
         p2_empty      : in    std_logic;
         p2_enable     : out   std_logic;
         -- Peripheral 3
         p3_datain     : in    std_logic_vector((p3_data_width+1)*8-1 downto 0);
         p3_params     : out   std_logic_vector(p3_params_width*8*nb_asics-1 downto 0);
         p3_rd         : out   std_logic;
         p3_empty      : in    std_logic;
         p3_enable     : out   std_logic;
         -- Peripheral 4
         p4_datain     : in    std_logic_vector(p4_data_width*8-1 downto 0);
         p4_params     : out   std_logic_vector(p4_params_width*8-1 downto 0);
         p4_rd         : out   std_logic;
         p4_empty      : in    std_logic;
         p4_enable     : out   std_logic;
         -- Peripheral 5
         p5_datain     : in    std_logic_vector(p5_data_width*8-1 downto 0);
         p5_params     : out   std_logic_vector(p5_params_width*8-1 downto 0);
         p5_rd         : out   std_logic;
         p5_empty      : in    std_logic;
         p5_enable     : out   std_logic;
         -- Peripheral 6
         p6_datain     : in    std_logic_vector(p6_data_width*8-1 downto 0);
         p6_params     : out   std_logic_vector(p6_params_width*8-1 downto 0);
         p6_rd         : out   std_logic;
         p6_empty      : in    std_logic;
         p6_enable     : out   std_logic;
         --Inspection
         px1_sel       : out   std_logic_vector(15 downto 0);
         px2_sel       : out   std_logic_vector(15 downto 0));
end usb_if;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of usb_if is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Signaux][Signaux:1]]
-- ** Signaux

   type RegBank is array(0 to RegBankLength*nb_asics) of std_logic_vector(7 downto 0);
-- @@@ Signaux:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Signaux][Signaux:2]]
-- ** Signaux

   signal saved_word, data_taken     : std_logic_vector(127 downto 0)            := (others => '0');
   signal memo_fifo_dout             : std_logic_vector(143 downto 0)            := (others => '0');
   signal rst_write                  : std_logic                                 := '0';
   signal dina                       : std_logic_vector(7 downto 0)              := (others => '0');
   signal my_RegBank                 : RegBank                                   := (others => X"00");
   signal usb_rd_inner, interrupt    : std_logic                                 := '0';
   signal txe_one                    : std_logic                                 := '0';
   signal control_register           : std_logic_vector(7 downto 0)              := X"A5";
   signal pX_enable                  : std_logic_vector(5 downto 0)              := (others => '0');
   signal addrw                      : natural range 0 to RegBankLength*nb_asics := 0;
   signal addrrd, old_addrrb, MaxAdd : natural range 0 to RegBankLength*nb_asics := 0;
   signal max_loop                   : natural range 0 to RegBankLength*nb_asics := 0;
   signal pX_read                    : std_logic                                 := '0';
   signal disable_40MHz_tmp          : std_logic                                 := '0';
-- @@@ Signaux:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Signaux][Signaux:3]]
-- ** Signaux

   signal p2_params_tmp   : std_logic_vector(p2_params_width*8*nb_asics-1 downto 0) := (others => '0');
   signal p2_params_inner : std_logic_vector(p2_params_width*8*nb_asics-1 downto 0) := (others => '0');
   signal p3_params_tmp   : std_logic_vector(p3_params_width*8*nb_asics-1 downto 0) := (others => '0');
   signal p4_params_tmp   : std_logic_vector(p4_params_width*8-1 downto 0)          := (others => '0');
   signal p5_params_tmp   : std_logic_vector(p5_params_width*8-1 downto 0)          := (others => '0');
   signal p6_params_tmp   : std_logic_vector(p6_params_width*8-1 downto 0)          := (others => '0');
-- @@@ Signaux:3 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Signaux][Signaux:4]]
-- ** Signaux

   type read_state_type is (read_stand_by, read_state_1, read_state_2, read_wait_state);

   type write_state_type is (write_stand_by, write_readout, write_end,
                             write_wait_data_pulse, write_send_data_pulse, write_take_data_pulse_2,
                             write_control_register);

   signal read_state  : read_state_type;
   signal write_state : write_state_type;
   signal px1_sel_tmp : std_logic_vector(15 downto 0) := (others => '0');
   signal px2_sel_tmp : std_logic_vector(15 downto 0) := (others => '0');
-- @@@ Signaux:4 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Architecture%20:%20begin][Architecture : begin:1]]
-- ** Architecture : begin

begin
-- @@@ Architecture : begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Generic][Generic:1]]
-- ** Generic

   usb_siwua        <= '1';
-- usb_reset_fpga <= main_rst;          --'0'
-- button         <= main_rst;          --'0'
   usb_rd           <= usb_rd_inner;
   control_register <= X"04";
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*USB%20in][USB in:1]]
-- ** USB in

   U_USBIn : process (clk_2232)
   begin
      if (clk_2232 = '1' and clk_2232'event) then
         if (main_rst = '1') then
            usb_rd_inner <= '1';
            usb_oe       <= '1';
            interrupt    <= '1';
            addrw        <= 0;
            read_state   <= read_stand_by;
         else
            case read_state is
               when read_stand_by =>
                  usb_rd_inner <= '1';
                  usb_oe       <= '1';
                  interrupt    <= '0';
                  addrw        <= 0;
                  if usb_rxf = '0' then
                     read_state <= read_state_1;
                  else
                     read_state <= read_stand_by;
                  end if;
               when read_state_1 =>
                  usb_rd_inner <= '1';
                  usb_oe       <= '0';
                  interrupt    <= '1';
                  read_state   <= read_state_2;
               when read_state_2 =>
                  interrupt <= '1';
                  usb_oe    <= '0';
                  if (usb_rd_inner = '0' and usb_rxf = '0') then
                     addrw             <= addrw + 1;
                     my_RegBank(addrw) <= usb_data;
                     dina              <= usb_data;
                  end if;
                  if usb_rxf = '0' then
                     usb_rd_inner <= '0';
                     read_state   <= read_state_2;
                  else
                     usb_rd_inner <= '1';
                     read_state   <= read_wait_state;
                  end if;
               when read_wait_state =>
                  usb_rd_inner <= '1';
                  usb_oe       <= '1';
                  interrupt    <= '1';
                  -- MaxAdd       <= '0'&addrw;
                  MaxAdd       <= addrw;
                  if dina = X"FF" then
                     read_state <= read_stand_by;
                  elsif usb_rxf = '0' then
                     read_state <= read_state_1;
                  else
                     read_state <= read_wait_state;
                  end if;
            end case;
         end if;
      end if;
   end process;
-- @@@ USB in:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*USB%20in][USB in:2]]
-- ** USB in

   interrupt_out <= interrupt;
-- @@@ USB in:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*px%20enable][px enable:1]]
-- *** px enable

   pX_enable <= "000000" when (main_rst = '1' or interrupt = '1' or rst_write = '1') else
                "000001" when my_RegBank(0) = X"01" else
                "000010" when my_RegBank(0) = X"06" else
                "000100" when my_RegBank(0) = X"CC" else
                "001000" when my_RegBank(0) = X"07" else
                "010000" when my_RegBank(0) = X"08" else
                "100000" when my_RegBank(0) = X"09" else
                "000000";
-- @@@ px enable:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*px%20enable][px enable:2]]
-- *** px enable

   p1_enable <= pX_enable(0);
   p2_enable <= pX_enable(1) or pX_enable(5);
   p3_enable <= pX_enable(2);
   p4_enable <= pX_enable(3);
   p5_enable <= pX_enable(4);
   p6_enable <= pX_enable(5);
-- @@@ px enable:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*px%20read][px read:1]]
-- *** px read

   p1_rd <= pX_enable(0) and pX_read;
   p2_rd <= pX_enable(1) and pX_read;
   p3_rd <= pX_enable(2) and pX_read;
   p4_rd <= pX_enable(3) and pX_read;
   p5_rd <= pX_enable(4) and pX_read;
   p6_rd <= pX_enable(5) and pX_read;
-- @@@ px read:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*px%20read][px read:2]]
-- *** px read

   px1_sel_tmp <= my_RegBank(1) & my_RegBank(2) when my_RegBank(0) = X"E0" else px1_sel_tmp;
   px1_sel     <= px1_sel_tmp;
   px2_sel_tmp <= my_RegBank(1) & my_RegBank(2) when my_RegBank(0) = X"E1" else px2_sel_tmp;
   px2_sel     <= px2_sel_tmp;
-- @@@ px read:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*px%20params][px params:1]]
-- *** px params

   p1_params <= my_RegBank(1) & my_RegBank(2) & my_RegBank(3) & my_RegBank(4) when my_RegBank(0) = X"01" else (others => '0');

   U_p2_params : for i in 0 to p2_params_width*nb_asics-1 generate
      p2_params_tmp(p2_params'left-i*8 downto p2_params'left-i*8 - 7) <= my_RegBank(i+1);
   end generate;
   p2_params_inner <= p2_params_tmp when my_RegBank(0) = X"06" else p2_params_inner;
   p2_params <= p2_params_inner;

   U_p3_params : for i in 0 to p3_params_width*nb_asics-1 generate
      p3_params_tmp(p3_params'left-i*8 downto p3_params'left-i*8 - 7) <= my_RegBank(i+1);
   end generate;
   p3_params <= p3_params_tmp when my_RegBank(0) = X"CC" else (others => '0');

   U_p4_params : for i in 0 to p4_params_width-1 generate
      p4_params_tmp(p4_params'left-i*8 downto p4_params'left-i*8 - 7) <= my_RegBank(i+1);
   end generate;
   p4_params <= p4_params_tmp when my_RegBank(0) = X"07" else (others => '0');

   U_p5_params : for i in 0 to p5_params_width-1 generate
      p5_params_tmp(p5_params'left-i*8 downto p5_params'left-i*8 - 7) <= my_RegBank(i+1);
   end generate;
   p5_params <= p5_params_tmp when my_RegBank(0) = X"08" else (others => '0');

   U_p6_params : for i in 0 to p6_params_width-1 generate
      p6_params_tmp(p6_params'left-i*8 downto p6_params'left-i*8 - 7) <= my_RegBank(i+1);
   end generate;
   p6_params <= p6_params_tmp when my_RegBank(0) = X"09" else (others => '0');
-- @@@ px params:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Disable%2040MHz][Disable 40MHz:1]]
-- ** Disable 40MHz

   U_disable_40Hz : process (clk_2232)
   begin
      if (clk_2232 = '1' and clk_2232'event) then
         if my_RegBank(0) = X"0A" then
            disable_40MHz_tmp <= my_RegBank(1)(0);
         else
            disable_40MHz_tmp <= disable_40MHz_tmp;
         end if;
         disable_40MHz <= disable_40MHz_tmp;
      end if;
   end process;
-- @@@ Disable 40MHz:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*rxt%20write][rxt write:1]]
-- *** rxt write

   rst_write <= '0' when (my_RegBank(0) = X"0B" or my_RegBank(0) = X"05" or my_RegBank(0) = X"04" or
                          my_RegBank(0) = X"01" or my_RegBank(0) = X"06" or my_RegBank(0) = X"CC" or
                          my_RegBank(0) = X"07" or my_RegBank(0) = X"08" or my_RegBank(0) = X"09") else '1';
-- @@@ rxt write:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*Usb%20out%20FSM][Usb out FSM:1]]
-- *** Usb out FSM

   U_USBOut : process (clk_2232)
   begin

      if (clk_2232 = '1' and clk_2232'event) then

         -- *** Sync Reset
         if (interrupt = '1' or rst_write = '1' or main_rst = '1') then

            usb_wr   <= '1';
            addrrd   <= 0;
            usb_data <= (others => 'Z');
            txe_one  <= '1';
            pX_read  <= '0';
            if (my_RegBank(0) = X"01" or my_RegBank(0) = X"06" or my_RegBank(0) = X"CC" or my_RegBank(0) = X"07" or my_RegBank(0) = X"08" or my_RegBank(0) = X"09") then
               write_state <= write_wait_data_pulse;
            elsif my_RegBank(0) = X"0B" then
               -- X"0B" readout control register
               write_state <= write_control_register;
            else
               -- X"05" returns register bank
               -- X"04" returns a ramp
               write_state <= write_stand_by;
            end if;

         else

            case write_state is

               -- *** readout pulses
               when write_wait_data_pulse =>
                  usb_wr   <= '1';
                  usb_data <= (others => '1');
                  addrrd   <= 0;
                  txe_one  <= '1';
                  if (p1_empty = '0' and usb_txe = '0') or
                     (p2_empty = '0' and usb_txe = '0') or
                     (p3_empty = '0' and usb_txe = '0') or
                     (p4_empty = '0' and usb_txe = '0') or
                     (p5_empty = '0' and usb_txe = '0') or
                     (p6_empty = '0' and usb_txe = '0') then
                     pX_read     <= '0';
                     write_state <= write_take_data_pulse_2;
                  else
                     pX_read     <= '0';
                     write_state <= write_wait_data_pulse;
                  end if;

               when write_take_data_pulse_2 =>
                  usb_wr   <= '1';
                  usb_data <= (others => '1');
                  addrrd   <= 0;
                  pX_read  <= '1';
                  txe_one  <= '1';
                  if pX_enable(0) = '1' then
                     data_taken(data_taken'left downto data_taken'left+1-p1_data_width*8) <= p1_datain;
                     max_loop                                                             <= p1_data_width;
                  elsif pX_enable(1) = '1' then
                     data_taken(data_taken'left downto data_taken'left+1-(p2_data_width+2)*8) <= p2_datain & BoardID;
                     max_loop                                                                 <= p2_data_width+2;
                  elsif pX_enable(2) = '1' then
                     data_taken(data_taken'left downto data_taken'left+1-(p3_data_width+2)*8) <= p3_datain & BoardID;
                     max_loop                                                                 <= p3_data_width+2;
                  elsif pX_enable(3) = '1' then
                     data_taken(data_taken'left downto
                                data_taken'left+1-p4_data_width*8) <=
                        p4_datain(15 downto 0) & p4_datain(31 downto 16) &
                        p4_datain(47 downto 32) & p4_datain(63 downto 48) &
                        p4_datain(79 downto 64) & p4_datain(95 downto 80) &
                        p4_datain(111 downto 96) & p4_datain(127 downto 112);
                     max_loop <= p4_data_width;
                  elsif pX_enable(4) = '1' then
                     data_taken(data_taken'left downto data_taken'left+1-p5_data_width*8) <= p5_datain;
                     max_loop                                                             <= p5_data_width;
                  else
                     data_taken(data_taken'left downto data_taken'left+1-p6_data_width*8) <= p6_datain;
                     max_loop                                                             <= p6_data_width;
                  end if;
                  write_state <= write_send_data_pulse;

               when write_send_data_pulse =>

                  pX_read <= '0';
                  if (addrrd = max_loop and usb_txe = '0') then
                     usb_wr      <= '1';
                     txe_one     <= '1';
                     usb_data    <= (others => '1');
                     write_state <= write_wait_data_pulse;
                  elsif usb_txe = '0' then
                     addrrd      <= addrrd + 1;
                     usb_wr      <= '0';
                     txe_one     <= '1';
                     usb_data    <= data_taken(data_taken'left downto data_taken'left+1-8);
                     saved_word  <= data_taken;
                     data_taken  <= data_taken(data_taken'left+1-9 downto 0) & X"FF";
                     write_state <= write_send_data_pulse;
                  else
                     if (addrrd > 0 and txe_one = '1') then
                        addrrd <= addrrd - 1;
                     end if;
                     txe_one     <= '0';
                     usb_wr      <= '1';
                     usb_data    <= (others => '1');
                     data_taken  <= saved_word;
                     write_state <= write_send_data_pulse;
                  end if;

               -- *** readout register bank
               when write_stand_by =>
                  usb_wr   <= '1';
                  usb_data <= (others => '0');
                  addrrd   <= addrrd;
                  if usb_txe = '0' then
                     write_state <= write_readout;
                  else
                     write_state <= write_stand_by;
                  end if;

               when write_readout =>
                  if (addrrd = MaxAdd and my_RegBank(0) = X"05") then
                     usb_wr      <= '1';
                     usb_data    <= (others => '0');
                     addrrd      <= 0;
                     write_state <= write_end;
                  elsif usb_txe = '0' then
                     usb_wr <= '0';
                     if my_RegBank(0) = X"05" then
                        -- usb_data <= my_RegBank(to_integer(addrrd));
                        usb_data <= my_RegBank(addrrd);
                     else
                        usb_data <= (others => '0');  -- std_logic_vector(addrrd(7 downto 0));
                     end if;
                     addrrd      <= addrrd + 1;
                     old_addrrb  <= addrrd;
                     write_state <= write_readout;
                  else
                     usb_wr      <= '1';
                     usb_data    <= (others => '0');
                     addrrd      <= old_addrrb;
                     write_state <= write_readout;
                  end if;

               when write_end =>
                  usb_wr      <= '1';
                  usb_data    <= (others => '0');
                  addrrd      <= 0;
                  write_state <= write_end;

               -- *** readout control register
               when write_control_register =>
                  usb_wr      <= '0';
                  usb_data    <= control_register;
                  write_state <= write_end;

            end case;

         end if;

      end if;

   end process;
-- @@@ Usb out FSM:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/usb_if.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
