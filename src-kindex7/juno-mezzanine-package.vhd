-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
use IEEE.STD_LOGIC_1164.all;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*Package][Package:1]]
-- * Package

package juno_mezzanine_package is
-- @@@ Package:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*Components][Components:1]]
-- ** Components
-- @@@ Components:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*discri_small_fifo][discri_small_fifo:1]]
-- *** discri_small_fifo

   component discri_small_fifo
      port (wr_clk : in  std_logic;
            rd_clk : in  std_logic;
            rst    : in  std_logic;
            din    : in  std_logic_vector(63 downto 0);
            wr_en  : in  std_logic;
            rd_en  : in  std_logic;
            dout   : out std_logic_vector(63 downto 0);
            full   : out std_logic;
            empty  : out std_logic);
   end component;
-- @@@ discri_small_fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*pll][pll:1]]
-- *** pll

   component pll
      port (CLK_IN1  : in  std_logic;
            -- Clock out ports
            CLK_OUT1 : out std_logic;
            CLK_OUT2 : out std_logic;
            CLK_OUT3 : out std_logic;
            CLK_OUT4 : out std_logic;
            CLK_OUT5 : out std_logic;
            CLK_OUT6 : out std_logic;
            CLK_OUT7 : out std_logic;
            -- Status and control signals
            RESET    : in  std_logic;
            LOCKED   : out std_logic);
   end component;
-- @@@ pll:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*pll%20gcu][pll gcu:1]]
-- *** pll gcu

   component pll_gcu
      port
         (                              -- Clock in ports
            -- Clock out ports
            clk_out1 : out std_logic;
            -- Status and control signals
            reset    : in  std_logic;
            locked   : out std_logic;
            clk_in1  : in  std_logic
            );
   end component;
-- @@@ pll gcu:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*select%20wiz][select wiz:1]]
-- *** select wiz

   component selectio_wiz_0
      -- generic
      --    (                              -- width of the data for the system
      --       SYS_W : integer := 1;
      --       -- width of the data for the device
      --       DEV_W : integer := 8);
      port
         (
            -- From the system into the device
            data_in_from_pins : in  std_logic_vector(1-1 downto 0);
            data_in_to_device : out std_logic_vector(8-1 downto 0);
            bitslip           : in  std_logic_vector(1-1 downto 0);  -- Bitslip module is enabled in NETWORKING mode
            -- User should tie it to '0' if not needed
            -- Clock and reset signals
            clk_in            : in  std_logic;   -- Fast clock from PLL/MMCM
            clk_div_in        : in  std_logic;   -- Slow clock from PLL/MMCM
            io_reset          : in  std_logic);  -- Reset signal for IO circuit
   end component;
-- @@@ select wiz:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*cati_dataout_fifo][cati_dataout_fifo:1]]
-- *** cati_dataout_fifo

   component cati_dataout_fifo
      port (rst    : in  std_logic;
            wr_clk : in  std_logic;
            rd_clk : in  std_logic;
            din    : in  std_logic_vector(63 downto 0);
            wr_en  : in  std_logic;
            rd_en  : in  std_logic;
            dout   : out std_logic_vector(63 downto 0);
            full   : out std_logic;
            empty  : out std_logic);
   end component;
-- @@@ cati_dataout_fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*Fifos][Fifos:1]]
-- *** Fifos

   component Cati_frame_fifo
      port (clk         : in  std_logic;
            rst         : in  std_logic;
            din         : in  std_logic_vector(47 downto 0);
            wr_en       : in  std_logic;
            rd_en       : in  std_logic;
            dout        : out std_logic_vector(47 downto 0);
            full        : out std_logic;
            almost_full : out std_logic;
            empty       : out std_logic);
   end component;

   component Cati_unifFifo
      port (clk   : in  std_logic;
            rst   : in  std_logic;
            din   : in  std_logic_vector(63 downto 0);
            wr_en : in  std_logic;
            rd_en : in  std_logic;
            dout  : out std_logic_vector(63 downto 0);
            full  : out std_logic;
            empty : out std_logic);
   end component;

   component Cati_output_fifoSC
      port (rst    : in  std_logic;
            wr_clk : in  std_logic;
            rd_clk : in  std_logic;
            din    : in  std_logic_vector(7 downto 0);
            wr_en  : in  std_logic;
            rd_en  : in  std_logic;
            dout   : out std_logic_vector(7 downto 0);
            full   : out std_logic;
            empty  : out std_logic);
   end component;

   component Cati_output_fifo1
      port (rst    : in  std_logic;
            wr_clk : in  std_logic;
            rd_clk : in  std_logic;
            din    : in  std_logic_vector(31 downto 0);
            wr_en  : in  std_logic;
            rd_en  : in  std_logic;
            dout   : out std_logic_vector(127 downto 0);
            full   : out std_logic;
            empty  : out std_logic);
   end component;
-- @@@ Fifos:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*End%20package][End package:1]]
-- ** End package

end juno_mezzanine_package;
-- @@@ End package:1 ends her
-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/juno-mezzanine-package.org::*Package%20Body][Package Body:1]]


-- @@@ Package Body:1 ends here
