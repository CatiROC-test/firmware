-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Xilinx%20fifo][Xilinx fifo:1]]
-- ** Xilinx fifo

use work.juno_mezzanine_package.all;
-- @@@ Xilinx fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Entity][Entity:1]]
-- * Entity

entity discriminator_data is
   generic (num_of_discri_channels : natural range 1 to 16);
   port (rst                     : in  std_logic;
         ovf                     : in  std_logic;
         resetb                  : in  std_logic;
         clk_80MHz               : in  std_logic;  -- clk out @ 80 MHz
         clk_40MHz               : in  std_logic;
         discri_rising           : in  std_logic_vector(15 downto 0);
         discri_falling          : in  std_logic_vector(15 downto 0);
         discri_data_f           : in  std_logic_vector(num_of_discri_channels*6-1 downto 0);
         discri_data_r           : in  std_logic_vector(num_of_discri_channels*6-1 downto 0);
         tstamp                  : in  std_logic_vector(31 downto 0);
         ReduceDiscriminatorData : in  std_logic;
         ---periferal
         fifo_dataout            : out std_logic_vector(63 downto 0);
         fifo_rd                 : in  std_logic;
         fifo_empty              : out std_logic);
end entity discriminator_data;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of discriminator_data is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Signals][Signals:1]]
-- ** Signals

   type EventCounterType is array(0 to 15) of unsigned(24 downto 0);
   signal EventCounters              : EventCounterType                                    := (others => '0' & X"000000");
   type FifoSignalType is array(0 to 15) of std_logic_vector(63 downto 0);
   signal fifo_small_data_in         : FifoSignalType                                      := (others => X"0000000000000000");
   signal fifo_small_data_out        : FifoSignalType                                      := (others => X"0000000000000000");
   signal fifo_small_empty           : std_logic_vector(15 downto 0)                       := (others => '1');
   signal fifo_small_wr              : std_logic_vector(num_of_discri_channels-1 downto 0) := (others => '0');
   signal fifo_small_rd              : std_logic_vector(15 downto 0)                       := (others => '0');
   signal fifo_big_data_in           : std_logic_vector(63 downto 0)                       := (others => '0');
   signal fifo_big_full, fifo_big_wr : std_logic                                           := '0';
   type dataout_fifo_state_type is (standby, write_tempo);
   signal dataout_fifo_state         : dataout_fifo_state_type;
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Signals][Signals:2]]
-- ** Signals

   signal num_of_discri_channels_unsigned : unsigned(3 downto 0) := X"0";
   signal priority_flag                   : unsigned(3 downto 0) := X"0";
-- @@@ Signals:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Juno%20mezzanine][Juno mezzanine:1]]
-- *** Juno mezzanine

   signal cnt_f, cnt_r                       : std_logic_vector(num_of_discri_channels-1 downto 0) := (others => '0');
   signal fifo_pre_data_in                   : FifoSignalType                                      := (others => (others => '0'));
   signal fifo_pre_data_out                  : FifoSignalType                                      := (others => (others => '0'));
   signal fifo_pre_empty                     : std_logic_vector(15 downto 0)                       := (others => '1');
   signal fifo_pre_wr                        : std_logic_vector(num_of_discri_channels-1 downto 0) := (others => '0');
   signal fifo_pre_rd                        : std_logic_vector(15 downto 0)                       := (others => '0');
   signal fifo_opt_data_in                   : FifoSignalType                                      := (others => (others => '0'));
   signal fifo_opt_wr                        : std_logic_vector(num_of_discri_channels-1 downto 0) := (others => '0');
   signal first                              : std_logic_vector(num_of_discri_channels-1 downto 0) := (others => '0');
   signal first_edge, second_edge            : std_logic_vector(15 downto 0)                       := (others => '0');
   signal first_edge_l, second_edge_l        : std_logic_vector(15 downto 0)                       := (others => '0');
   type tstamp_t is array(0 to 15) of std_logic_vector(25 downto 0);
   signal first_tstamp, second_tstamp        : tstamp_t                                            := (others => (others => '0'));
   signal first_tstamp_l, second_tstamp_l    : tstamp_t                                            := (others => (others => '0'));
   type finet_t is array(0 to 15) of std_logic_vector(5 downto 0);
   signal first_finet, second_finet          : finet_t                                             := (others => (others => '0'));
   signal first_finet_l, second_finet_l      : finet_t                                             := (others => (others => '0'));
   type width_t is array(0 to 15) of std_logic_vector(51 downto 0);
   signal width                              : width_t                                             := (others => (others => '0'));
   type timeout_t is array(0 to 15) of std_logic_vector(7 downto 0);
   signal timeout                            : timeout_t                                           := (others => (others => '0'));
   signal event_timeout, event_timeout_l     : std_logic_vector(15 downto 0)                       := (others => '0');
   signal event_timeout_r, event_timeout_r_l : std_logic_vector(15 downto 0)                       := (others => '0');
   signal event_close, event_close_l         : std_logic_vector(15 downto 0)                       := (others => '0');
   type EventCType is array(0 to 15) of unsigned(17 downto 0);
   signal EventC                             : EventCType                                          := (others => (others => '0'));
-- @@@ Juno mezzanine:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Store%20events][Store events:1]]
-- ** Store events
-- @@@ Store events:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Send%20to%20fifos][Send to fifos:1]]
-- **** Send to fifos

   U_events : for i in 0 to num_of_discri_channels-1 generate
      process(clk_80MHz)
      begin
         if (clk_80MHz'event and clk_80MHz = '1') then
            if (rst = '1') then
               fifo_pre_data_in(i) <= X"000000000000000B";
               fifo_pre_wr(i)      <= '0';
               EventCounters(i)    <= '0' & X"000000";
               cnt_f(i)            <= '0';
               cnt_r(i)            <= '0';
            else
               if discri_falling(i) = '1' and cnt_f(i) = '0' then
                  fifo_pre_wr(i)      <= '1';
                  fifo_pre_data_in(i) <= "100" &
                                         std_logic_vector(to_unsigned(i, 4)) &
                                         std_logic_vector(EventCounters(i)) &
                                         tstamp(25 downto 0) & discri_data_f(6*(i+1)-1 downto 6*i);
                  EventCounters(i) <= EventCounters(i) + 1;
                  cnt_f(i)         <= '1';
                  cnt_r(i)         <= '0';
               elsif discri_rising(i) = '1' and cnt_r(i) = '0' then
                  fifo_pre_wr(i)      <= '1';
                  fifo_pre_data_in(i) <= "101" &
                                         std_logic_vector(to_unsigned(i, 4)) &
                                         std_logic_vector(EventCounters(i)) &
                                         tstamp(25 downto 0) & discri_data_r(6*(i+1)-1 downto 6*i);
                  EventCounters(i) <= EventCounters(i) + 1;
                  cnt_r(i)         <= '1';
                  cnt_f(i)         <= '0';
               else
                  fifo_pre_wr(i)      <= '0';
                  fifo_pre_data_in(i) <= X"000000000000000A";
                  EventCounters(i)    <= EventCounters(i);
                  cnt_f(i)            <= '0';
                  cnt_r(i)            <= '0';
               end if;
            end if;
         end if;
      end process;
-- @@@ Send to fifos:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Data%20reduction:%20switching][Data reduction: switching:1]]
-- **** Data reduction: switching

      fifo_small_data_in <= fifo_pre_data_in when ReduceDiscriminatorData = '0' else
                            fifo_opt_data_in;
      fifo_small_wr <= fifo_pre_wr when ReduceDiscriminatorData = '0' else
                       fifo_opt_wr;

      event_timeout_r(i) <= '1' when (event_timeout(i) = '1' and event_timeout_l(i) = '0') else
                            '0';
-- @@@ Data reduction: switching:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Data%20reduction:%20corner%20cases][Data reduction: corner cases:1]]
-- **** Data reduction: corner cases

      process(clk_80MHz)
      begin
         if (clk_80MHz'event and clk_80MHz = '1') then
            if (rst = '1') then
               first(i)         <= '0';
               timeout(i)       <= (others => '0');
               event_close(i)   <= '0';
               event_timeout(i) <= '0';
            else
               if fifo_pre_wr(i) = '1' then
                  if first(i) = '0' then
                     first_edge(i)   <= fifo_pre_data_in(i)(61);
                     first_tstamp(i) <= fifo_pre_data_in(i)(31 downto 6);
                     first_finet(i)  <= fifo_pre_data_in(i)(5 downto 0);
                     event_close(i)  <= '0';
                  else
                     second_edge(i)   <= fifo_pre_data_in(i)(61);
                     second_tstamp(i) <= fifo_pre_data_in(i)(31 downto 6);
                     second_finet(i)  <= fifo_pre_data_in(i)(5 downto 0);
                     event_close(i)   <= '1';
                  end if;
                  if unsigned(timeout(i)) <= 8 then
                     first(i)         <= not first(i);
                     event_timeout(i) <= '0';
                  else
                     first(i)         <= '0';
                     event_timeout(i) <= '1';
                  end if;
               else
                  if first(i) = '0' then
                     first_edge(i)    <= '0';
                     first_tstamp(i)  <= (others => '0');
                     first_finet(i)   <= (others => '0');
                     second_edge(i)   <= '0';
                     second_tstamp(i) <= (others => '0');
                     second_finet(i)  <= (others => '0');
                  end if;
                  event_close(i) <= '0';
               end if;
               if first(i) = '1' then
                  timeout(i) <= std_logic_vector(unsigned(timeout(i)) + 1);
               else
                  timeout(i) <= (others => '0');
               end if;

               width(i) <= std_logic_vector(unsigned(second_tstamp(i))*24 + unsigned(second_finet(i)) -
                                            unsigned(first_tstamp(i))*24 - unsigned(first_finet(i)));

               event_close_l(i)     <= event_close(i);
               event_timeout_l(i)   <= event_timeout(i);
               event_timeout_r_l(i) <= event_timeout_r(i);
               first_edge_l(i)      <= first_edge(i);
               second_edge_l(i)     <= second_edge(i);

               if (event_close_l(i) or event_timeout_r_l(i)) = '1' then
                  EventC(i)      <= EventC(i) + 1;
                  fifo_opt_wr(i) <= '1';
                  if event_timeout_r_l(i) = '1' then
                     fifo_opt_data_in(i) <= "10" & first_edge_l(i) &
                                            std_logic_vector(to_unsigned(i, 4)) &
                                            std_logic_vector(EventC(i)) & "1111111" &
                                            first_tstamp_l(i) & first_finet_l(i);
                  elsif event_close_l(i) = '1' then
                     if first_edge_l(i) /= second_edge_l(i) then
                        fifo_opt_data_in(i) <= "100" &
                                               std_logic_vector(to_unsigned(i, 4)) &
                                               std_logic_vector(EventC(i)) & width(i)(6 downto 0) &
                                               first_tstamp_l(i) & first_finet_l(i);
                     else
                        fifo_opt_data_in(i) <= "10" & first_edge_l(i) &
                                               std_logic_vector(to_unsigned(i, 4)) &
                                               std_logic_vector(EventC(i)) & "0000000" &
                                               first_tstamp_l(i) & first_finet_l(i);
                     end if;
                  end if;
               else
                  fifo_opt_data_in(i) <= (others => '0');
                  fifo_opt_wr(i)      <= '0';
               end if;
            end if;
         end if;
      end process;
-- @@@ Data reduction: corner cases:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Juno%20mezzanine%20code][Juno mezzanine code:1]]
-- *** Juno mezzanine code

      U_discri_small_fifo : discri_small_fifo
         port map (wr_clk => clk_80MHz,
                   rd_clk => clk_80MHz,
                   rst    => rst,
                   din    => fifo_small_data_in(i),
                   wr_en  => fifo_small_wr(i),
                   rd_en  => fifo_small_rd(i),
                   dout   => fifo_small_data_out(i),
                   full   => open,
                   empty  => fifo_small_empty(i));

   end generate;
-- @@@ Juno mezzanine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Randomize][Randomize:1]]
-- *** Randomize

   num_of_discri_channels_unsigned <= to_unsigned(num_of_discri_channels-1, 4);
-- @@@ Randomize:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Randomize][Randomize:2]]
-- *** Randomize

   process(clk_80Mhz)
   begin
      if (clk_80Mhz'event and clk_80Mhz = '1') then
         if rst = '1' then
            priority_flag <= X"0";
         else
            if priority_flag = num_of_discri_channels_unsigned then
               priority_flag <= (others => '0');
            else
               priority_flag <= priority_flag + 1;
            end if;
         end if;
      end if;
   end process;
-- @@@ Randomize:2 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Merge%20process][Merge process:1]]
-- *** Merge process

   process(clk_80Mhz)

   begin

      if (clk_80Mhz'event and clk_80Mhz = '1') then

         if rst = '1' then

            fifo_big_wr        <= '0';
            fifo_big_data_in   <= X"000000000000000C";
            dataout_fifo_state <= standby;

         else

            case dataout_fifo_state is

               when standby =>

                  if (priority_flag = X"0" and fifo_small_empty(0) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(0);
                     fifo_small_rd      <= X"0001";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"1" and fifo_small_empty(1) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(1);
                     fifo_small_rd      <= X"0002";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"2" and fifo_small_empty(2) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(2);
                     fifo_small_rd      <= X"0004";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"3" and fifo_small_empty(3) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(3);
                     fifo_small_rd      <= X"0008";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"4" and fifo_small_empty(4) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(4);
                     fifo_small_rd      <= X"0010";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"5" and fifo_small_empty(5) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(5);
                     fifo_small_rd      <= X"0020";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"6" and fifo_small_empty(6) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(6);
                     fifo_small_rd      <= X"0040";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"7" and fifo_small_empty(7) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(7);
                     fifo_small_rd      <= X"0080";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"8" and fifo_small_empty(8) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(8);
                     fifo_small_rd      <= X"0100";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  -- 9
                  elsif (priority_flag = X"9" and fifo_small_empty(9) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(9);
                     fifo_small_rd      <= X"0200";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"A" and fifo_small_empty(10) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(10);
                     fifo_small_rd      <= X"0400";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"B" and fifo_small_empty(11) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(11);
                     fifo_small_rd      <= X"0800";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  -- 12
                  elsif (priority_flag = X"C" and fifo_small_empty(12) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(12);
                     fifo_small_rd      <= X"1000";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"D" and fifo_small_empty(13) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(13);
                     fifo_small_rd      <= X"2000";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"E" and fifo_small_empty(14) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(14);
                     fifo_small_rd      <= X"4000";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  elsif (priority_flag = X"F" and fifo_small_empty(15) = '0' and fifo_big_full = '0') then
                     fifo_big_data_in   <= fifo_small_data_out(15);
                     fifo_small_rd      <= X"8000";
                     fifo_big_wr        <= '1';
                     dataout_fifo_state <= write_tempo;
                  else
                     fifo_big_data_in   <= X"000000000000000E";
                     fifo_small_rd      <= X"0000";
                     fifo_big_wr        <= '0';
                     dataout_fifo_state <= standby;
                  end if;

               when write_tempo =>

                  fifo_big_data_in   <= X"000000000000000D";
                  fifo_big_wr        <= '0';
                  fifo_small_rd      <= X"0000";
                  dataout_fifo_state <= standby;

            end case;

         end if;

      end if;

   end process;
-- @@@ Merge process:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*Juno%20mezzanine%20code][Juno mezzanine code:1]]
-- *** Juno mezzanine code

   Cati_unifFifo_inst1 : Cati_unifFifo
      port map (clk   => clk_80MHz,
                rst   => rst,
                din   => fifo_big_data_in,
                wr_en => fifo_big_wr,
                rd_en => fifo_rd,
                dout  => fifo_dataout,
                full  => fifo_big_full,
                empty => fifo_empty);
-- @@@ Juno mezzanine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/discriminator_data.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
