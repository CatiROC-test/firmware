# M2 (#1)     M1 (#3)
# M4 (#2)     M3 (#4)
#
#        FPGA
#
# M36 (#5)    M35 (#7)
# M38 (#6)    M37 (#8)

# * Clocks

# Clock FPGA Input

set_property IOSTANDARD LVCMOS33 [get_ports clk_fpga]
set_property PACKAGE_PIN F17 [get_ports clk_fpga]
# create_clock -period 25.0 -name clk_fpga_time [get_ports clk_fpga] # set frequency to 40 MHz
# set_clock_groups -asynch -group [get_clocks clk_fpga_time] # declare async with respecto to rest of clock domains

# clk_40MHz out

set_property IOSTANDARD LVDS_25 [get_ports clk_40MHz_out_p]
set_property IOSTANDARD LVDS_25 [get_ports clk_40MHz_out_n]
set_property PACKAGE_PIN M21 [get_ports clk_40MHz_out_p]
set_property PACKAGE_PIN M22 [get_ports clk_40MHz_out_n]

# clk_160MHz out

set_property IOSTANDARD LVDS_25 [get_ports clk_160MHz_out_p]
set_property IOSTANDARD LVDS_25 [get_ports clk_160MHz_out_n]
set_property PACKAGE_PIN N19 [get_ports clk_160MHz_out_p]
set_property PACKAGE_PIN M20 [get_ports clk_160MHz_out_n]

# Usb clock

set_property IOSTANDARD LVCMOS33 [get_ports clk_2232]
set_property PACKAGE_PIN E18 [get_ports clk_2232]
# set frequency to 60 MHz
create_clock -period 16.666 -name clk_usb_time [get_ports clk_2232]
# declare async with respecto to rest of clock domains
set_clock_groups -asynch -group [get_clocks clk_usb_time]

# * USB interface

# usb_data

set_property IOSTANDARD LVTTL [get_ports {usb_data[7]}]
set_property PACKAGE_PIN B16 [get_ports {usb_data[7]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[6]}]
set_property PACKAGE_PIN A18 [get_ports {usb_data[6]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[5]}]
set_property PACKAGE_PIN A19 [get_ports {usb_data[5]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[4]}]
set_property PACKAGE_PIN B17 [get_ports {usb_data[4]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[3]}]
set_property PACKAGE_PIN A17 [get_ports {usb_data[3]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[2]}]
set_property PACKAGE_PIN C19 [get_ports {usb_data[2]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[1]}]
set_property PACKAGE_PIN B19 [get_ports {usb_data[1]}]
set_property IOSTANDARD LVTTL [get_ports {usb_data[0]}]
set_property PACKAGE_PIN C17 [get_ports {usb_data[0]}]

# usb_rxf

set_property IOSTANDARD LVCMOS33 [get_ports usb_rxf]
set_property PACKAGE_PIN E15 [get_ports usb_rxf]
set_property PULLUP true [get_ports usb_rxf]

# usb_txe

set_property IOSTANDARD LVCMOS33 [get_ports usb_txe]
set_property PACKAGE_PIN E16 [get_ports usb_txe]
set_property PULLUP true [get_ports usb_txe]

# usb_rd

set_property IOSTANDARD LVCMOS33 [get_ports usb_rd]
set_property PACKAGE_PIN G17 [get_ports usb_rd]

# usb_wr

set_property IOSTANDARD LVCMOS33 [get_ports usb_wr]
set_property PACKAGE_PIN F18 [get_ports usb_wr]

# usb_oe

set_property IOSTANDARD LVCMOS33 [get_ports usb_oe]
set_property PACKAGE_PIN C16 [get_ports usb_oe]

# * Test points

# PX1
set_property IOSTANDARD LVCMOS18 [get_ports PX1]
set_property PACKAGE_PIN AA3 [get_ports PX1]

# PX2
set_property IOSTANDARD LVCMOS18 [get_ports PX2]
set_property PACKAGE_PIN AA2 [get_ports PX2]

# PX3
set_property IOSTANDARD LVCMOS18 [get_ports PX3]
set_property PACKAGE_PIN AB4 [get_ports PX3]

# PX4
set_property IOSTANDARD LVCMOS18 [get_ports PX4]
set_property PACKAGE_PIN AA4 [get_ports PX4]

# * Asics

# ** Common

# *** Data capture

set_property IOSTANDARD LVDS_25 [get_ports {cati_dataout_p[*]}]
set_property IOSTANDARD LVDS_25 [get_ports {cati_dataout_n[*]}]
set_property DIFF_TERM TRUE [get_ports {cati_dataout_p[*]}]

set_property IOSTANDARD LVCMOS18 [get_ports cati_strobe[*]]

# *** Slow Control

set_property IOSTANDARD LVCMOS33 [get_ports sr_clk]
set_property PACKAGE_PIN M17 [get_ports {sr_clk}]

set_property IOSTANDARD LVCMOS33 [get_ports sr_rst]
set_property PACKAGE_PIN L20 [get_ports {sr_rst}]

set_property IOSTANDARD LVCMOS18 [get_ports sr_select]
set_property PACKAGE_PIN Y5 [get_ports {sr_select}]

set_property IOSTANDARD LVCMOS33 [get_ports {sr_in[*]}]
set_property IOSTANDARD LVCMOS33 [get_ports {sr_out[*]}]

# *** Rest

# ovf

set_property IOSTANDARD LVCMOS25 [get_ports ovf]
set_property PACKAGE_PIN U16 [get_ports ovf]

# resetb : output rest

set_property IOSTANDARD LVCMOS33 [get_ports resetb]
set_property PACKAGE_PIN E17 [get_ports resetb]

# pwr_on : output power on
set_property IOSTANDARD LVCMOS33 [get_ports pwr_on]
set_property PACKAGE_PIN D16 [get_ports pwr_on]

# trig ext
set_property IOSTANDARD LVCMOS33 [get_ports trig_ext]
set_property PACKAGE_PIN L17 [get_ports {trig_ext}]

# StartSys
set_property IOSTANDARD LVCMOS18 [get_ports StartSys]
set_property PACKAGE_PIN Y6 [get_ports {StartSys}]

# val_evt
set_property IOSTANDARD LVDS_25 [get_ports val_evt_p]
set_property IOSTANDARD LVDS_25 [get_ports val_evt_n]
set_property PACKAGE_PIN T24 [get_ports val_evt_p]
set_property PACKAGE_PIN T25 [get_ports val_evt_n]

# ** M2

# *** Data capture

# set_property PACKAGE_PIN R16 [get_ports {cati_dataout_p[0]}]
# set_property PACKAGE_PIN R17 [get_ports {cati_dataout_n[0]}]
# set_property PACKAGE_PIN U19 [get_ports {cati_dataout_p[1]}]
# set_property PACKAGE_PIN U20 [get_ports {cati_dataout_n[1]}]

# set_property PACKAGE_PIN U4 [get_ports {cati_strobe[0]}]
# set_property PACKAGE_PIN Y1 [get_ports {cati_strobe[1]}]

# *** Slow Control

# set_property PACKAGE_PIN D18 [get_ports {sr_in}]
# set_property PACKAGE_PIN E20 [get_ports {sr_out}]

# *** Triggers / discri output

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
# set_property PACKAGE_PIN U6 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
# set_property PACKAGE_PIN U5 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
# set_property PACKAGE_PIN U2 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
# set_property PACKAGE_PIN U1 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
# set_property PACKAGE_PIN W6 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
# set_property PACKAGE_PIN W5 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
# set_property PACKAGE_PIN V3 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
# set_property PACKAGE_PIN W3 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
# set_property PACKAGE_PIN U7 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
# set_property PACKAGE_PIN V6 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
# set_property PACKAGE_PIN V4 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
# set_property PACKAGE_PIN W4 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
# set_property PACKAGE_PIN Y3 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
# set_property PACKAGE_PIN Y2 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
# set_property PACKAGE_PIN V2 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
# set_property PACKAGE_PIN V1 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** M4

# *** Data capture

# set_property PACKAGE_PIN T18 [get_ports {cati_dataout_p[0]}]
# set_property PACKAGE_PIN T19 [get_ports {cati_dataout_n[0]}]
# set_property PACKAGE_PIN U17 [get_ports {cati_dataout_p[1]}]
# set_property PACKAGE_PIN T17 [get_ports {cati_dataout_n[1]}]

# set_property PACKAGE_PIN AB5 [get_ports {cati_strobe[0]}]
# set_property PACKAGE_PIN AA5 [get_ports {cati_strobe[1]}]

# *** Slow Control

# set_property PACKAGE_PIN C18 [get_ports {sr_in}]
# set_property PACKAGE_PIN F19 [get_ports {sr_out}]

# *** Triggers / discri output

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
# set_property PACKAGE_PIN AF2 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
# set_property PACKAGE_PIN AF3 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
# set_property PACKAGE_PIN AE5 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
# set_property PACKAGE_PIN AE6 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
# set_property PACKAGE_PIN AE2 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
# set_property PACKAGE_PIN AE3 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
# set_property PACKAGE_PIN AF4 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
# set_property PACKAGE_PIN AF5 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
# set_property PACKAGE_PIN AE1 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
# set_property PACKAGE_PIN AD1 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
# set_property PACKAGE_PIN AD3 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
# set_property PACKAGE_PIN AD4 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
# set_property PACKAGE_PIN AD5 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
# set_property PACKAGE_PIN AD6 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
# set_property PACKAGE_PIN AC6 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
# set_property PACKAGE_PIN AB6 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** M36

# *** Data capture

# set_property PACKAGE_PIN R26 [get_ports {cati_dataout_p[0]}]
# set_property PACKAGE_PIN P26 [get_ports {cati_dataout_n[0]}]
# set_property PACKAGE_PIN P24 [get_ports {cati_dataout_p[1]}]
# set_property PACKAGE_PIN N24 [get_ports {cati_dataout_n[1]}]

# set_property PACKAGE_PIN AC4 [get_ports {cati_strobe[0]}]
# set_property PACKAGE_PIN AC3 [get_ports {cati_strobe[1]}]

# *** Slow Control

# set_property PACKAGE_PIN D20 [get_ports {sr_in}]
# set_property PACKAGE_PIN J20 [get_ports {sr_out}]

# *** Triggers / discri output

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
# set_property PACKAGE_PIN AE25 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
# set_property PACKAGE_PIN AD25 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
# set_property PACKAGE_PIN AE26 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
# set_property PACKAGE_PIN AD26 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
# set_property PACKAGE_PIN AF25 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
# set_property PACKAGE_PIN AF24 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
# set_property PACKAGE_PIN AC24 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
# set_property PACKAGE_PIN AC23 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
# set_property PACKAGE_PIN AA24 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
# set_property PACKAGE_PIN AB24 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
# set_property PACKAGE_PIN AA23 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
# set_property PACKAGE_PIN AC26 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
# set_property PACKAGE_PIN AB26 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
# set_property PACKAGE_PIN AA25 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
# set_property PACKAGE_PIN AB25 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
# set_property PACKAGE_PIN Y26 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# # ** M38

# # *** Data capture

#  set_property PACKAGE_PIN R25 [get_ports {cati_dataout_p[0]}]
#  set_property PACKAGE_PIN P25 [get_ports {cati_dataout_n[0]}]
#  set_property PACKAGE_PIN T20 [get_ports {cati_dataout_p[1]}]
#  set_property PACKAGE_PIN R20 [get_ports {cati_dataout_n[1]}]

#  set_property PACKAGE_PIN AB2 [get_ports {cati_strobe[0]}]
#  set_property PACKAGE_PIN AC2 [get_ports {cati_strobe[1]}]

# # *** Slow Control

#  set_property PACKAGE_PIN D19 [get_ports {sr_in}]
#  set_property PACKAGE_PIN K20 [get_ports {sr_out}]

# # *** Triggers / discri output

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
#  set_property PACKAGE_PIN Y20 [get_ports {triggers[0]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
#  set_property PACKAGE_PIN AF22 [get_ports {triggers[1]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
#  set_property PACKAGE_PIN AE22 [get_ports {triggers[2]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
#  set_property PACKAGE_PIN AF23 [get_ports {triggers[3]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
#  set_property PACKAGE_PIN AE23 [get_ports {triggers[4]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
#  set_property PACKAGE_PIN AE21 [get_ports {triggers[5]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
#  set_property PACKAGE_PIN AD21 [get_ports {triggers[6]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
#  set_property PACKAGE_PIN AC21 [get_ports {triggers[7]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
#  set_property PACKAGE_PIN AB21 [get_ports {triggers[8]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
#  set_property PACKAGE_PIN AC22 [get_ports {triggers[9]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
#  set_property PACKAGE_PIN AB22 [get_ports {triggers[10]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
#  set_property PACKAGE_PIN AD24 [get_ports {triggers[11]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
#  set_property PACKAGE_PIN AD23 [get_ports {triggers[12]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
#  set_property PACKAGE_PIN Y21 [get_ports {triggers[13]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
#  set_property PACKAGE_PIN AA22 [get_ports {triggers[14]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
#  set_property PACKAGE_PIN Y22 [get_ports {triggers[15]}]
##  set_property  CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** M1

# *** Data capture

# set_property PACKAGE_PIN P19 [get_ports {cati_dataout_p[0]}]
# set_property PACKAGE_PIN P20 [get_ports {cati_dataout_n[0]}]
# set_property PACKAGE_PIN R18 [get_ports {cati_dataout_p[1]}]
# set_property PACKAGE_PIN P18 [get_ports {cati_dataout_n[1]}]

# set_property PACKAGE_PIN AB1 [get_ports {cati_strobe[0]}]
# set_property PACKAGE_PIN AC1 [get_ports {cati_strobe[1]}]

# *** Slow Control

# set_property PACKAGE_PIN H17 [get_ports {sr_in}]
# set_property PACKAGE_PIN H19 [get_ports {sr_out}]

# *** Triggers / discri output

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
# set_property PACKAGE_PIN A10 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
# set_property PACKAGE_PIN B10 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
# set_property PACKAGE_PIN D10 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
# set_property PACKAGE_PIN E10 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
# set_property PACKAGE_PIN B9 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
# set_property PACKAGE_PIN C9 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
# set_property PACKAGE_PIN A8 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
# set_property PACKAGE_PIN A9 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
# set_property PACKAGE_PIN D8 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
# set_property PACKAGE_PIN D9 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
# set_property PACKAGE_PIN B11 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
# set_property PACKAGE_PIN B12 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
# set_property PACKAGE_PIN C12 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
# set_property PACKAGE_PIN C11 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
# set_property PACKAGE_PIN E11 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
# set_property PACKAGE_PIN D11 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** M3

# *** Data capture

# set_property PACKAGE_PIN P16 [get_ports {cati_dataout_p[0]}]
# set_property PACKAGE_PIN N17 [get_ports {cati_dataout_n[0]}]
# set_property PACKAGE_PIN N18 [get_ports {cati_dataout_p[1]}]
# set_property PACKAGE_PIN M19 [get_ports {cati_dataout_n[1]}]

# set_property PACKAGE_PIN W1 [get_ports {cati_strobe[0]}]
# set_property PACKAGE_PIN T7 [get_ports {cati_strobe[1]}]

# *** Slow Control

# set_property PACKAGE_PIN H18 [get_ports {sr_in}]
# set_property PACKAGE_PIN G20 [get_ports {sr_out}]

# *** Triggers / discri output

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
# set_property PACKAGE_PIN H9 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
# set_property PACKAGE_PIN H8 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
# set_property PACKAGE_PIN G10 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
# set_property PACKAGE_PIN G9 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
# set_property PACKAGE_PIN H11 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
# set_property PACKAGE_PIN F9 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
# set_property PACKAGE_PIN F8 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
# set_property PACKAGE_PIN G11 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
# set_property PACKAGE_PIN J8 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
# set_property PACKAGE_PIN J11 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
# set_property PACKAGE_PIN J10 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
# set_property PACKAGE_PIN H12 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
# set_property PACKAGE_PIN G12 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
# set_property PACKAGE_PIN F12 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
# set_property PACKAGE_PIN E12 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
# set_property PACKAGE_PIN J13 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** M35

# *** Data capture

# set_property PACKAGE_PIN M25 [get_ports {cati_dataout_p[0]}]
# set_property PACKAGE_PIN L25 [get_ports {cati_dataout_n[0]}]
# set_property PACKAGE_PIN N26 [get_ports {cati_dataout_p[1]}]
# set_property PACKAGE_PIN M26 [get_ports {cati_dataout_n[1]}]

# set_property PACKAGE_PIN F13 [get_ports {cati_strobe[0]}]
# set_property PACKAGE_PIN J14 [get_ports {cati_strobe[1]}]

# *** Slow Control

# set_property PACKAGE_PIN G19 [get_ports {sr_in}]
# set_property PACKAGE_PIN J18 [get_ports {sr_out}]

# *** Triggers / discri output

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
# set_property PACKAGE_PIN A12 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
# set_property PACKAGE_PIN A13 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
# set_property PACKAGE_PIN A15 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
# set_property PACKAGE_PIN B15 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
# set_property PACKAGE_PIN A14 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
# set_property PACKAGE_PIN B14 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
# set_property PACKAGE_PIN C13 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
# set_property PACKAGE_PIN C14 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
# set_property PACKAGE_PIN E13 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
# set_property PACKAGE_PIN D13 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
# set_property PACKAGE_PIN D14 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
# set_property PACKAGE_PIN G14 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
# set_property PACKAGE_PIN H14 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
# set_property PACKAGE_PIN H13 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
# set_property PACKAGE_PIN F10 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

# set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
# set_property PACKAGE_PIN F14 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** M37

# *** Data capture

#  set_property PACKAGE_PIN M24 [get_ports {cati_dataout_p[0]}]
#  set_property PACKAGE_PIN L24 [get_ports {cati_dataout_n[0]}]
#  set_property PACKAGE_PIN K25 [get_ports {cati_dataout_p[1]}]
#  set_property PACKAGE_PIN K26 [get_ports {cati_dataout_n[1]}]

#  set_property PACKAGE_PIN Y25 [get_ports {cati_strobe[0]}]
#  set_property PACKAGE_PIN Y23 [get_ports {cati_strobe[1]}]

# *** Slow Control

#  set_property PACKAGE_PIN F20 [get_ports {sr_in}]
#  set_property PACKAGE_PIN J19 [get_ports {sr_out}]

# *** Triggers / discri output

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
#  set_property PACKAGE_PIN U21 [get_ports {triggers[0]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
#  set_property PACKAGE_PIN U22 [get_ports {triggers[1]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
#  set_property PACKAGE_PIN V22 [get_ports {triggers[2]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
#  set_property PACKAGE_PIN U24 [get_ports {triggers[3]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
#  set_property PACKAGE_PIN U25 [get_ports {triggers[4]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
#  set_property PACKAGE_PIN V23 [get_ports {triggers[5]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
#  set_property PACKAGE_PIN V24 [get_ports {triggers[6]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
#  set_property PACKAGE_PIN U26 [get_ports {triggers[7]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
#  set_property PACKAGE_PIN V26 [get_ports {triggers[8]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
#  set_property PACKAGE_PIN W25 [get_ports {triggers[9]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
#  set_property PACKAGE_PIN W26 [get_ports {triggers[10]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
#  set_property PACKAGE_PIN V21 [get_ports {triggers[11]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
#  set_property PACKAGE_PIN W21 [get_ports {triggers[12]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
#  set_property PACKAGE_PIN W23 [get_ports {triggers[13]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
#  set_property PACKAGE_PIN W24 [get_ports {triggers[14]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

#  set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
#  set_property PACKAGE_PIN W20 [get_ports {triggers[15]}]
#  set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# ** All

# To be used with all 8 asics

# *** M2

# **** Data capture

set_property PACKAGE_PIN R16 [get_ports {cati_dataout_p[0]}]
set_property PACKAGE_PIN R17 [get_ports {cati_dataout_n[0]}]
set_property PACKAGE_PIN U19 [get_ports {cati_dataout_p[1]}]
set_property PACKAGE_PIN U20 [get_ports {cati_dataout_n[1]}]

set_property PACKAGE_PIN U4 [get_ports {cati_strobe[0]}]
set_property PACKAGE_PIN Y1 [get_ports {cati_strobe[1]}]

# **** Slow Control

set_property PACKAGE_PIN D18 [get_ports {sr_in[0]}]
set_property PACKAGE_PIN E20 [get_ports {sr_out[0]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[0]]
set_property PACKAGE_PIN U6 [get_ports {triggers[0]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[0]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[1]]
set_property PACKAGE_PIN U5 [get_ports {triggers[1]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[1]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[2]]
set_property PACKAGE_PIN U2 [get_ports {triggers[2]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[2]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[3]]
set_property PACKAGE_PIN U1 [get_ports {triggers[3]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[3]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[4]]
set_property PACKAGE_PIN W6 [get_ports {triggers[4]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[4]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[5]]
set_property PACKAGE_PIN W5 [get_ports {triggers[5]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[5]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[6]]
set_property PACKAGE_PIN V3 [get_ports {triggers[6]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[6]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[7]]
set_property PACKAGE_PIN W3 [get_ports {triggers[7]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[7]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[8]]
set_property PACKAGE_PIN U7 [get_ports {triggers[8]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[8]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[9]]
set_property PACKAGE_PIN V6 [get_ports {triggers[9]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[9]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[10]]
set_property PACKAGE_PIN V4 [get_ports {triggers[10]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[10]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[11]]
set_property PACKAGE_PIN W4 [get_ports {triggers[11]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[11]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[12]]
set_property PACKAGE_PIN Y3 [get_ports {triggers[12]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[12]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[13]]
set_property PACKAGE_PIN Y2 [get_ports {triggers[13]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[13]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[14]]
set_property PACKAGE_PIN V2 [get_ports {triggers[14]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[14]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[15]]
set_property PACKAGE_PIN V1 [get_ports {triggers[15]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[15]]

# *** M4

# **** Data capture

set_property PACKAGE_PIN T18 [get_ports {cati_dataout_p[2]}]
set_property PACKAGE_PIN T19 [get_ports {cati_dataout_n[2]}]
set_property PACKAGE_PIN U17 [get_ports {cati_dataout_p[3]}]
set_property PACKAGE_PIN T17 [get_ports {cati_dataout_n[3]}]

set_property PACKAGE_PIN AB5 [get_ports {cati_strobe[2]}]
set_property PACKAGE_PIN AA5 [get_ports {cati_strobe[3]}]

# **** Slow Control

set_property PACKAGE_PIN C18 [get_ports {sr_in[1]}]
set_property PACKAGE_PIN F19 [get_ports {sr_out[1]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[16]]
set_property PACKAGE_PIN AF2 [get_ports {triggers[16]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[16]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[17]]
set_property PACKAGE_PIN AF3 [get_ports {triggers[17]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[17]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[18]]
set_property PACKAGE_PIN AE5 [get_ports {triggers[18]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[18]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[19]]
set_property PACKAGE_PIN AE6 [get_ports {triggers[19]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[19]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[20]]
set_property PACKAGE_PIN AE2 [get_ports {triggers[20]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[20]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[21]]
set_property PACKAGE_PIN AE3 [get_ports {triggers[21]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[21]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[22]]
set_property PACKAGE_PIN AF4 [get_ports {triggers[22]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[22]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[23]]
set_property PACKAGE_PIN AF5 [get_ports {triggers[23]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[23]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[24]]
set_property PACKAGE_PIN AE1 [get_ports {triggers[24]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[24]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[25]]
set_property PACKAGE_PIN AD1 [get_ports {triggers[25]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[25]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[26]]
set_property PACKAGE_PIN AD3 [get_ports {triggers[26]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[26]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[27]]
set_property PACKAGE_PIN AD4 [get_ports {triggers[27]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[27]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[28]]
set_property PACKAGE_PIN AD5 [get_ports {triggers[28]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[28]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[29]]
set_property PACKAGE_PIN AD6 [get_ports {triggers[29]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[29]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[30]]
set_property PACKAGE_PIN AC6 [get_ports {triggers[30]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[30]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[31]]
set_property PACKAGE_PIN AB6 [get_ports {triggers[31]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[31]]

# *** M1

# **** Data capture

set_property PACKAGE_PIN P19 [get_ports {cati_dataout_p[4]}]
set_property PACKAGE_PIN P20 [get_ports {cati_dataout_n[4]}]
set_property PACKAGE_PIN R18 [get_ports {cati_dataout_p[5]}]
set_property PACKAGE_PIN P18 [get_ports {cati_dataout_n[5]}]

set_property PACKAGE_PIN AB1 [get_ports {cati_strobe[4]}]
set_property PACKAGE_PIN AC1 [get_ports {cati_strobe[5]}]

# **** Slow Control

set_property PACKAGE_PIN H17 [get_ports {sr_in[2]}]
set_property PACKAGE_PIN H19 [get_ports {sr_out[2]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[32]]
set_property PACKAGE_PIN A10 [get_ports {triggers[32]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[32]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[33]]
set_property PACKAGE_PIN B10 [get_ports {triggers[33]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[33]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[34]]
set_property PACKAGE_PIN D10 [get_ports {triggers[34]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[34]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[35]]
set_property PACKAGE_PIN E10 [get_ports {triggers[35]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[35]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[36]]
set_property PACKAGE_PIN B9 [get_ports {triggers[36]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[36]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[37]]
set_property PACKAGE_PIN C9 [get_ports {triggers[37]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[37]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[38]]
set_property PACKAGE_PIN A8 [get_ports {triggers[38]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[38]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[39]]
set_property PACKAGE_PIN A9 [get_ports {triggers[39]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[39]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[40]]
set_property PACKAGE_PIN D8 [get_ports {triggers[40]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[40]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[41]]
set_property PACKAGE_PIN D9 [get_ports {triggers[41]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[41]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[42]]
set_property PACKAGE_PIN B11 [get_ports {triggers[42]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[42]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[43]]
set_property PACKAGE_PIN B12 [get_ports {triggers[43]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[43]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[44]]
set_property PACKAGE_PIN C12 [get_ports {triggers[44]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[44]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[45]]
set_property PACKAGE_PIN C11 [get_ports {triggers[45]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[45]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[46]]
set_property PACKAGE_PIN E11 [get_ports {triggers[46]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[46]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[47]]
set_property PACKAGE_PIN D11 [get_ports {triggers[47]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[47]]

# *** M3

# **** Data capture

set_property PACKAGE_PIN P16 [get_ports {cati_dataout_p[6]}]
set_property PACKAGE_PIN N17 [get_ports {cati_dataout_n[6]}]
set_property PACKAGE_PIN N18 [get_ports {cati_dataout_p[7]}]
set_property PACKAGE_PIN M19 [get_ports {cati_dataout_n[7]}]

set_property PACKAGE_PIN W1 [get_ports {cati_strobe[6]}]
set_property PACKAGE_PIN T7 [get_ports {cati_strobe[7]}]

# **** Slow Control

set_property PACKAGE_PIN H18 [get_ports {sr_in[3]}]
set_property PACKAGE_PIN G20 [get_ports {sr_out[3]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[48]]
set_property PACKAGE_PIN H9 [get_ports {triggers[48]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[48]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[49]]
set_property PACKAGE_PIN H8 [get_ports {triggers[49]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[49]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[50]]
set_property PACKAGE_PIN G10 [get_ports {triggers[50]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[50]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[51]]
set_property PACKAGE_PIN G9 [get_ports {triggers[51]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[51]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[52]]
set_property PACKAGE_PIN H11 [get_ports {triggers[52]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[52]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[53]]
set_property PACKAGE_PIN F9 [get_ports {triggers[53]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[53]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[54]]
set_property PACKAGE_PIN F8 [get_ports {triggers[54]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[54]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[55]]
set_property PACKAGE_PIN G11 [get_ports {triggers[55]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[55]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[56]]
set_property PACKAGE_PIN J8 [get_ports {triggers[56]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[56]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[57]]
set_property PACKAGE_PIN J11 [get_ports {triggers[57]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[57]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[58]]
set_property PACKAGE_PIN J10 [get_ports {triggers[58]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[58]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[59]]
set_property PACKAGE_PIN H12 [get_ports {triggers[59]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[59]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[60]]
set_property PACKAGE_PIN G12 [get_ports {triggers[60]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[60]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[61]]
set_property PACKAGE_PIN F12 [get_ports {triggers[61]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[61]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[62]]
set_property PACKAGE_PIN E12 [get_ports {triggers[62]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[62]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[63]]
set_property PACKAGE_PIN J13 [get_ports {triggers[63]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[63]]

# *** M36

# **** Data capture

set_property PACKAGE_PIN R26 [get_ports {cati_dataout_p[8]}]
set_property PACKAGE_PIN P26 [get_ports {cati_dataout_n[8]}]
set_property PACKAGE_PIN P24 [get_ports {cati_dataout_p[9]}]
set_property PACKAGE_PIN N24 [get_ports {cati_dataout_n[9]}]

set_property PACKAGE_PIN AC4 [get_ports {cati_strobe[8]}]
set_property PACKAGE_PIN AC3 [get_ports {cati_strobe[9]}]

# **** Slow Control

set_property PACKAGE_PIN D20 [get_ports {sr_in[4]}]
set_property PACKAGE_PIN J20 [get_ports {sr_out[4]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[64]]
set_property PACKAGE_PIN AE25 [get_ports {triggers[64]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[64]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[65]]
set_property PACKAGE_PIN AD25 [get_ports {triggers[65]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[65]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[66]]
set_property PACKAGE_PIN AE26 [get_ports {triggers[66]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[66]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[67]]
set_property PACKAGE_PIN AD26 [get_ports {triggers[67]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[67]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[68]]
set_property PACKAGE_PIN AF25 [get_ports {triggers[68]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[68]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[69]]
set_property PACKAGE_PIN AF24 [get_ports {triggers[69]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[69]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[70]]
set_property PACKAGE_PIN AC24 [get_ports {triggers[70]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[70]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[71]]
set_property PACKAGE_PIN AC23 [get_ports {triggers[71]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[71]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[72]]
set_property PACKAGE_PIN AA24 [get_ports {triggers[72]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[72]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[73]]
set_property PACKAGE_PIN AB24 [get_ports {triggers[73]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[73]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[74]]
set_property PACKAGE_PIN AA23 [get_ports {triggers[74]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[74]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[75]]
set_property PACKAGE_PIN AC26 [get_ports {triggers[75]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[75]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[76]]
set_property PACKAGE_PIN AB26 [get_ports {triggers[76]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[76]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[77]]
set_property PACKAGE_PIN AA25 [get_ports {triggers[77]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[77]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[78]]
set_property PACKAGE_PIN AB25 [get_ports {triggers[78]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[78]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[79]]
set_property PACKAGE_PIN Y26 [get_ports {triggers[79]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[79]]

# *** M38

# **** Data capture

set_property PACKAGE_PIN R25 [get_ports {cati_dataout_p[10]}]
set_property PACKAGE_PIN P25 [get_ports {cati_dataout_n[10]}]
set_property PACKAGE_PIN T20 [get_ports {cati_dataout_p[11]}]
set_property PACKAGE_PIN R20 [get_ports {cati_dataout_n[11]}]

set_property PACKAGE_PIN AB2 [get_ports {cati_strobe[10]}]
set_property PACKAGE_PIN AC2 [get_ports {cati_strobe[11]}]

# **** Slow Control

set_property PACKAGE_PIN D19 [get_ports {sr_in[5]}]
set_property PACKAGE_PIN K20 [get_ports {sr_out[5]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[80]]
set_property PACKAGE_PIN Y20 [get_ports {triggers[80]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[80]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[81]]
set_property PACKAGE_PIN AF22 [get_ports {triggers[81]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[81]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[82]]
set_property PACKAGE_PIN AE22 [get_ports {triggers[82]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[82]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[83]]
set_property PACKAGE_PIN AF23 [get_ports {triggers[83]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[83]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[84]]
set_property PACKAGE_PIN AE23 [get_ports {triggers[84]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[84]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[85]]
set_property PACKAGE_PIN AE21 [get_ports {triggers[85]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[85]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[86]]
set_property PACKAGE_PIN AD21 [get_ports {triggers[86]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[86]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[87]]
set_property PACKAGE_PIN AC21 [get_ports {triggers[87]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[87]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[88]]
set_property PACKAGE_PIN AB21 [get_ports {triggers[88]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[88]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[89]]
set_property PACKAGE_PIN AC22 [get_ports {triggers[89]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[89]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[90]]
set_property PACKAGE_PIN AB22 [get_ports {triggers[90]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[90]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[91]]
set_property PACKAGE_PIN AD24 [get_ports {triggers[91]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[91]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[92]]
set_property PACKAGE_PIN AD23 [get_ports {triggers[92]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[92]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[93]]
set_property PACKAGE_PIN Y21 [get_ports {triggers[93]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[93]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[94]]
set_property PACKAGE_PIN AA22 [get_ports {triggers[94]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[94]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[95]]
set_property PACKAGE_PIN Y22 [get_ports {triggers[95]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[95]]

# *** M35

# **** Data capture

set_property PACKAGE_PIN M25 [get_ports {cati_dataout_p[12]}]
set_property PACKAGE_PIN L25 [get_ports {cati_dataout_n[12]}]
set_property PACKAGE_PIN N26 [get_ports {cati_dataout_p[13]}]
set_property PACKAGE_PIN M26 [get_ports {cati_dataout_n[13]}]

set_property PACKAGE_PIN F13 [get_ports {cati_strobe[12]}]
set_property PACKAGE_PIN J14 [get_ports {cati_strobe[13]}]

# **** Slow Control

set_property PACKAGE_PIN G19 [get_ports {sr_in[6]}]
set_property PACKAGE_PIN J18 [get_ports {sr_out[6]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[96]]
set_property PACKAGE_PIN A12 [get_ports {triggers[96]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[96]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[97]]
set_property PACKAGE_PIN A13 [get_ports {triggers[97]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[97]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[98]]
set_property PACKAGE_PIN A15 [get_ports {triggers[98]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[98]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[99]]
set_property PACKAGE_PIN B15 [get_ports {triggers[99]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[99]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[100]]
set_property PACKAGE_PIN A14 [get_ports {triggers[100]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[100]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[101]]
set_property PACKAGE_PIN B14 [get_ports {triggers[101]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[101]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[102]]
set_property PACKAGE_PIN C13 [get_ports {triggers[102]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[102]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[103]]
set_property PACKAGE_PIN C14 [get_ports {triggers[103]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[103]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[104]]
set_property PACKAGE_PIN E13 [get_ports {triggers[104]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[104]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[105]]
set_property PACKAGE_PIN D13 [get_ports {triggers[105]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[105]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[106]]
set_property PACKAGE_PIN D14 [get_ports {triggers[106]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[106]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[107]]
set_property PACKAGE_PIN G14 [get_ports {triggers[107]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[107]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[108]]
set_property PACKAGE_PIN H14 [get_ports {triggers[108]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[108]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[109]]
set_property PACKAGE_PIN H13 [get_ports {triggers[109]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[109]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[110]]
set_property PACKAGE_PIN F10 [get_ports {triggers[110]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[110]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[111]]
set_property PACKAGE_PIN F14 [get_ports {triggers[111]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[111]]

# *** M37

# **** Data capture

set_property PACKAGE_PIN M24 [get_ports {cati_dataout_p[14]}]
set_property PACKAGE_PIN L24 [get_ports {cati_dataout_n[14]}]
set_property PACKAGE_PIN K25 [get_ports {cati_dataout_p[15]}]
set_property PACKAGE_PIN K26 [get_ports {cati_dataout_n[15]}]

set_property PACKAGE_PIN Y25 [get_ports {cati_strobe[14]}]
set_property PACKAGE_PIN Y23 [get_ports {cati_strobe[15]}]

# **** Slow Control

set_property PACKAGE_PIN F20 [get_ports {sr_in[7]}]
set_property PACKAGE_PIN J19 [get_ports {sr_out[7]}]

# **** Triggers / discri output

set_property IOSTANDARD LVCMOS18 [get_ports triggers[112]]
set_property PACKAGE_PIN U21 [get_ports {triggers[112]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[112]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[113]]
set_property PACKAGE_PIN U22 [get_ports {triggers[113]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[113]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[114]]
set_property PACKAGE_PIN V22 [get_ports {triggers[114]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[114]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[115]]
set_property PACKAGE_PIN U24 [get_ports {triggers[115]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[115]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[116]]
set_property PACKAGE_PIN U25 [get_ports {triggers[116]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[116]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[117]]
set_property PACKAGE_PIN V23 [get_ports {triggers[117]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[117]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[118]]
set_property PACKAGE_PIN V24 [get_ports {triggers[118]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[118]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[119]]
set_property PACKAGE_PIN U26 [get_ports {triggers[119]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[119]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[120]]
set_property PACKAGE_PIN V26 [get_ports {triggers[120]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[120]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[121]]
set_property PACKAGE_PIN W25 [get_ports {triggers[121]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[121]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[122]]
set_property PACKAGE_PIN W26 [get_ports {triggers[122]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[122]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[123]]
set_property PACKAGE_PIN V21 [get_ports {triggers[123]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[123]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[124]]
set_property PACKAGE_PIN W21 [get_ports {triggers[124]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[124]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[125]]
set_property PACKAGE_PIN W23 [get_ports {triggers[125]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[125]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[126]]
set_property PACKAGE_PIN W24 [get_ports {triggers[126]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[126]]

set_property IOSTANDARD LVCMOS18 [get_ports triggers[127]]
set_property PACKAGE_PIN W20 [get_ports {triggers[127]}]
# set_property CLOCK_DEDICATED_ROUTE FALSE [get_nets triggers_IBUF[127]]
