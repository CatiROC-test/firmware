-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Header][Header:1]]
-- * Header

-- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
-- @@@ Header:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Libraries][Libraries:1]]
-- * Libraries

library IEEE;
-- @@@ Libraries:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

library UNISIM;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Packages][Packages:1]]
-- * Packages

use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use work.common_package.all;
-- @@@ Packages:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Juno%20mezzamine%20code][Juno mezzamine code:1]]
-- ** Juno mezzamine code

use work.juno_mezzanine_package.all;
use UNISIM.VComponents.all;
-- @@@ Juno mezzamine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Entity][Entity:1]]
-- * Entity

entity catiroc_slowcontrol is
   port (clk          : in  std_logic;  -- generic oscilator present on board
         main_rst     : in  std_logic;
         -- cati
         resetb       : out std_logic;
         start_system : out std_logic;
         sr_clk       : out std_logic;
         sr_in        : out std_logic;  -- serialized data to configure cati
         sr_rst       : out std_logic;  -- reset cati before sending configuration
         sr_select    : out std_logic;  -- to be able to write in cati
         sr_out       : in  std_logic;  -- serialized data to read back conf sen
         -- usb
         clk_2232     : in  std_logic;  -- usb clock, 60 MHz
         p3_params    : in  std_logic_vector(527 downto 0);
         usb_data_out : out std_logic_vector(7 downto 0);  -- usb Output bus (Send data to usb)
         usb_fifo1_rd : in  std_logic;  -- USB demande lecture de la fifo
         enable       : in  std_logic;  --PC demande l'ordre decriture et de lecture dans la fifo
         fifo_empty   : out std_logic);
end entity catiroc_slowcontrol;
-- @@@ Entity:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Architecture][Architecture:1]]
-- * Architecture

architecture Behavioral of catiroc_slowcontrol is
-- @@@ Architecture:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Signals][Signals:1]]
-- ** Signals

   signal fifo_data_in                 : std_logic_vector (7 downto 0)   := (others => '0');
   signal reset_catiCount              : std_logic_vector (3 downto 0)   := (others => '0');
   signal Slow_ControlCount            : unsigned (11 downto 0)          := (others => '0');
   signal Slow_Control_reg             : std_logic_vector (327 downto 0) := (others => '0');
   signal Probe_reg                    : std_logic_vector (193 downto 0) := (others => '0');
   signal start_readback_SC            : std_logic                       := '0';
   signal start_readback_PB            : std_logic                       := '0';
   signal readbackcount                : std_logic_vector(3 downto 0)    := (others => '0');
   signal write_sl_state               : slow_control_write_state;
   signal fifo_wr                      : std_logic                       := '0';
   signal active_srclk                 : std_logic                       := '0';
   signal probeRead_count              : unsigned (7 downto 0)           := (others => '0');
   signal fifo_clear, not_active_srclk : std_logic                       := '0';
-- signal wrfull            : std_logic                       := '0';

   signal sresetb : std_logic := '0';
   signal ssr_rst : std_logic := '0';

   signal gnd        : std_logic_vector (0 downto 0) := (others => '0');
   signal vcc        : std_logic_vector (0 downto 0) := (others => '1');
   signal sr_clk_tmp : std_logic_vector (0 downto 0) := (others => '0');
-- @@@ Signals:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*begin][begin:1]]
-- ** begin

begin
-- @@@ begin:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Generic][Generic:1]]
-- ** Generic

   gnd(0) <= '0';
   vcc(0) <= '1';
-- @@@ Generic:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*SR%20to%20ASIC][SR to ASIC:1]]
-- ** SR to ASIC

   process (clk)
   begin

      if (clk'event and clk = '0') then

         if (main_rst = '1' or enable = '0') then

            resetb            <= '1';
            sr_in             <= '0';
            sr_rst            <= '1';
            sr_select         <= '0';
            reset_catiCount   <= (others => '0');
            Slow_Control_reg  <= (others => '0');
            Probe_reg         <= (others => '0');
            Slow_ControlCount <= (others => '0');
            start_readback_SC <= '0';
            start_readback_PB <= '0';
            start_system      <= '1';
            active_srclk      <= '0';
            write_sl_state    <= write_sl_standby;

         else

            case write_sl_state is

               when write_sl_standby =>
                  start_system      <= '1';
                  sr_in             <= '0';
                  reset_catiCount   <= (others => '0');
                  Slow_ControlCount <= (others => '0');
                  start_readback_SC <= '0';
                  start_readback_PB <= '0';
                  active_srclk      <= '0';

                  Slow_Control_reg <= p3_params(527 downto 200);
                  Probe_reg        <= p3_params(199 downto 6);
                  resetb           <= '1';
                  sr_rst           <= '1';
                  sr_select        <= '1';
                  write_sl_state   <= write_sl_resetb;

               when write_sl_resetb =>

                  if(reset_catiCount = X"F") then
                     resetb          <= '1';
                     reset_catiCount <= (others => '0');
                     write_sl_state  <= write_sl_reset;
                  else
                     reset_catiCount <= std_logic_vector(unsigned(reset_catiCount) + 1);
                     resetb          <= '0';
                     write_sl_state  <= write_sl_resetb;
                  end if;

               when write_sl_reset =>

                  if(reset_catiCount = X"F") then
                     sr_rst          <= '1';
                     reset_catiCount <= (others => '0');
                     write_sl_state  <= write_sl;
                  else
                     reset_catiCount <= std_logic_vector(unsigned(reset_catiCount) + 1);
                     sr_rst          <= '0';
                     write_sl_state  <= write_sl_reset;
                  end if;


               when write_sl =>

                  if(Slow_ControlCount < 656) then  --656 328x2 (2x SlControl + 8bit) -- ca marche avec 664 (656 + 8bit)
                     active_srclk      <= '1';
                     sr_in             <= Slow_Control_reg(327);
                     Slow_Control_reg  <= Slow_Control_reg(326 downto 0) & Slow_Control_reg(327);
                     Slow_ControlCount <= Slow_ControlCount + 1;
                     if (Slow_ControlCount < 328) then  ---1er bit de relecture on a remplis les 328 premiers
                        start_readback_SC <= '0';
                        write_sl_state    <= write_sl;
                     else
                        start_readback_SC <= '1';
                        write_sl_state    <= write_sl;
                     end if;

                  else

                     active_srclk      <= '0';
                     Slow_ControlCount <= (others => '0');
                     write_sl_state    <= write_sl_tempo_1;

                  end if;

               when write_sl_tempo_1 =>

                  start_readback_SC <= '0';
                  sr_select         <= '0';
                  write_sl_state    <= write_sl_tempo_2;

               when write_sl_tempo_2 =>

                  write_sl_state <= write_sl_probe;

               when write_sl_probe =>

                  if(Slow_ControlCount < 388) then  --388 194x2 (2x PorbeReg)
                     active_srclk      <= '1';
                     sr_in             <= Probe_reg(193);
                     Probe_reg         <= Probe_reg(192 downto 0) & Probe_reg(193);
                     Slow_ControlCount <= Slow_ControlCount + 1;
                                        --      start_readback_PB <= '1';
                     if (Slow_ControlCount < 194) then  ---1er bit de relecture on a remplis les 194 premiers
                        start_readback_PB <= '0';
                        write_sl_state    <= write_sl_probe;
                     else
                        start_readback_PB <= '1';
                        write_sl_state    <= write_sl_probe;
                     end if;
                  else
                     active_srclk      <= '0';
                     Slow_ControlCount <= (others => '0');
                     write_sl_state    <= write_sl_run;
                  end if;

               when write_sl_run =>

                  start_readback_PB <= '0';
                  start_system      <= '1';
                  write_sl_state    <= write_sl_run;

            end case;

         end if;

      end if;

   end process;
-- @@@ SR to ASIC:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*SR%20Readout][SR Readout:1]]
-- ** SR Readout

   process (clk)
   begin

      if (clk'event and clk = '1') then

         if (main_rst = '1' or enable = '0') then

            fifo_data_in    <= (others => '0');
            readbackcount   <= (others => '0');
            probeRead_count <= (others => '0');
            fifo_wr         <= '0';
            fifo_clear      <= '1';

         else

            fifo_clear <= '0';

            if (start_readback_SC = '1') then

               fifo_data_in <= fifo_data_in(6 downto 0) & sr_out;
               if (readbackcount = X"7") then
                  fifo_wr       <= '1';
                  readbackcount <= (others => '0');
               else
                  fifo_wr       <= '0';
                  readbackcount <= std_logic_vector(unsigned(readbackcount) + 1);
               end if;

            elsif (start_readback_PB = '1') then

               if (probeRead_count < 192) then

                  fifo_data_in    <= fifo_data_in(6 downto 0) & sr_out;
                  probeRead_count <= probeRead_count + 1;
                  if (readbackcount = X"7") then
                     fifo_wr       <= '1';
                     readbackcount <= (others => '0');
                  else
                     fifo_wr       <= '0';
                     readbackcount <= std_logic_vector(unsigned(readbackcount) + 1);
                  end if;

               else

                  if (readbackcount = X"1") then
                     fifo_data_in  <= fifo_data_in(0) & sr_out & B"000000";
                     fifo_wr       <= '1';
                     readbackcount <= (others => '0');
                  else
                     fifo_data_in  <= fifo_data_in(6 downto 0) & sr_out;
                     fifo_wr       <= '0';
                     readbackcount <= std_logic_vector(unsigned(readbackcount) + 1);
                  end if;

               end if;

            else

               fifo_data_in    <= (others => '0');
               fifo_wr         <= '0';
               readbackcount   <= (others => '0');
               probeRead_count <= (others => '0');

            end if;

         end if;

      end if;

   end process;
-- @@@ SR Readout:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Juno%20mezzanine%20code][Juno mezzanine code:1]]
-- *** Juno mezzanine code

   U_oddr : ODDR
      generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE",
                  INIT         => '0',
                  SRTYPE       => "SYNC")
      port map (Q  => sr_clk,
                C  => clk,
                CE => '1',
                D1 => '1',
                D2 => '0',
                R  => not_active_srclk,
                S  => '0');

   not_active_srclk <= not(active_srclk);
-- @@@ Juno mezzanine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Fifo][Fifo:1]]
-- ** Fifo
-- @@@ Fifo:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*Juno%20mezzanine%20code][Juno mezzanine code:1]]
-- *** Juno mezzanine code

   Cati_output_SC_inst : Cati_output_fifoSC
      port map (wr_clk => clk,
                rd_clk => clk_2232,
                rst    => fifo_clear,
                din    => fifo_data_in,
                wr_en  => fifo_wr,
                rd_en  => usb_fifo1_rd,
                dout   => usb_data_out,
                full   => open,
                empty  => fifo_empty);
-- @@@ Juno mezzanine code:1 ends here

-- @@@ [[file:~/Projects/apc/juno/CatiROC-test-firmware/src-vorg/catiroc_slowcontrol.org::*end][end:1]]
-- ** end

end architecture Behavioral;
-- @@@ end:1 ends here
