-------------------------------------------------------------------------------
-- Title      : Testbench for design "top"
-- Project    :
-------------------------------------------------------------------------------
-- File       : top_tb.vhd
-- Author     :   <csantos@hp-home>
-- Company    :
-- Created    : 2019-03-25
-- Last update: 2019-03-25
-- Platform   :
-- Standard   : VHDL'08
-------------------------------------------------------------------------------
-- Description:
-------------------------------------------------------------------------------
-- Copyright (c) 2019
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-03-25  1.0      csantos Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity top_tb is

end entity top_tb;

-------------------------------------------------------------------------------

architecture tb of top_tb is

  -- component generics
  constant RegBankLength          : natural               := 70;
  constant nb_asics               : natural range 1 to 8  := 8;
  constant num_of_discri_channels : natural range 1 to 16 := 16;
  constant freq_source            : natural range 1 to 42 := 40;
  constant p1_data_width          : natural range 1 to 16 := 16;
  constant p2_data_width          : natural range 1 to 16 := 8;
  constant p3_data_width          : natural range 1 to 8  := 1;
  constant p4_data_width          : natural range 1 to 16 := 16;
  constant p5_data_width          : natural range 1 to 16 := 1;
  constant p6_data_width          : natural range 1 to 16 := 16;
  constant p1_params_width        : natural range 1 to 4  := 4;
  constant p2_params_width        : natural range 1 to 4  := 4;
  constant p3_params_width        : natural range 1 to 66 := 66;
  constant p4_params_width        : natural range 1 to 8  := 2;
  constant p5_params_width        : natural range 1 to 8  := 3;
  constant p6_params_width        : natural range 1 to 8  := 4;

  -- component ports
  signal usb_rxf          : std_logic;
  signal usb_txe          : std_logic;
  signal clk_2232         : std_logic;
  signal usb_oe           : std_logic;
  signal usb_rd           : std_logic;
  signal usb_wr           : std_logic;
  signal usb_data         : std_logic_vector(7 downto 0);
  signal cati_dataout_p   : std_logic_vector (2*nb_asics-1 downto 0);
  signal cati_dataout_n   : std_logic_vector (2*nb_asics-1 downto 0);
  signal val_evt_p        : std_logic;
  signal val_evt_n        : std_logic;
  signal clk_40MHz_out_p  : std_logic;
  signal clk_40MHz_out_n  : std_logic;
  signal clk_160MHz_out_p : std_logic;
  signal clk_160MHz_out_n : std_logic;
  signal PX1              : std_logic;
  signal PX2              : std_logic;
  signal PX3              : std_logic;
  signal PX4              : std_logic;
  signal pwr_on           : std_logic;
  signal resetb           : std_logic;
  signal StartSys         : std_logic;
  signal sr_clk           : std_logic;
  signal sr_in            : std_logic_vector(nb_asics-1 downto 0);
  signal sr_rst           : std_logic;
  signal sr_select        : std_logic;
  signal sr_out           : std_logic_vector(nb_asics-1 downto 0);
  signal triggers         : std_logic_vector (16*nb_asics-1 downto 0);
  signal trig_ext         : std_logic;
  signal ovf              : std_logic;
  signal cati_strobe      : std_logic_vector (2*nb_asics-1 downto 0);
  signal clk_fpga         : std_logic;
  -- clock
  signal clk_fpga         : std_logic := '1';
  signal clk_40MHz        : std_logic := '0';  -- 40 MHz out of the FPGA sent to CatiROC
  signal clk_160MHz       : std_logic := '0';  -- 160 MHz out of the FPGA sent to CatiROC
  signal clk_80MHz        : std_logic := '0';  -- 80 MHz, internal to CatiROC

begin  -- architecture tb

  -- component instantiation
  DUT : entity work.top
    generic map (
      RegBankLength          => RegBankLength,
      nb_asics               => nb_asics,
      num_of_discri_channels => num_of_discri_channels,
      freq_source            => freq_source,
      p1_data_width          => p1_data_width,
      p2_data_width          => p2_data_width,
      p3_data_width          => p3_data_width,
      p4_data_width          => p4_data_width,
      p5_data_width          => p5_data_width,
      p6_data_width          => p6_data_width,
      p1_params_width        => p1_params_width,
      p2_params_width        => p2_params_width,
      p3_params_width        => p3_params_width,
      p4_params_width        => p4_params_width,
      p5_params_width        => p5_params_width,
      p6_params_width        => p6_params_width)
    port map (
      clk_fpga         => clk_fpga
      -- USB
      usb_rxf          => usb_rxf,
      usb_txe          => usb_txe,
      clk_2232         => clk_2232,
      usb_oe           => usb_oe,
      usb_rd           => usb_rd,
      usb_wr           => usb_wr,
      usb_data         => usb_data,
      -- CatoROC
      pwr_on           => pwr_on,
      resetb           => resetb,
      StartSys         => StartSys,
      triggers         => triggers,
      trig_ext         => trig_ext,
      -- CatiROC data capture
      cati_dataout_p   => cati_dataout_p,
      cati_dataout_n   => cati_dataout_n,
      ovf              => ovf,
      cati_strobe      => cati_strobe,
      val_evt_p        => val_evt_p,
      val_evt_n        => val_evt_n,
      clk_40MHz_out_p  => clk_40MHz_out_p,
      clk_40MHz_out_n  => clk_40MHz_out_n,
      clk_160MHz_out_p => clk_160MHz_out_p,
      clk_160MHz_out_n => clk_160MHz_out_n,
      -- CatiROC slow control
      sr_clk           => sr_clk,
      sr_in            => sr_in,
      sr_rst           => sr_rst,
      sr_select        => sr_select,
      sr_out           => sr_out,
      -- Inspection
      PX1              => PX1,
      PX2              => PX2,
      PX3              => PX3,
      PX4              => PX4);

  -- clock generation
  clk_fpga <= not clk_fpga after 12.5 ns;
  clk_2232 <= not clk_2232 after 16.6 ns;

-- * Usb emulator process

-- dfslkj

  U_usb : process
  begin
    usb_rxf  <= '1';
    usb_txe  <= '1';
    usb_data <= (others => 'Z');
    wait until rising_edge(clk_2232);
  end process U_usb;

-- * CatiROC data capture emulator process

-- dfslkj

  U_usb : process
  begin

    wait until rising_edge(clk_fpga);
  end process U_usb;

-- * CatiROC slow control emulator process

-- dfslkj

  U_IBUFDS_clk_40 : IBUFDS
    port map (O  => clk_40MHz,
              I  => clk_40MHz_out_p,
              IB => clk_40MHz_out_n);

  U_IBUFDS_clk_160 : IBUFDS
    port map (O  => clk_160MHz,
              I  => clk_160MHz_out_p,
              IB => clk_160MHz_out_n);

  U_clk_80 : process (clk_160MHz) is
  begin
    if rising_edge(clk_160MHz) then
      clk_80MHz <= not clk_80MHZ;
    end if;
  end process U_clk_80;

  U_usb : process
  begin
    ovf    <= '0';
    sr_out <= sr_in after 20 ns;
    wait until rising_edge(clk_fpga);
  end process U_usb;

-- * Inspection lines

  PX1 <= '0';
  PX2 <= '0';
  PX3 <= '0';
  PX4 <= '0';

end architecture tb;

-------------------------------------------------------------------------------

configuration top_tb_tb_cfg of top_tb is
  for tb
  end for;
end top_tb_tb_cfg;

-------------------------------------------------------------------------------
