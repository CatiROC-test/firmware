#!/bin/sh

# * Containing project with support files

SUPPORT_VIVADO=$HOME/Projects/perso/eda-common/xilinx-vivado

# create gtags.files with al vhd files in project
find ./src/ -type f \( -iname \*.vhd -o -iname \*.ucf \) -print > gtags.files
find ./ip/ -type f \( -iname \*netlist.vhd -o -iname \*netlist.vhdl \) -print >> gtags.files

# * create new link to gitignore

[[ -e .gitignore ]] && rm .gitignore
ln -s $SUPPORT_VIVADO/gitignore .gitignore

echo "Done."
