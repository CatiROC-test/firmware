# implement
launch_runs impl_1 -jobs 40
wait_on_run impl_1

# set property
open_run impl_1
set_property STEPS.WRITE_BITSTREAM.TCL.PRE {../../../pre-bit.tcl} [get_runs impl_1]

# bitstream
launch_runs impl_1 -to_step write_bitstream -jobs 40
