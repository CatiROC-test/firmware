# open project
open_project vivado18.2-project/vivado18.2-project.xpr

# synthesis
launch_runs synth_1 -jobs 40
wait_on_run synth_1

# implement
launch_runs impl_1 -jobs 40
wait_on_run impl_1

# set property
open_run impl_1

# set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
set_property STEPS.WRITE_BITSTREAM.TCL.PRE {../../../pre-bit.tcl} [get_runs impl_1]

# bitstream
launch_runs impl_1 -to_step write_bitstream -jobs 40
