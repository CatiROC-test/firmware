set bitfile [lindex $argv 0]
set argument1 "PROGRAM.FILE \{"
set argument2 "\} \[get_hw_devices xc7k325t_0\]"
set argument $argument1$bitfile$argument2
open_hw
connect_hw_server
open_hw_target
current_hw_device [get_hw_devices xc7k325t_0]
refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xc7k325t_0] 0]
set_property PROBES.FILE {} [get_hw_devices xc7k325t_0]
set_property FULL_PROBES.FILE {} [get_hw_devices xc7k325t_0]
eval set_property $argument
program_hw_devices [get_hw_devices xc7k325t_0]
refresh_hw_device [lindex [get_hw_devices xc7k325t_0] 0]
