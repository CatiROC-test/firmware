#!/bin/sh

# To compile some files including ip cores, use
# ghdl_include *.vhd*
# ghdl_make --ieee=synopsys -P /software/altera/CompiledLibraries/Quartus/13.1 --warn-unused -fexplicit p1_counter

rm -rf work

IEEE=synopsys
EXT=".vhd"
STD=02

# add sources

zsh -i -c "ghdl_include ../src-cyclone3/*.vhd" && \
    echo "included all vhdl sources" || \
	echo "error including all vhdl sources"

# add IPs

IP="Cati_output_fifo1"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_output_fifoSC"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_unifFifo"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_frame_fifo"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_dataout_fifo"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="discri_small_fifo"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="pll"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="altddio_syncSrClk"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
    	echo "error including $IP"

IP="pll_adc"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
    	echo "error including $IP"

IP="fifo_8"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="fifo_128"
zsh -i -c "ghdl_include ../quartus13.1-project/cores/$IP/${IP}${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

# compile

ghdl -m -g -O2 --work=work --workdir=work --std=$STD --ieee=$IEEE \
     -P/software/altera/CompiledLibraries/Quartus/13.1\
     --warn-unused -fexplicit top

# gen makefile

ghdl --gen-makefile -g -O2 --work=work --workdir=work --std=$STD --ieee=$IEEE \
     -P/software/altera/CompiledLibraries/Quartus/13.1 \
     --warn-unused -fexplicit top > Makefile_cyclone3
