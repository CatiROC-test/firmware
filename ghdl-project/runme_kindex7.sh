#!/bin/sh

# To compile some files including ip cores, use
# ghdl_include *.vhd*
# ghdl_make --ieee=synopsys -P/software/Xilinx/CompiledLibraries/Vivado/2017.2 --warn-unused -fexplicit p1_counter

rm -rf work

IEEE=synopsys
EXT="sim_netlist.vhdl"
STD=02

# add sources

zsh -i -c "ghdl_include ../src-kindex7/*.vhd" && \
    echo "included all vhdl sources" || \
	echo "error including all vhdl sources"

# add IPs

IP="Cati_output_fifo1"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_output_fifoSC"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_unifFifo"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="Cati_frame_fifo"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="cati_dataout_fifo"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="discri_small_fifo"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="pll"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="pll_gcu"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="selectio_wiz_0"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="fifo_8"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="fifo_64"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

IP="fifo_128"
zsh -i -c "ghdl_include ../vivado17.2-project/ip/$IP/${IP}_${EXT}" && \
    echo "included $IP" || \
	echo "error including $IP"

# compile

ghdl -m -g -O2 --work=work --workdir=work --std=$STD --ieee=$IEEE \
     -P/software/Xilinx/CompiledLibraries/Vivado/2017.2 \
     --warn-unused -fexplicit top

# gen makefile

# ghdl --gen-makefile -g -O2 --work=work --workdir=work --std=$STD --ieee=$IEEE \
    #      -P/software/Xilinx/CompiledLibraries/Vivado/2017.2 \
    #      --warn-unused -fexplicit top > Makefile_kindex7
