;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

;; ((nil
;;   (vhdl-tools-vorg-tangle-header-argument-var . "altera")
;;   (vhdl-tools-vorg-src-vhdl-dir . "../src-cyclone3")
;;   (csb/runorgconfig-enable . nil)
;;   (org-publish-project-alist . (("CatiROC-test-firmware"
;;				 :base-directory "/home/csantos/Projects/apc/juno/CatiROC-test-firmware/src-vorg/"
;;				 :publishing-directory "/tmp/src-html/"
;;				 :publishing-function org-html-publish-to-html
;;				 :makeindex nil
;;				 :section-numbers nil
;;				 :auto-sitemap t
;;				 :sitemap-filename "index"
;;				 :sitemap-title "CatiROC tests Firmware"
;;				 :with-toc t )))))

((nil
  (vhdl-tools-vorg-tangle-header-argument-var . "xilinx")
  (vhdl-tools-vorg-src-vhdl-dir . "../src-kindex7")
  (csb/runorgconfig-enable . nil)
  (org-publish-project-alist . (("CatiROC-test-firmware"
				 :base-directory "/home/csantos/Projects/apc/juno/CatiROC-test-firmware/src-vorg/"
				 :publishing-directory "/tmp/src-html/"
				 :publishing-function org-html-publish-to-html
				 :makeindex nil
				 :section-numbers nil
				 :auto-sitemap t
				 :sitemap-filename "index"
				 :sitemap-title "CatiROC tests Firmware"
				 :with-toc t )))))
