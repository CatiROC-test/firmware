-*- mode: vhdl-tools-vorg -*-

#+TITLE:     CatiROC
#+AUTHOR:    Cayetano Santos
#+LANGUAGE:  en
#+DESCRIPTION: CatiROC interface peripheral
#+OPTIONS:   H:5 num:t toc:3

#+SETUPFILE: readtheorg-local.setup

* Table of Contents                                 :TOC_5_org:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[Catiroc][Catiroc]]
  - [[Header][Header]]
  - [[Libraries][Libraries]]
    - [[Juno mezzamine code][Juno mezzamine code]]
  - [[Packages][Packages]]
    - [[Altera][Altera]]
    - [[Juno mezzamine code][Juno mezzamine code]]
  - [[Entity][Entity]]
  - [[Architecture][Architecture]]
    - [[Signals][Signals]]
    - [[begin][begin]]
    - [[P3: Slow Control, "CC" Command][P3: Slow Control, "CC" Command]]
    - [[P2: Data Capture, "06" Command][P2: Data Capture, "06" Command]]
    - [[Combine signals][Combine signals]]
    - [[end][end]]

* Catiroc

** Header

#+begin_src vhdl-tools
  -- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
#+end_src

** Libraries

#+begin_src vhdl-tools
  library IEEE;
#+end_src

*** Juno mezzamine code                                               :xilinx:

#+begin_src vhdl-tools
  library UNISIM;
#+end_src

** Packages

#+begin_src vhdl-tools
  use IEEE.STD_LOGIC_1164.all;
  use IEEE.numeric_std.all;
  use work.common_package.all;
#+end_src

*** Altera                                                  :altera:

#+begin_src vhdl-tools
  use work.cati_package.all;
#+end_src

*** Juno mezzamine code                                               :xilinx:

#+begin_src vhdl-tools
  use work.juno_mezzanine_package.all;
  use UNISIM.VComponents.all;
#+end_src

** Entity

#+begin_src vhdl-tools
  entity catiroc is
     generic (num_of_discri_channels : natural range 1 to 16;
              p2_data_width          : natural range 1 to 16);
     port (clk_10MHz         : in  std_logic;
           clk_80MHz         : in  std_logic;
           clk_40MHz         : in  std_logic;
           clk_480MHz        : in  std_logic;
           clk_120MHz        : in  std_logic;
           pll_locked        : in  std_logic;
           discri_falling    : out std_logic_vector(15 downto 0);
           ovf               : in  std_logic;
           main_rst          : in  std_logic;
           triggers          : in  std_logic_vector (15 downto 0);
           NewEvent          : out std_logic;
           NewEventByChannel : out std_logic_vector(15 downto 0);
           --cati
           resetb            : out std_logic;
           StartSys          : out std_logic;
           sr_clk            : out std_logic;
           sr_in             : out std_logic;  -- serialized data to configure cati
           sr_rst            : out std_logic;  -- reset cati before sending configuration
           sr_select         : out std_logic;  -- to be able to write in cati
           sr_out            : in  std_logic;  -- serialized data to read back conf sen
           -- usb
           clk_2232          : in  std_logic;  -- usb clock, 60 MHz
           p2_datain         : out std_logic_vector(p2_data_width*8-1 downto 0);
           p2_params         : in  std_logic_vector(7 downto 0);
           p3_datain         : out std_logic_vector(7 downto 0);
           p3_params         : in  std_logic_vector(527 downto 0);
           p3_read           : in  std_logic;
           p3_enable         : in  std_logic;
           p2_enable         : in  std_logic;
           p2_read           : in  std_logic;
           p3_empty          : out std_logic;
           p2_empty          : out std_logic;
           cati_dataout      : in  std_logic_vector (1 downto 0);
           cati_strobe       : in  std_logic_vector (1 downto 0));
  end entity catiroc;
#+end_src

** Architecture

#+begin_src vhdl-tools
  architecture Behavioral of catiroc is
#+end_src

*** Signals

#+begin_src vhdl-tools
  signal resetb_capture, resetb_config : std_logic := '0';
  signal sr_rst_sc       : std_logic := '1';
#+end_src

*** begin

#+begin_src vhdl-tools
  begin
#+end_src

*** P3: Slow Control, "CC" Command

Slow Control Management.

Implements sending control registers to asic, and reading them back.
When the software sends a command to peripheral 3, the parameters are
transferred to the asic twice. As slow control is sent through a shift register,
its output is read back here and sent to the usb. The software will read usb
contents, and compare with data sent.

#+begin_src vhdl-tools
  U_catiroc_slowcontrol : catiroc_slowcontrol
     port map(clk          => clk_10MHz,
              main_rst     => main_rst,
              -- catiroc interface
              resetb       => resetb_config,
              start_system => open,
              sr_clk       => sr_clk,     -- : out   std_logic;
              sr_in        => sr_in,  -- : out   std_logic; -- serialized data to configure cati
              sr_rst       => sr_rst_sc,  -- : out   std_logic; -- reset cati before sending configuration
              sr_select    => sr_select,  -- : out   std_logic; -- to be able to write in cati
              sr_out       => sr_out,  -- : in    std_logic; -- serialized data to read back conf send
              -- usb interface
              clk_2232     => clk_2232,   -- usb clock, 60 MHz
              p3_params    => p3_params,
              usb_data_out => p3_datain,  -- 15 bits, usb Output bus (Send data to usb)
              usb_fifo1_rd => p3_read,    -- USB demande lecture de la fifo
              enable       => p3_enable,  -- PC demande l'ordre decriture et de lecture dans la fifo
              fifo_empty   => p3_empty);  -- FIFO_empty_signal
#+end_src

*** P2: Data Capture, "06" Command

Data Capture Management.

Implements ...

#+begin_src vhdl-tools
  U_Handle_dataout_top : catiroc_data_capture
     generic map (num_of_discri_channels => num_of_discri_channels,
                  p2_data_width          => p2_data_width)
     port map(Clk_usb           => clk_2232,
              ovf               => ovf,
              triggers          => triggers,
              test              => open,
              NewEvent          => NewEvent,
              NewEventByChannel => NewEventByChannel,
              clk_80Mhz         => clk_80Mhz,
              clk_40Mhz         => clk_40Mhz,
              clk_480Mhz        => clk_480Mhz,
              clk_120Mhz        => clk_120Mhz,
              pll_locked        => pll_locked,
              discri_falling    => discri_falling,
              main_rst          => main_rst,
              resetb            => resetb_capture,
              StartSys          => StartSys,
              usb_params2       => p2_params,
              fifo_dataout      => p2_datain,
              fifo_rd           => p2_read,
              fifo_empty        => p2_empty,
              p2_enable         => p2_enable,
              cati_dataout      => cati_dataout,
              cati_strobe       => cati_strobe);
#+end_src

*** Combine signals

Common =restb= (resets the ASIC) for the two blocks.

#+begin_src vhdl-tools
   -- resetb <= resetb_capture and resetb_config;
   resetb <= resetb_capture;
#+end_src

Common =sr_rst= (resets the SC shift register, active low) for the sc
and the =main_rst=.

#+begin_src vhdl-tools
  -- sr_rst <= not (main_rst) and sr_rst_sc;
  sr_rst <= not (main_rst);
#+end_src

*** end

#+begin_src vhdl-tools
  end architecture Behavioral;
#+end_src
