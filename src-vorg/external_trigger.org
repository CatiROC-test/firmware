-*- mode: vhdl-tools-vorg -*-

#+TITLE:     S Curve
#+AUTHOR:    Cayetano Santos
#+LANGUAGE:  en
#+DESCRIPTION: Implements S-Curve computing
#+OPTIONS:   H:5 num:t toc:3
#+SETUPFILE: readtheorg-local.setup

* Table of Contents                                 :TOC_5_org:noexport:
:PROPERTIES:
:VISIBILITY: all
:END:

- [[External_trigger][External_trigger]]
  - [[Header][Header]]
  - [[Libraries][Libraries]]
  - [[Packages][Packages]]
  - [[Entity][Entity]]
  - [[Architecture][Architecture]]
    - [[Signals][Signals]]
    - [[begin][begin]]
    - [[Process][Process]]
    - [[end][end]]
 
* External_trigger

** Header

#+begin_src vhdl-tools
  -- DON'T MODIFY THIS FILE. INSTEAD, MODIFY ITS CORRESPONDING ORG FILE.
#+end_src

** Libraries

#+begin_src vhdl-tools
  library IEEE;
#+end_src

** Packages

#+begin_src vhdl-tools
  use IEEE.STD_LOGIC_1164.all;
  use IEEE.numeric_std.all;
#+end_src

** Entity

#+begin_src vhdl-tools
  entity external_trigger is
     port (clk                   : in  std_logic;
           main_rst              : in  std_logic;
           trig_ext              : out std_logic;
           params                : in  std_logic_vector(3 downto 0);
           EnableExternalTrigger : in  std_logic;
           enable                : in  std_logic;
           io_fpga1              : in  std_logic);
  end entity external_trigger;
#+end_src

** Architecture

#+begin_src vhdl-tools
  architecture Behavioral of external_trigger is
#+end_src

*** Signals

#+begin_src vhdl-tools
  signal counter_trig_ext                            : unsigned(15 downto 0)                 := (others => '0');
#+end_src

Temp trig ext to be oored with external signal

#+begin_src vhdl-tools
  signal trig_ext_inner : std_logic := '0';  -- temp trig ext to be oored with external signal
#+end_src

*** begin

#+begin_src vhdl-tools
  begin
#+end_src

*** Process

#+begin_src vhdl-tools
  U_trig_ext : process (clk) is
  begin
     if clk'event and clk = '1' then
        if main_rst = '1' then
           counter_trig_ext <= (others => '0');
           trig_ext_inner   <= '0';
        else
           if counter_trig_ext >= unsigned(params & X"800") then
              trig_ext_inner   <= enable;
              counter_trig_ext <= (others => '0');
           else
              trig_ext_inner   <= '0';
              counter_trig_ext <= counter_trig_ext + 1;
           end if;
        end if;
     end if;
  end process U_trig_ext;

  trig_ext <= trig_ext_inner or (io_fpga1 and EnableExternalTrigger);
#+end_src

*** end

#+begin_src vhdl-tools
  end architecture Behavioral;
#+end_src
